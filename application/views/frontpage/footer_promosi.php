<footer>
	<div class="container">
		<div class="row">
		    <div class="col-sm-3 col-lg-3">
				<div class="widget">
					<h4>Alamat Perusahaan</h4>
					<address>
						<strong>PT. Peksi Gunaraharja</strong><br>
					Jl. Cangkringan Km 5, Ngasem, Selomartani, Kalasan, Sleman<br> Yogyakarta 55571</address>
					<p>
						<i class="icon-phone"></i> (0274) 2850063 <br>
						<i class="icon-envelope-alt"></i> peksigunaraharja@gmail.com
					</p>
				</div>
			</div>
			<div class="col-sm-3 col-lg-3">
				<div class="widget">
					<h4>Peksi Gunaraharja</h4>
					<ul class="link-list">
						<li><a href="<?php echo base_url('brosur'); ?>">Download Brosur</a></li>
						<li><a href="<?php echo base_url('artikel'); ?>">Artikel</a></li>
						<li><a href="<?php echo base_url('galeri'); ?>">Galeri</a></li>
						<li><a href="<?php echo base_url('kontak'); ?>">Kontak</a></li>
						<li><a href="<?php echo base_url('tentang'); ?>">Tentang Kami</a></li>
					</ul>
				</div>
				
			</div>
			
			<div class="col-sm-3 col-lg-3">
			    <h4>Feedback Peksi</h4>
				<div class="widget">
				   <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FDOQ-puyuh-impor%2F656995837689480&amp;width&amp;height=800&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true" scrolling="YES" frameborder="0" style="border:none; overflow:hidden; height:135px;" allowtransparency="true"></iframe>
				</div>
			</div>

			<div class="col-sm-3 col-lg-3">
			</div>
			
		</div>
	</div>
	<div id="sub-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="copyright">
						<p>Copyright &copy; 2017 - PT. Peksi Gunaraharja Yogyakarta <br> Seluruh material konten yang ada di website ini, baik berupa tulisan dan gambar merupakan hak cipta PT. Peksi Gunaraharja </p>
                        <div class="credits">
                            
                        </div>
					</div>
				</div>
				<div class="col-lg-6">
					<ul class="social-network">
						<li><a href="https://www.facebook.com/jualbibitpuyuh" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
						<li><a href="https://twitter.com/jualburungpuyuh" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	</footer>