<!DOCTYPE html>
<html>
<head lang="en">
	<?php require_once(APPPATH .'views/include_front/head_style.php'); ?>
</head>
<body>

<div id="wrapper">
	
	<?php $this->load->view('frontpage/nav_menu_front'); ?>

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-sm-12 col-lg-12">
						<h4> PT. Peksi Gunaraharja</h4>
						<p>
						"Berternak tidak hanya sekedar memberi pakan, namun lebih dari itu, berternak harus dilakukan dengan rasa kasih sayang. Keuntungan akan menghampiri karena keiklasan kita memelihara mahluk ciptaan-nya"
						</p>
						<p>
							Yang perlu diperhatikan oleh peternak sebelum memulai usaha adalah memahami 3 (tiga) unsur produksi usaha peternakan yaitu :
							<ul>
								<li>Bibit</li>
								<li>Pakan (Ransum)</li>
								<li>Management Pemeliharaan</li>
							</ul>
						</p>
						<p>
							Bibit merupakan salah satu faktor penentu dalam keberhasilan usaha peternakan, maka pemilihan bibit yang berkualitas sangatlah penting.
						</p>
						<p>
							Banyak beredar dikalangan peternak, bibit lokal yang kualitasnya masih perlu dipertanyakan, dimana umur bibit yang tidak seragam dan dari hasil perkawinan satu keluarga (inbreeding). Hal ini akan berdampak pada penurunan performace dan produktivitas puyuh. Implikasinya peternak akan mengalami kerugian atau keuntungan yang tidak maksimal.
						</p>
						<p>
							<strong>PT. Peksi Gunaraharja</strong> adalah perusahaan peternakan puyuh yang integrated. Satu - satunya Perusahaan Breeder puyuh di Indonesia yang menghasilkan <strong>DOQ ( Day Old Quail )</strong> yang berkualitas.
						</p>
						<p>
							Dengan dukungan teknologi berstandar internasional dan tenaga ahli di bidangnya, PT. Peksi Gunaraharja mampu menghasilkan DOQ dengan kapasitas 150.000 DOQ perminggu dengan tingkat keseragaman dan kesehatan yang baik serta waktu tetas yang tepat, sehingga kualitas DOQ dapat terjamin.
						</p>
					</div>
				</div>
			</div>
		</div>
		</div>

		<!-- divider -->
		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="solidline">
				</div>
			</div>
		</div>
		</div>
		<!-- end divider -->

		<div class="container">
			<!-- Descriptions -->
			<div class="row">
			    <div class="text-center">
					<h2>Visi dan Misi</h2>
				</div>
				<div class="col-lg-6">
					<!-- Description -->
					<h4>Visi</h4>
					<dl style="font-size: 16px; text-align: justify; line-height: 29px;">
						Dengan  berlandaskan iman dan taqwa <strong>PEKSI GUNARAHARJA</strong> menjadi perusahaan yang berkembang di dalam sector peternakan unggas pada umumnya dan puyuh pada khususnya yang profesional, tangguh, efisien, jujur, berteknologi, paling maju, produktif, kompetitif, dan berusaha semaksimal mungkin memberikan pelayanan dan kualitas yang terbaik bagi masyarakat di Indonesia.
					</dl>
					<dl>
						<img style="width: 100%" src="<?php echo base_url('assets_front/img_artikel/visi_misi.jpg'); ?>" class="img-responsive" alt="Image">
					</dl>
				</div>
				<!-- Horizontal Description -->
				<div class="col-lg-6">
					<h4>Misi</h4>
					<dl>
						<ul style="font-size: 16px; line-height: 27px; text-align: justify;">
						    <li>Meningkatakan kertersediaan bahan pangan ternak memenuhi kebutuhan dan kecukupan gizi masyarakat menuju kecukupan gizi protein hewani.</li>
						    <li>Meningkatkan kualitas sumberdaya masyarakat peternakan yang berperan aktif dalam kegiatan usaha peternakanyang berbasis agribisnis.</li>
						    <li>Membangun system kelembagaan usaha tani ternak yang tangguh dan mampu menjalin pola kemitraan guna pengembangan peternakan dalam pemasaran produk unggulan serta melestariakan komoditi dan populasi ternak puyuh.</li>
						    <li>Menggunakan teknologi tepat guna yang berwawasan ramah lingkungan didukung dengan pembinaan berkelanjutan.</li>
						    <li>Meningkatkan dan mengembangkan produk dalam negri yang unggul yang berdaya saing tinggi untuk menghadapi pasar global.</li>
						    <li>Menciptakan birokrasi yang profesional serta memiliki integritas moral yang tinggi.</li>
						    <li>Menjadi perusahaan terdepan di bidangnya.</li>
						    <li>Memperluas lapangan kerja untuk kemakmuran mahasiswa dan masyarakat sekitar tempat produksi pada khususnya dan masyarakat Indonesia pada umumnya.</li>
						</ul>
					</dl>
				</div>
			</div>
			<!-- divider -->
			<div class="row">
				<div class="col-lg-12">
					<div class="solidline">
					</div>
				</div>
			</div>
			<!-- end divider -->
		</div>

		<div class="container">
			<!-- Descriptions -->
			<div class="row">
			    <div class="text-center">
					<h2>Video Local</h2>
				</div>
				<div class="col-lg-12">
					<video width="400" controls>
					  <source src="<?php echo base_url('assets_front/video/video.mp4'); ?>" type="video/mp4">
					  <source src="video.ogg" type="video/ogg">
					  Your browser does not support HTML5 video.
					</video>
				</div>
			</div>
			<!-- divider -->
			<div class="row">
				<div class="col-lg-12">
					<div class="solidline">
					</div>
				</div>
			</div>
			<!-- end divider -->
		</div>
	
	<?php $this->load->view('frontpage/footer.php'); ?>
</div>
  <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>

	<?php require_once(APPPATH .'views/include_front/head_script.php'); ?>
</body>
</html>