<!DOCTYPE html>
<html>
<head lang="en">
	<?php require_once(APPPATH .'views/include_front/head_style.php'); ?>
</head>
<body>

<div id="wrapper">
	
	<?php $this->load->view('frontpage/nav_menu_front'); ?>

	<section id="content">
		<div class="container">
			<!-- Descriptions -->
			<div class="row">
			    <div class="text-center">
					<h2>Visi dan Misi</h2>
				</div>
				<div class="col-lg-6">
					<!-- Description -->
					<h4>Visi</h4>
					<dl style="font-size: 16px; text-align: justify; line-height: 29px;">
						Dengan  berlandaskan iman dan taqwa <strong>PEKSI GUNARAHARJA</strong> menjadi perusahaan yang berkembang di dalam sector peternakan unggas pada umumnya dan puyuh pada khususnya yang profesional, tangguh, efisien, jujur, berteknologi, paling maju, produktif, kompetitif, dan berusaha semaksimal mungkin memberikan pelayanan dan kualitas yang terbaik bagi masyarakat di Indonesia.
					</dl>
					<dl>
						<img style="width: 100%" src="<?php echo base_url('assets_front/img_artikel/visi_misi.jpg'); ?>" class="img-responsive" alt="Image">
					</dl>
				</div>
				<!-- Horizontal Description -->
				<div class="col-lg-6">
					<h4>Misi</h4>
					<dl>
						<ul style="font-size: 16px; line-height: 27px; text-align: justify;">
						    <li>Meningkatakan kertersediaan bahan pangan ternak memenuhi kebutuhan dan kecukupan gizi masyarakat menuju kecukupan gizi protein hewani.</li>
						    <li>Meningkatkan kualitas sumberdaya masyarakat peternakan yang berperan aktif dalam kegiatan usaha peternakanyang berbasis agribisnis.</li>
						    <li>Membangun system kelembagaan usaha tani ternak yang tangguh dan mampu menjalin pola kemitraan guna pengembangan peternakan dalam pemasaran produk unggulan serta melestariakan komoditi dan populasi ternak puyuh.</li>
						    <li>Menggunakan teknologi tepat guna yang berwawasan ramah lingkungan didukung dengan pembinaan berkelanjutan.</li>
						    <li>Meningkatkan dan mengembangkan produk dalam negri yang unggul yang berdaya saing tinggi untuk menghadapi pasar global.</li>
						    <li>Menciptakan birokrasi yang profesional serta memiliki integritas moral yang tinggi.</li>
						    <li>Menjadi perusahaan terdepan di bidangnya.</li>
						    <li>Memperluas lapangan kerja untuk kemakmuran mahasiswa dan masyarakat sekitar tempat produksi pada khususnya dan masyarakat Indonesia pada umumnya.</li>
						</ul>
					</dl>
				</div>
			</div>
			<!-- divider -->
			<div class="row">
				<div class="col-lg-12">
					<div class="solidline">
					</div>
				</div>
			</div>
			<!-- end divider -->
		</div>
	</section>
	
	<?php $this->load->view('frontpage/footer.php'); ?>
</div>
  <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>

	<?php require_once(APPPATH .'views/include_front/head_script.php'); ?>
</body>
</html>