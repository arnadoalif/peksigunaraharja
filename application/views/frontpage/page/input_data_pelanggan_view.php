<!DOCTYPE html>
<html>
<head lang="en">
	<?php require_once(APPPATH .'views/include_front/head_style.php'); ?>
</head>
<body>

<div id="wrapper">
	
	<?php $this->load->view('frontpage/nav_menu_front'); ?>

	<div class="container">
		<div class="row">
		    <div class="col-xs-12 col-sm-8 col-md-4">
				
			</div>
			<div class="col-xs-12 col-sm-8 col-md-8">
				

				<form role="form" action="<?php echo base_url('page/action_register_pelanggan'); ?> " method="POST" class="register-form">
					<h2>Formulir Registrasi <small> <br>Isikan data diri dengan lengkap dan pastikan cek kembali agar tidak ada kesalahan</small></h2>
					<?php if (isset($_SESSION['sendmessage'])): ?>
					<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
						</button>
						<?php echo $_SESSION['sendmessage'] ?>
					</div>
					<?php endif ?>
					<?php if (isset($_SESSION['validmessage'])): ?>
					<div class="alert alert-warning" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
						</button>
						<?php echo $_SESSION['validmessage'] ?>
					</div>
					<?php endif ?>
					<div class="form-group">
						<input type="text" name="nama_pelanggan" required id="nama_pelanggan" class="form-control input-md" placeholder="Nama Lengkap" tabindex="3">
					</div>
					<div class="form-group">
						<input type="text" name="username" required id="username" class="form-control input-md" placeholder="Username" tabindex="3">
					</div>
					<div class="form-group">
						<input type="text" name="email_address" required id="email_address" class="form-control input-md" placeholder="Email Address" tabindex="3">
					</div>
					<div class="form-group">
						<input type="text" name="nomor_telepon" required onkeypress="return isNumber(event)" id="nomor_telepon" class="form-control input-md" placeholder="Nomor telepon / WhatsApp" tabindex="3">
					</div>
					<div class="form-group">
						<input type="password" name="password" required id="password" class="form-control input-md" placeholder="Password" tabindex="3">
					</div>
					<div class="form-group">
						<textarea name="alamat" id="alamat" required class="form-control input-md" rows="3" placeholder="Alamat Pengiriman" tabindex="4"></textarea>
					</div>
					<div class="form-group">
						<select name="kota" id="provinsi" class="form-control input-md" required>
							<option value="">Pilih Provinsi</option>
						</select>
					</div>
					<input type="hidden" name="wilayah" id="wilayah" value="" placeholder="">
					<input type="hidden" name="koordinat" id="koordinat" value="" placeholder="">
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<div class="col-md-6 col-lg-6">
								<input type="submit" id="submit" value="Kirim data" class="btn btn-theme btn-block btn-md" tabindex="7">
							</div>
							<div class="col-md-6 col-lg-6">
								<a class="btn btn-theme btn-block btn-md" href="<?php echo base_url('loginmember') ?> " role="button">Masuk Pelanggan</a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<?php $this->load->view('frontpage/footer_promosi.php'); ?>
</div>
  <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>

	<?php require_once(APPPATH .'views/include_front/head_script.php'); ?>

	 <script type="text/javascript">
        jQuery(document).ready(function($) {
        	var base_url = window.location.origin;
            $.ajax({
            type: "GET",
            url: base_url+"/assets_front/js/indonesia.json",
            dataType: "json",
            success: function (data) {
                $.each(data, function(i, value) {
                    $('#provinsi').append($('<option>').text(value).attr('value', value));
                });
            },
            error: function (result) {
                console.log("Error");
            }
        });
        });

        function isNumber(evt) {
	      evt = (evt) ? evt : window.event;
	      var charCode = (evt.which) ? evt.which : evt.keyCode;
	      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
	          return false;
	      }
	      return true;
	  	}

	  	$(document).ready(function() {
	  		// $('#submit').click(function(event) {
	  			$.get("http://ipinfo.io", function (response) {
	  				// console.log(response.loc);
		  			// console.log(response.region);
		  			$("#wilayah").val(response.region);
	  				$("#koordinat").val(response.loc);
				    // $("#ip").html("IP: " + response.ip);
				    // $("#address").html("Location: " + response.city + ", " + response.region);
				    // $("#details").html(JSON.stringify(response, null, 4));
				}, "jsonp");
	  			
	  		// });
	  	});
    </script>


</body>
</html>