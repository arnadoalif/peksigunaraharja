<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="utf-8">
	<title><?php echo $data_artikel->judul_berita ?></title>
	<meta name="author" content="peksigunaraharja">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description"  itemprop="description" content="<?php echo $data_artikel->judul_berita." ". substr(strip_tags($data_artikel->isi_berita),0, 158); ?>...."/>
        <meta name="keywords" itemprop="keywords" content=" <?php echo str_replace(",", ", ", $data_artikel->tag_post); ?>">

	<meta property="og:url"           content="<?php echo current_url(); ?>" />
	<meta property="og:type"          content="Peksi Gunaraharja" />
	<meta property="og:title"         content="<?php echo $data_artikel->judul_berita ?>" />
	<meta property="og:description"   content="<?php echo substr(strip_tags($data_artikel->isi_berita),0, 160); ?>....." />

	<?php if (!empty($data_artikel->cover_berita)): ?>
		<meta property="og:image"         content="<?php echo base_url('assets/upload_image/'.$data_artikel->cover_berita)?>" />
	<?php else: ?>
		<meta property="og:image"         content="<?php echo base_url('assets/upload_image/default.png')?>" />
	<?php endif ?>

	<?php require_once(APPPATH .'views/include_front/head_style_artikel.php'); ?>
</head>
<body>

<div id="wrapper">
	
	<?php $this->load->view('frontpage/nav_menu_front'); ?>

	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url(); ?> "><i class="fa fa-home"></i></a></li>
					<li><a href="<?php echo base_url('artikel'); ?> ">Artikel</a></li>
					<li class="active"><?php echo $judul_berita; ?></li>
				</ul>
			</div>
		</div>
	</div>
	</section>

	<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-8">
				<article>
						<div class="post-image">
							<div class="post-heading">
								<h3><a href="<?php echo base_url('artikel/'.$data_artikel->kata_kunci); ?>"><?php echo $data_artikel->judul_berita; ?></a></h3>
							</div>
						</div>
						<p>
							 <?php echo $data_artikel->isi_berita; ?>
						</p>
						<div class="bottom-article">
							<ul class="meta-post">
								<li><i class="fa fa-calendar"></i><a href="#"> <?php echo date("M d, Y H:i:s", strtotime($data_artikel->tanggal_post)); ?></a></li>
								<li><i class="fa fa-user"></i><a href="#"> <?php echo $data_artikel->post_by; ?></a></li>
								<li>
								   <?php if (!empty($data_artikel->cover_berita)): ?>
									<a href="javascript:fbShare('<?php echo current_url(); ?>', 'Fb Share', 'Facebook share popup', '<?php echo base_url('assets/upload_image/'.$data_artikel->cover_berita)?>', 520, 350)"> <i class="fa fa-facebook"></i> Share Facebook </a>
								   <?php else: ?>
								        <a href="javascript:fbShare('<?php echo current_url(); ?>', 'Fb Share', 'Facebook share popup', '<?php echo base_url('assets/upload_image/default.png')?>', 520, 350)"><i class="fa fa-facebook"></i> Share Facebook</a>
								   <?php endif ?>
								</li>
								<li>
									<a class="twitter customer share" href="https://twitter.com/share?url=<?php echo current_url(); ?>&amp;text=Share popup on &amp;hashtags=peksigunaraharja" title="Twitter share" target="_blank"><i class="fa fa-twitter"></i> Share Twitter</a>
								</li>
								<li>
									<a class="google_plus customer share" href="https://plus.google.com/share?url=<?php echo current_url(); ?>" title="Google Plus Share" target="_blank"><i class="fa fa-google-plus"></i> Share Google Plus</a>
								</li>	
							</ul>
						</div>
				</article>
					<!-- <div class="comment-area">

						<h4>4 Comments</h4>
						<div class="media">
							<a href="#" class="pull-left"><img src="img/avatar.png" alt="" class="img-circle" /></a>
							<div class="media-body">
								<div class="media-content">
									<h6><span>June 24, 2013</span> Dean Zaloza</h6>
									<p>
									Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
									</p>
									
									<a href="#" class="align-right reply">Reply</a>
								</div>
							</div>
						</div>
						
						<div class="marginbot30"></div>
						<h4>Leave your comment</h4>
						
						
						<form role="form">
						  <div class="form-group">
							<input type="text" class="form-control" id="name" placeholder="* Enter name">
						  </div>
						  <div class="form-group">
							<input type="email" class="form-control" id="email" placeholder="* Enter email address">
						  </div>
						  <div class="form-group">
							<textarea class="form-control" rows="8" placeholder="* Your comment here"></textarea>
						  </div>
						  <button type="submit" class="btn btn-theme btn-md">Submit</button>
						</form>

					</div> -->
				
				<div class="clear"></div>
			</div>
			<div class="col-lg-4">
				<aside class="right-sidebar">
				<div class="widget">
					<!-- <form role="form">
					  <div class="form-group">
						<input type="text" class="form-control" id="s" placeholder="Search..">
					  </div>
					</form> -->
				</div>
				<!-- <div class="widget">
					<h5 class="widgetheading">Kategori</h5>
					<ul class="cat">
						<?php 
						$a = 0;
						$b = 0;
						$c = 0;
						$d = 0;
						$e = 0;
					 ?>
						<?php foreach ($count_artikel as $dt_artikel): ?>
							<?php if ($dt_artikel->kategori_berita == "Burung Puyuh"): ?>
								<?php $a++ ?>
							<?php elseif ($dt_artikel->kategori_berita == "Kesehatan"): ?>
								<?php $b++ ?>
							<?php elseif ($dt_artikel->kategori_berita == "Kandang"): ?>
								<?php $c++ ?>
							<?php elseif ($dt_artikel->kategori_berita == "Kotoran"): ?>
								<?php $d++ ?>
							<?php elseif ($dt_artikel->kategori_berita == "Resep"): ?>	
								
							<?php else: ?>		
							<?php endif ?>
						<?php endforeach ?>

						<li><i class="fa fa-angle-right"></i>Burung Puyuh<span> (<?php echo $a; ?>)</span></li>
						<li><i class="fa fa-angle-right"></i>Kesehatan Burung Puyuh<span> (<?php echo $b; ?>)</span></li>
						<li><i class="fa fa-angle-right"></i>Kandang Burung Puyuh<span> (<?php echo $c; ?>)</span></li>
						<li><i class="fa fa-angle-right"></i>Kotoran Burung Puyuh<span> (<?php echo $d; ?>)</span></li>
						<li><i class="fa fa-angle-right"></i>Resep Burung Puyuh<span> (<?php echo $e; ?>)</span></li>
					</ul>
				</div> -->
				<div class="widget">
					<h5 class="widgetheading">Artikel Lainnya</h5>
					<?php foreach ($data_aktivitas as $data_artikel): ?>
						<ul class="recent" style="border-bottom: 1px solid #DCDCDC;">
							<li>
								<a href="<?php echo base_url('artikel/'.$data_artikel->kata_kunci); ?> "><?php echo $data_artikel->judul_berita; ?></a>
							</li>
						</ul>
					<?php endforeach ?>
				</div>
				<!-- <div class="widget">
					<h5 class="widgetheading">Popular tags</h5>
					<ul class="tags">
					    <?php 
					    $parts=explode(",",$tag_post);
						foreach ($parts as $key => $value) {
							echo "<li><a>".$value."</a></li>";
						}
					     ?>
					</ul>
				</div> -->
				</aside>
			</div>
		</div>
	</div>
	</section>

	
	<?php $this->load->view('frontpage/footer.php'); ?>
</div>
  <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>

	<?php require_once(APPPATH .'views/include_front/head_script.php'); ?>
	<script>
	    function fbShare(url, title, descr, image, winWidth, winHeight) {
	        var winTop = (screen.height / 2) - (winHeight / 2);
	        var winLeft = (screen.width / 2) - (winWidth / 2);
	        window.open('http://www.facebook.com/sharer.php?s=100&p[title]=' + title + '&p[summary]=' + descr + '&p[url]=' + url + '&p[images][0]=' + image, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
	    }

		$(document).ready(function() {
	    	/**
			   * jQuery function to prevent default anchor event and take the href * and the title to make a share popup
			   *
			   * @param  {[object]} e           [Mouse event]
			   * @param  {[integer]} intWidth   [Popup width defalut 500]
			   * @param  {[integer]} intHeight  [Popup height defalut 400]
			   * @param  {[boolean]} blnResize  [Is popup resizeabel default true]
			   */
			  $.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {
			    
			    // Prevent default anchor event
			    e.preventDefault();
			    
			    // Set values for window
			    intWidth = intWidth || '500';
			    intHeight = intHeight || '400';
			    strResize = (blnResize ? 'yes' : 'no');

			    // Set title and open popup with focus on it
			    var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
			        strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,            
			        objWindow = window.open(this.attr('href'), strTitle, strParam).focus();
			  }
			  
			  /* ================================================== */
			  
			  $(document).ready(function ($) {
			    $('.customer.share').on("click", function(e) {
			      $(this).customerPopup(e);
			    });
			  });
	   	 });


	</script>
</body>
</html>