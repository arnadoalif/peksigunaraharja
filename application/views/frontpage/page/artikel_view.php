<!DOCTYPE html>
<html>
<head lang="en">
	<?php require_once(APPPATH .'views/include_front/head_style.php'); ?>
</head>
<body>

<div id="wrapper">
	
	<?php $this->load->view('frontpage/nav_menu_front'); ?>


	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url(); ?> "><i class="fa fa-home"></i></a></li>
					<li class="active">Artikel</li>
				</ul>
			</div>
		</div>
	</div>
	</section>
	
	<!-- <section id="content"> -->
	<div class="container">
		<div class="row">
			<?php 
				//$no = $this->uri->segment('3') + 1;
			 ?>
			 <div class="col-lg-12">
			 	<div class="index-content">
				   <!--  <div class="container"> -->
				        <?php foreach ($data_aktivitas as $data_artikel): ?>
								<a href="<?php echo base_url('artikel/'.$data_artikel->kata_kunci); ?>">
									<div class="col-lg-3">
										<div class="card">
											<?php if (!empty($data_artikel->cover_berita)): ?>
												<img style="height: 202px;" src="<?php echo base_url('assets/upload_image/'.$data_artikel->cover_berita);?>">
											<?php else: ?>
												<img style="height: 200px;" src="<?php echo base_url('assets/upload_image/default.jpg');?>">
											<?php endif ?>
											<h5><?php echo $data_artikel->judul_berita; ?></h5>

											<a href="<?php echo base_url('artikel/'.$data_artikel->kata_kunci); ?>" class="red-button">Selanjutnya</a>
										</div>
									</div>
								</a>
						<?php endforeach ?>
				   <!--  </div> -->
				</div>
			</div>
		</div>
		<?php //echo $this->pagination->create_links(); ?>
	</div>
	<!-- </section> -->

	
	<?php $this->load->view('frontpage/footer.php'); ?>
</div>
  <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>

	<?php require_once(APPPATH .'views/include_front/head_script.php'); ?>
</body>
</html>
