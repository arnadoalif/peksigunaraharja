<!DOCTYPE html>
<html>
<head lang="en">
	<?php require_once(APPPATH .'views/include_front/head_style.php'); ?>
	<style type="text/css">
		.glyphicon { margin-right:10px; }
		.panel-body { padding:0px; }
		.panel-body table tr td { padding-left: 15px }
		.panel-body .table {margin-bottom: 0px; }

		*,
		*:before,
		*:after {
		    -webkit-box-sizing: border-box; 
		    -moz-box-sizing: border-box; 
		    box-sizing: border-box;
		}

		a{
			text-decoration: none;
		}

		#integration-list {
		    font-family: 'Open Sans', sans-serif;
		    width: 80%;
		    margin: 0 auto;
		    display: table;
		}
		#integration-list ul {
		    padding: 0;
		    margin: 20px 0;
		    color: #555;
		}
		#integration-list ul > li {
		    list-style: none;
		    border-top: 1px solid #ddd;
		    display: block;
		    padding: 15px;
		    overflow: hidden;
		}
		#integration-list ul:last-child {
		    border-bottom: 1px solid #ddd;
		}
		#integration-list ul > li:hover {
		    background: #efefef;
		}
		.expand {
		    display: block;
		    text-decoration: none;
		    color: #555;
		    cursor: pointer;
		}
		#integration-list h2 {
		    padding: 0;
		    margin: 0;
		    font-size: 17px;
		    font-weight: 400;
		}
		#integration-list span {
		    font-size: 12.5px;
		}
		#left,#right{
		    display: table;
		}
		#sup{
		    display: table-cell;
		    vertical-align: middle;
		    width: 80%;
		}
		.detail a {
		    text-decoration: none;
		    color: #C0392B;
		    border: 1px solid #C0392B;
		    padding: 6px 10px 5px;
		    font-size: 14px;
		}
		.detail {
		    margin: 10px 0 10px 0px;
		    display: none;
		    line-height: 22px;
		    height: 150px;
		}
		.detail span{
		    margin: 0;
		}
		.right-arrow {
		    margin-top: 12px;
		    margin-left: 20px;
		    width: 10px;
		    height: 100%;
		    float: right;
		    font-weight: bold;
		    font-size: 20px;
		}
		#integration-list .icon {
		    height: 75px;
		    width: 75px;
		    float: left;
		    margin: 0 15px 0 0;
		}
		.london {
		    background: url("http://placehold.it/50x50") top left no-repeat;
		    background-size: cover;
		}
		.newyork {
		    background: url("http://placehold.it/50x50") top left no-repeat;
		    background-size: cover;
		}
		.paris {
		    background: url("http://placehold.it/50x50") top left no-repeat;
		    background-size: cover;
		}

		#calendar {
				max-width: 900px;
				margin: 0 auto;
			}

		.fc-unthemed td.fc-today {
		    background: #64e55d;
		}

		.bts-popup-container .bts-popup-button {
		    /*padding: 3px 5px;*/
		    /*border: 2px solid white;*/
		    display: inline-block;
		    margin-bottom: 160px;
		}

		.bts-popup-button {
		    left: 326px;
		    position: absolute;
		    margin: 0 auto;
		    bottom: 145px;
		    font-weight: 800;
		}

		.bts-popup-container a {
		    color: #090808;
		    text-decoration: none;
		    text-transform: uppercase;
		    background: yellow;
		    padding: 8px;
		}

		.bts-popup-button a:hover {
			background: yellow;
			color: black;
		}

		 @media (min-width: 320px) and (max-width: 480px) {
            .bts-popup-button {
			    left: 162px;
			    position: absolute;
			    margin: 0 auto;
			    bottom: -9px;
			    font-weight: 800;
			}

			.bts-popup-container a {
			    color: #090808;
			    text-decoration: none;
			    text-transform: uppercase;
			    background: yellow;
			    padding: 4px;
			    font-size: 12px;
			}
        }
</style>
<style type="text/css">
		.img-replace {
			  /* replace text with an image */
			  display: inline-block;
			  overflow: hidden;
			  text-indent: 100%; 
			  color: transparent;
			  white-space: nowrap;
			}
			.bts-popup {
			  position: fixed;
			  left: 0;
			  top: 0;
			  height: 100%;
			  width: 100%;
			  background-color: rgba(0, 0, 0, 0.5);
			  opacity: 0;
			  visibility: hidden;
			  -webkit-transition: opacity 0.3s 0s, visibility 0s 0.3s;
			  -moz-transition: opacity 0.3s 0s, visibility 0s 0.3s;
			  transition: opacity 0.3s 0s, visibility 0s 0.3s;
			}
			.bts-popup.is-visible {
			  opacity: 1;
			  visibility: visible;
			  -webkit-transition: opacity 0.3s 0s, visibility 0s 0s;
			  -moz-transition: opacity 0.3s 0s, visibility 0s 0s;
			  transition: opacity 0.3s 0s, visibility 0s 0s;
			}

			.bts-popup-container {
			  position: relative;
			  width: 90%;
			  /*max-width: 400px;*/
			  max-width: 600px;
			  margin: 4em auto;
			  /*background: #a0cff1;*/
			  background: transparent;
			  border-radius: none; 
			  text-align: center;
			  /*box-shadow: 0 0 2px rgba(0, 0, 0, 0.2);*/
			  -webkit-transform: translateY(-40px);
			  -moz-transform: translateY(-40px);
			  -ms-transform: translateY(-40px);
			  -o-transform: translateY(-40px);
			  transform: translateY(-40px);
			  /* Force Hardware Acceleration in WebKit */
			  -webkit-backface-visibility: hidden;
			  -webkit-transition-property: -webkit-transform;
			  -moz-transition-property: -moz-transform;
			  transition-property: transform;
			  -webkit-transition-duration: 0.3s;
			  -moz-transition-duration: 0.3s;
			  transition-duration: 0.3s;
			}
			.bts-popup-container img {
			  /*padding: 20px 0 0 0;*/
			  padding: 30px 0 0 0;
			}
			.bts-popup-container p {
				color: white;
			  padding: 10px 40px;
			}
			/*.bts-popup-container .bts-popup-button {
			  padding: 5px 25px;
			  border: 2px solid white;
				display: inline-block;
			  margin-bottom: 10px;
			}*/

			.bts-popup-container a {
			  color: red;
			  text-decoration: none;
			  text-transform: uppercase;
			}

			.bts-popup-container .bts-popup-close {
			  /*position: absolute;
			  top: 8px;
			  right: 8px;
			  width: 30px;
			  height: 30px;*/
			  position: absolute;
			  top: 16px;
			  right: 34px;
			  width: 30px;
			  height: 30px;
			  background: #000;
			  border-radius: 22px;
			}
			.bts-popup-container .bts-popup-close::before, .bts-popup-container .bts-popup-close::after {
			  content: '';
			  position: absolute;
			  top: 12px;
			  width: 16px;
			  height: 3px;
			  background-color: white;
			}
			.bts-popup-container .bts-popup-close::before {
			  -webkit-transform: rotate(45deg);
			  -moz-transform: rotate(45deg);
			  -ms-transform: rotate(45deg);
			  -o-transform: rotate(45deg);
			  transform: rotate(45deg);
			  left: 8px;
			}
			.bts-popup-container .bts-popup-close::after {
			  -webkit-transform: rotate(-45deg);
			  -moz-transform: rotate(-45deg);
			  -ms-transform: rotate(-45deg);
			  -o-transform: rotate(-45deg);
			  transform: rotate(-45deg);
			  right: 6px;
			  top: 13px;
			}
			.is-visible .bts-popup-container {
			  -webkit-transform: translateY(0);
			  -moz-transform: translateY(0);
			  -ms-transform: translateY(0);
			  -o-transform: translateY(0);
			  transform: translateY(0);
			}
			@media only screen and (min-width: 1170px) {
			  .bts-popup-container {
			    margin: 8em auto;
			  }
			}

			.blink {
				animation: blink-animation 1s steps(5, start) infinite;
				-webkit-animation: blink-animation 1s steps(5, start) infinite;
			}

			@keyframes blink-animation {
				to {
				visibility: hidden;
				}
				
			}

			@-webkit-keyframes blink-animation {
				to {
					visibility: hidden;
				}
			}

                        
	</style>

</head>
<!-- <body oncontextmenu="return false" onkeydown="return false;" onmousedown="return false;"> -->

<body>

<div id="wrapper">
	<?php $this->load->view('frontpage/nav_menu_front'); ?>

		<!-- start slider -->
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<!-- Slider -->
					<div id="main-slider" class="main-slider flexslider" style="z-index: -0;">
						<ul class="slides">
							<li>
								<img src="<?php echo base_url('assets_front/img/slides/flexslider/slide_1.jpg'); ?> " alt="" />
								<!-- <div class="flex-caption">
									<h3>Clean & Fast</h3>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit donec mer lacinia.</p>
														<a href="#" class="btn btn-theme">Learn More</a>
								</div> -->
							</li>
							<li>
								<img src="<?php echo base_url('assets_front/img/slides/flexslider/foto2.png'); ?> " alt="" />
								<!-- <div class="flex-caption">
									<h3>Modern Design</h3>
														<p>Duis fermentum auctor ligula ac malesuada. Mauris et metus odio, in pulvinar urna</p>
														<a href="#" class="btn btn-theme">Learn More</a>
								</div> -->
							</li>
							<li>
								<img src="<?php echo base_url('assets_front/img/slides/flexslider/backround_peksi_gunaraharja.png'); ?> " alt="" />
								<!-- <div class="flex-caption">
									<h3>Fully Responsive</h3>
														<p>Sodales neque vitae justo sollicitudin aliquet sit amet diam curabitur sed fermentum.</p>
														<a href="#" class="btn btn-theme">Learn More</a>
								</div> -->
							</li>
							
						</ul>
					</div>
					<!-- end slider -->
				</div>
			</div>

			<section class="callaction">
				<div class="container">
					<div class="row">
						<div class="col-lg-8">
							<div class="cta-text">
								<h2>Jual Beli Bibit Puyuh <span>Peksi</span> Bibit Burung Puyuh Unggul</h2>
								<p>Peksi Gunaraharja perusahaan jual beli bibit puyuh dan siap mengirimkan bibit puyuh ke keseluruh penjuru pulau Indonesia</p>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="cta-btn">
								<a href="#hubungi_langsung" class="btn btn-theme btn-lg">Kontak Sales Sekarang <i class="fa fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>


		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="text-center">
						<h3>Keunggulan <strong>Bibit Puyuh</strong> Peksi</h3>
					</div>
				</div>
				<div class="col-lg-6">
					<div id="integration-list">
						<ul>
							<li>
								<div class="">
									<div>
										<h2>Hasil Perkawinan Out Breeding</h2>
									</div>
								</div>
							</li>
							<li>
								<div class="">
									<div>
										<h2>Bibit Puyuh berumur nol hari, dengan ukuran seragam</h2>
									</div>
								</div>
							<li>
								<div class="">
									<div>
										<h2>Bibit Puyuh telah divaksinasi secara modern</h2>
									</div>
								</div>
							<li>
								<div class="">
									<div>
										<h2>Dijamin jumlah Bibit Puyuh betina lebih banyak <strong>(99%)</strong></h2>
									</div>
								</div>
							<li>
								<div class="">
									<div>
										<h2>Mortalitas saat pembesaran sangat kecil</h2>
									</div>
								</div>
							</li>
							<li>
								<div class="">
									<div>
										<h2>Mulai berproduksi lebih awal (38 -40 hari)</h2>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-6">
					<div id="integration-list">
						<ul>
							<li>
								<div class="">
									<div>
										<h2>Produktifitas lebih tinggi</h2>
									</div>
								</div>
							</li>
							<li>
								<div class="">
									<div>
										<h2>Masa puncak produksi lebih panjang</h2>
									</div>
								</div>
							<li>
								<div class="">
									<div>
										<h2>Umur produksi lebih lama</h2>
									</div>
								</div>
							<li>
								<div class="">
									<div>
										<h2>Mortalitas selama masa produksi rendah</h2>
									</div>
								</div>
							<li>
								<div class="">
									<div>
										<h2>Menghasilkan telur dengan ukuran standar</h2>
									</div>
								</div>
							</li>
							<li>
								<div class="">
									<div>
										<h2>Body weigth saat afkir ideal</h2>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!-- divider -->
		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="solidline">
				</div>
			</div>
		</div>
		</div>
		<!-- end divider -->

	    <div class="container">
			<div class="row">				
				<div class="col-md-12 col-lg-9">
					<div class="text-center">
						<h3>Jadwal Pengiriman Bibit Puyuh</h3>
					</div>
					<div id='calendar'></div>
				</div>
				<div class="col-md-12 col-lg-3">
					<fieldset>
						<legend>Keterangan</legend>
						<div class="btn-group">
						  <button class="btn btn-success" style="border-radius: 0px; background-color: red;" disabled type="button">&nbsp;</button>
						  <button class="btn btn-default" style="border-radius: 0px; background-color: transparent; border-style: none;" disabled type="button">Bibit Puyuh yang terjual</button>
						</div>
						<br><br>
						<div class="btn-group">
						  <button class="btn btn-success" style="border-radius: 0px; background-color: blue;" disabled type="button">&nbsp;</button>
						  <button class="btn btn-default" style="border-radius: 0px; background-color: transparent; border-style: none;" disabled type="button">Bibit Puyuh yang belum terjual</button>
						</div>
					</fieldset>
				</div>
				
				<div class="col-md-12 col-lg-3">
					<br><br>
					<img src="<?php echo base_url('assets_front/ads/peksi_promo_agustus.jpg'); ?>" class="img-responsive" alt="Ads">
				</div>
			</div>
		</div>

		<!-- divider -->
		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="solidline">
				</div>
			</div>
		</div>
		</div>
		<!-- end divider -->

		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="text-center">
					<h2>Tahapan membeli Bibit Puyuh Peksi</h2>					
					<p>Tahapan ini yang peksi gunakan untuk melakukan pengiriman barang pada pelanggan peksi lainnya, barang kiriman menjadi aman dan kualitas Bibit Puyuh masih terjaga</p>
				</div>
			</div>		
		</div>
		</div>
		
		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-sm-3 col-md-3 col-lg-3">
						<div class="">
							<div class="aligncenter">								
								<div class="icon">
								<i class="fa fa-laptop fa-5x"></i>
								</div>
								<h4>Daftar Pelanggan</h4>
								<p>
									Pendaftaran menjadi pelanggan peksi dapat di lakukan melalui web peksi, agar peksi memiliki data atau indentitas pelanggan yang akurat, baik nama , alamat serta nomor ponsel yang dapat dihubungi oleh pihak peksi, dengan data ini pelanggan dapat melakukan order kembali langsung tanpa mendaftar lagi karena data pelanggan sudah ada di kami , <strong>pendaftaran pelanggan di lakukan 1x</strong>
								</p>
							</div>
						</div>
					</div>
					<div class="col-sm-3 col-md-3 col-lg-3">
						<div class="">
							<div class="aligncenter">								
								<div class="icon">
								<i class="fa fa fa-phone fa-5x"></i>
								</div>
								<h4>Konfirmasi Admin</h4>
								<p>
									Pihak peksi akan menghubungi pelanggan guna untuk mencocokan data baik nama, alamat dan jumlah pesanan serta nominal yang harus di bayar oleh pelanggan.
								</p>
							</div>
						</div>
					</div>
					<div class="col-sm-3 col-md-3 col-lg-3">
						<div class="">
							<div class="aligncenter">								
								<div class="icon">
								<i class="fa fa-thumbs-o-up fa-5x" aria-hidden="true"></i>
								</div>
								<h4>Kesepakatan Bibit Puyuh</h4>
								<p>
									Data pelanggan dan total pengiriman bibit puyuh sudah di anggap valid oleh pihak peksi, sepakat <strong> untuk menentukan jadwal pengiriman</strong> agar bibit puyuh tepat pada waktu pengiriman yang telah di tentukan.
								</p>
							</div>
						</div>
					</div>
					<div class="col-sm-3 col-md-3 col-lg-3">
						<div class="">
							<div class="aligncenter">								
								<div class="icon">
								<i class="fa fa-truck fa-5x"></i>
								</div>
								<h4>Kirim Bibit Puyuh</h4>
								<p>
									Pengiriman bibit puyuh kepada pelanggan tengan waktu yang telah ditetapkan, <strong>Terimakasi sudah menjadi pelanggan bibit puyuh peksi</strong>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>

		<!-- divider -->
		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="solidline">
				</div>
			</div>
		</div>
		</div>
		<!-- end divider -->

		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="text-center">
					<h2>Apa kata pelanggan peksi</h2>					
					<p>Berikut pelanggan yang sudah merasakan kualitas DOQ dari peksi</p>
				</div>
			</div>		
		</div>
		</div>

		<div class="container">
		  <div class='row'>
		    <div class='col-md-offset-2 col-md-8'>
		      <div class="carousel slide" data-ride="carousel" id="quote-carousel">
		        <ol class="carousel-indicators" style="z-index: auto;">
		          <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
		          <li data-target="#quote-carousel" data-slide-to="1"></li>
		          <li data-target="#quote-carousel" data-slide-to="2"></li>
		        </ol>
		        <div class="carousel-inner">
		          <div class="item active">
		            <blockquote>
		              <div class="row">
		                <div class="col-sm-3 text-center">
		                  <img class="img-circle" src="<?php echo base_url('assets_front/testimoni/DSC00466.JPG'); ?>" style="width: 100px;height:100px;">
		                </div>
		                <br>
		                <div class="col-sm-9">
		                  <p>Kualitas DOQ dari peksi memang top, puyuhnya sehat pengiriman tepat waktu</p>
		                  <small></small>
		                </div>
		              </div>
		            </blockquote>
		          </div>
		          <div class="item">
		            <blockquote>
		              <div class="row">
		                <div class="col-sm-3 text-center">
		                  <img class="img-circle" src="<?php echo base_url('assets_front/testimoni/DSC00469.JPG'); ?>" style="width: 100px;height:100px;">
		                </div>
		                <br>
		                <div class="col-sm-9">
		                  <p>Pertama kali membeli doq di peksi, pengiriman tepat waktu, doq yang di hasilkan lebih besar di bandingkan doq lainnya</p>
		                  <small></small>
		                </div>
		              </div>
		            </blockquote>
		          </div>
		          <div class="item">
		            <blockquote>
		              <div class="row">
		                <div class="col-sm-3 text-center">
		                  <img class="img-circle" src="<?php echo base_url('assets_front/testimoni/DSC00491.JPG'); ?>" style="width: 100px;height:100px;">
		                </div>
		                <br>
		                <div class="col-sm-9">
		                  <p>Saya sudah lama berlangganan doq di peksi gunaraharja, itu loh doq kualitasnya lebih bagus dibanding yang lain</p>
		                  <small></small>
		                </div>
		              </div>
		            </blockquote>
		          </div>
		          <div class="item">
		            <blockquote>
		              <div class="row">
		                <div class="col-sm-3 text-center">
		                  <img class="img-circle" src="<?php echo base_url('assets_front/testimoni/DSC00486.JPG'); ?>" style="width: 100px;height:100px;">
		                </div>
		                <br>
		                <div class="col-sm-9">
		                  <p>Bibit nya barus, telur yang di hasilkan juga lebih baik yang saya perkirakan</p>
		                  <small></small>
		                </div>
		              </div>
		            </blockquote>
		          </div>
		        </div>
		        
		        <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
		        <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
		      </div>                          
		    </div>
		  </div>
		</div>

		<!-- divider -->
		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="solidline">
				</div>
			</div>
		</div>
		</div>
		<!-- end divider -->

		<div class="container">
		  <div class='row'>
		    <div class="col-lg-12">
		    	<div class="text-center">
					<h1>Artikel Peksi Terbaru</h1>					
				</div>
			 	<div class="index-content">
				    <div class="container-fluid">
				        <?php foreach ($data_aktivitas as $key => $data_artikel): ?>
				        		<?php if ($key <= 3): ?>
								<a href="<?php echo base_url('artikel/'.$data_artikel->kata_kunci); ?>">
									<div class="col-lg-3">
										<div class="card" style="height: 320px; border-bottom-left-radius: 0px; border-bottom-right-radius: 0px;">
											<?php if (!empty($data_artikel->cover_berita)): ?>
												<img style="height: 200px;" src="<?php echo base_url('assets/upload_image/'.$data_artikel->cover_berita);?>">
											<?php else: ?>
												<img style="height: 200px;" src="<?php echo base_url('assets/upload_image/default.jpg');?>">
											<?php endif ?>
											<h5><?php echo $data_artikel->judul_berita; ?></h5>
										</div>
									</div>
								</a>
								<?php endif ?>
						<?php endforeach ?>
				    </div>
				</div>
			</div>
		  </div>
		</div>


	<?php $this->load->view('frontpage/footer.php'); ?>

</div>
<!-- <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a> -->


	<?php require_once(APPPATH .'views/include_front/head_script.php'); ?>
	<script>
		var d = new Date();
		var n = d.getMonth();
		var y = d.getFullYear();
				// Y        M      D
		var date = y+'-'+(n+1)+'-'+10;

		$(document).ready(function() {
			var base_url = window.location.origin;
			$('#calendar').fullCalendar({
				monthNames: ['Januari','Febuary','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'],
			    monthNamesShort: ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sep','Okt','Nov','Des'],
			    dayNames: ['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu'],
			    dayNamesShort: ['Min','Sen','Sel','Rab','Kam','Jum','Sab'],
			    weekNumbers: true,
			    
				defaultDate: date,
				editable: false,
				eventLimit: "more", // allow "more" link when too many events
				events: base_url+'/page/json_periode'
			});
		});
	</script>

	<div class="bts-popup" role="alert" style="z-index: 1;">
	    <div class="bts-popup-container">
	      <img src="<?php echo base_url('assets_front/ads/peksi_promo_agustus.jpg'); ?>" alt="" width="80%" />
	      	<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim officiis, dignissimos voluptates atque dolorem? Ipsam reiciendis, nemo sed harum quis nobis culpa amet aperiam nisi, iusto, aliquam aspernatur, suscipit expedita.</p> -->
			<!-- <div class="bts-popup-button">
			       <a href="https://goo.gl/RTmR47" target="_blank"><span class="blink">DAFTAR SEKARANG</span></a>
	         	</div> -->
	        <a href="#0" class="bts-popup-close img-replace">Close</a>
	    </div>
	</div>

	<script type="text/javascript">
		jQuery(document).ready(function($){
	  
			  window.onload = function (){
			    $(".bts-popup").delay(1000).addClass('is-visible');
				}
			  
				//open popup
				$('.bts-popup-trigger').on('click', function(event){
					event.preventDefault();
					$('.bts-popup').addClass('is-visible');
				});
				
				//close popup
				$('.bts-popup').on('click', function(event){
					if( $(event.target).is('.bts-popup-close') || $(event.target).is('.bts-popup') ) {
						event.preventDefault();
						$(this).removeClass('is-visible');
					}
				});
				//close popup when clicking the esc keyboard button
				$(document).keyup(function(event){
			    	if(event.which=='27'){
			    		$('.bts-popup').removeClass('is-visible');
				    }
			    });
		});
	</script>
</body>
</html>
