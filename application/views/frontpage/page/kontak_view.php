﻿<!DOCTYPE html>
<html>
<head lang="en">
	<?php require_once(APPPATH .'views/include_front/head_style.php'); ?>
</head>
<body>

<div id="wrapper">
	
	<?php $this->load->view('frontpage/nav_menu_front'); ?>

	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url(); ?> "><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
					<li class="active">Kontak</li>
				</ul>
			</div>
		</div>
	</div>
	</section>
	<section id="content">    
        <div class="container">
        	<div class="row">
        		<div class="col-sm-9 col-lg-9">
        			<section id="content">
						<div class="map">
							<div id="google-map" data-latitude="-7.720765" data-longitude="110.473063"></div>
						</div>
						<div class="container">
							<div class="row">
								<div class="12">
									
								<div class="row text-center">
									<h2 align="center">Pemesanan Bibit Puyuh Secara Langusung</h2>
									<div class="col-lg-4">
										<div class="contact-detail-box">
							                 <img class="img-circle" src="<?php echo base_url('assets_front/img/avatar_pengiriman_lansung/mas_taufa.png'); ?>" style="width: 100px;height:100px;">
											<h4>Area Pemesanan Luar Pulau Jawa</h4>
											<p>Sulawesi, Kalimantan, Sumatra, Papua Dll</p>
											<h4 class="text-muted">Thofan : 0823-2499-8546</h4>
											<a class="btn btn-info" href="tel:082324998546" role="button"><i class="fa fa-phone"></i> Telepon</a>
											<a class="btn btn-success" target="_blank" href="https://goo.gl/EA8rSx" role="button"> Whatsapp</a>
											<br><br>
										</div>
									</div><!-- end col -->
									<div class="col-lg-4">
										<div class="contact-detail-box">
							                 <img class="img-circle" src="<?php echo base_url('assets_front/img/avatar_pengiriman_lansung/mas_budi.jpg'); ?>" style="width: 100px;height:100px;">
											<h4>Area Pemesanan Pulau Jawa</h4>
											<p>Seluruh Jawa Barat Sekitarnya</p>
											<h4 class="text-muted">Budi : 0815-6872-657</h4>
											<a class="btn btn-info" href="tel:08156872657" role="button"><i class="fa fa-phone"></i> Telepon</a>
											<a class="btn btn-success" target="_blank" href="https://api.whatsapp.com/send?phone=628156872657&text=Saya%20Mau%20Order" role="button"> Whatsapp</a>
											<br><br>
										</div>
									</div><!-- end col -->
									<div class="col-lg-4">
										<div class="contact-detail-box">
											<img class="img-circle" src="<?php echo base_url('assets_front/img/avatar_pengiriman_lansung/mas_rudi.png'); ?>" style="width: 100px;height:100px;">
											<h4>Area Pemesanan Pulau Jawa</h4>
											<p>Seluruh Jogja dan Jawa Tengah Sekitarnya</p>
											<h4 class="text-muted">Rudi : 0822-2154-5855</h4>
											<a class="btn btn-info" href="tel:082221545855" role="button"><i class="fa fa-phone"></i> Telepon</a>
											<a class="btn btn-success" target="_blank" href="https://api.whatsapp.com/send?phone=6282221545855&text=Saya%20Mau%20Order" role="button"> Whatsapp</a>
											<br><br>
										</div>
									</div><!-- end col -->

									<div class="col-lg-4">
										<div class="contact-detail-box">
							                 <img class="img-circle" src="<?php echo base_url('assets_front/img/avatar_pengiriman_lansung/bayu_sales.jpeg'); ?>" style="width: 100px;height:100px;">
											<h4>Area Pemesanan Pulau Jawa Timur 1</h4>
											<p>MALANG, PASURUAN, PROBOLINGGO, LUMAJANG, BANYUWANGI </p>
											<h4 class="text-muted">Bayu : 0812-3310-2173</h4>
											<a class="btn btn-info" href="tel:0812-3310-2173" role="button"><i class="fa fa-phone"></i> Telepon</a>
											<a class="btn btn-success" target="_blank" href="https://api.whatsapp.com/send?phone=6281233102173&text=Saya%20Mau%20Order" role="button"> Whatsapp</a>
											<br><br>
										</div>
									</div><!-- end col -->
									<div class="col-lg-4">
										<div class="contact-detail-box">
											<img class="img-circle" src="<?php echo base_url('assets_front/img/avatar_pengiriman_lansung/andi_sales.jpeg'); ?>" style="width: 100px;height:100px;">
											<h4>Area Pemesanan Pulau Jawa Timur 2</h4>
											<p>Seluruh Jawa Timur</p>
											<h4 class="text-muted">Andi  : 0813-3333-6965</h4>
											<a class="btn btn-info" href="tel:081333336965" role="button"><i class="fa fa-phone"></i> Telepon</a>
											<a class="btn btn-success" target="_blank" href="https://api.whatsapp.com/send?phone=6281333336965&text=Saya%20Mau%20Order" role="button"> Whatsapp</a>
											<br><br>
										</div>
									</div><!-- end col -->

									<div class="col-lg-4">
										<!-- <div class="contact-detail-box">
											<img class="img-circle" src="<?php echo base_url('assets_front/img/avatar_pengiriman_lansung/mas_ifnu.jpeg'); ?>" style="width: 100px;height:100px;">
											<h4>Area Pemesanan Pulau Jawa Timur 1</h4>
											<p>TULUNGAGUNG, TRENGGAOLEK, PONOROGO, PACITAN, KEDIRI</p>
											<h4 class="text-muted">Ifnu : 0857-4211-7363</h4>
											<a class="btn btn-info" href="tel:085742117363" role="button"><i class="fa fa-phone"></i> Telepon</a>
											<a class="btn btn-success" target="_blank" href="https://goo.gl/GrGQow" role="button"> Whatsapp</a>
											<br><br>
										</div> -->
										<div class="contact-detail-box">
											<img class="img-circle" src="<?php echo base_url('assets_front/img/avatar_pengiriman_lansung/pak_dwi.png'); ?>" style="width: 100px;height:100px;">
											<h4>Supervisor / Support</h4>
											<p>Koordinator Sales & Konsultasi Puyuh</p>
											<h4 class="text-muted">Drh. Dwi J : 0812-2948-8897</h4>
											<a class="btn btn-info" href="tel:081229488897" role="button"><i class="fa fa-phone"></i> Telepon</a>
											<a class="btn btn-success" target="_blank" href="https://goo.gl/qLPoGd" role="button"> Whatsapp</a>
											<br><br>
										</div>
									</div> <!-- end col -->

									<!-- <div class="col-md-6 col-md-offset-3">
										<div class="contact-detail-box">
											<img class="img-circle" src="<?php echo base_url('assets_front/img/avatar_pengiriman_lansung/pak_dwi.png'); ?>" style="width: 100px;height:100px;">
											<h4>Supervisor / Support</h4>
											<p>Koordinator Sales & Konsultasi Puyuh</p>
											<h4 class="text-muted">Drh. Dwi J : 0812-2948-8897</h4>
											<a class="btn btn-info" href="tel:081229488897" role="button"><i class="fa fa-phone"></i> Telepon</a>
											<a class="btn btn-success" target="_blank" href="https://goo.gl/qLPoGd" role="button"> Whatsapp</a>
											<br><br>
										</div>
									</div> -->
								</div>

								    
								</div>
							</div>
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<h2 align="center">Kontak pesan <small>PT. Peksi Gunaraharja</small></h2>

								    <?php if (isset($_SESSION['sendmessage'])): ?>
									<div class="alert alert-success" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">×</span>
										</button>
										<?php echo $_SESSION['sendmessage'] ?>
									</div>
									<?php endif ?>

					                <form action="<?php echo base_url().'page/action_kontak_pelanggan'; ?> " method="post" role="form" class="contactForm">
					                    <div class="form-group">
					                        <input type="text" name="nama_penulis" class="form-control" id="nama_penulis" placeholder="Nama Penulis" required />
					                        <div class="validation"></div>
					                    </div>
					                    <div class="form-group">
					                        <input type="email" class="form-control" name="email_penulis" id="email_penulis" placeholder="Alamat Email" />
					                        <div class="validation"></div>
					                    </div>
					                    <div class="form-group">
					                        <input type="text" class="form-control" name="nomor_telepon" id="nomor_telepon" placeholder="Nomor telepon" required />
					                        <div class="validation"></div>
					                    </div>
					                    <div class="form-group">
					                        <input type="text" class="form-control" name="alamat_lengkap" id="alamat_lengkap" placeholder="Alamat Lengkap" required />
					                        <div class="validation"></div>
					                    </div>
					                    <div class="form-group">
					                        <textarea class="form-control" name="isi_pesan" rows="5" required placeholder="Isi Pesan"></textarea>
					                        <div class="validation"></div>
					                    </div>
					                    
					                    <div class="text-center"><button type="submit" class="btn btn-theme btn-block btn-md">Kirim Pesan Sekarang</button></div>
					                </form>
								</div>
							</div>
						</div>
					</section>
        		</div>
        		<div class="col-sm-3 col-lg-3">
        			<address>
						<h3><strong>PT. Peksi Gunaraharja</strong></h3><br>
						<img src="<?php echo base_url('assets_front/img/kantor.png'); ?> " class="img-responsive" alt="Image">
						Jl. Cangkringan Km 5, Ngasem, Selomortani, Kalasan, Sleman<br> Yogyakarta 55571</address>
					<p>
						<i class="fa fa-phone"></i> (0274) 2850063 <br>
						<i class="fa fa-clock-o"></i> Jam Buka : 09:00 - 16:00 
						 peksigunaraharja@gmail.com
					</p>
        		</div>
        	</div>
        </div>
    </section>
	

	<?php $this->load->view('frontpage/footer.php'); ?>
</div>
  <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>

	<?php require_once(APPPATH .'views/include_front/head_script.php'); ?>
</body>
</html>