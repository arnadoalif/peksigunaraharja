<!DOCTYPE html>
<html>
<head lang="en">
	<?php require_once(APPPATH .'views/include_front/head_style.php'); ?>
</head>
<body>

<div id="wrapper">
	
	<?php $this->load->view('frontpage/nav_menu_front'); ?>

	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url(); ?> "><i class="fa fa-home"></i></a></li>
					<li class="active">Download Brosur</li>
				</ul>
			</div>
		</div>
	</div>
	</section>

	<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div id="filters-container" class="cbp-l-filters-button">
					<div data-filter="*" class="cbp-filter-item-active cbp-filter-item">All<div class="cbp-filter-counter"></div></div>
					<div data-filter=".1" class="cbp-filter-item">Terbaru<div class="cbp-filter-counter"></div></div>
				</div>
				

				<div id="grid-container" class="cbp-l-grid-projects">
					<ul>
						<?php foreach ($data_browsur as $dt_browsur): ?>
						<li class="cbp-item <?php echo $dt_browsur->status ?>">
							<div class="cbp-caption">
								<div class="cbp-caption-defaultWrap">
									<img src="<?php echo base_url('assets/upload_browsur/'.$dt_browsur->file_name);?> " alt="" />
								</div>
								<div class="cbp-caption-activeWrap">
									<div class="cbp-l-caption-alignCenter">
										<div class="cbp-l-caption-body">
										
											<a href="<?php echo base_url('assets/upload_browsur/'.$dt_browsur->file_name);?> " class="cbp-lightbox cbp-l-caption-buttonRight" data-title="<?php echo $dt_browsur->file_name ?> ">Perbesar</a>
											<a href="<?php echo base_url('page/download/'.$dt_browsur->file_name) ?> " class=" cbp-l-caption-buttonRight">Download</a>
										</div>
									</div>
								</div>
							</div>
							<div class="cbp-l-grid-projects-title"><?php echo $dt_browsur->nama_browsur ?></div>
							<div class="cbp-l-grid-projects-desc"><?php echo date("d M Y", strtotime($dt_browsur->tanggal_upload)); ?></div>
						</li>
						<?php endforeach ?>

					</ul>
				</div>
				
				<!-- <div class="cbp-l-loadMore-button">
					<a href="#" class="cbp-l-loadMore-button-link">LOAD MORE</a>
				</div> -->

			</div>
		</div>
	</div>
	</section>

	
	<?php $this->load->view('frontpage/footer.php'); ?>
</div>
  <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>

	<?php require_once(APPPATH .'views/include_front/head_script.php'); ?>
</body>
</html>