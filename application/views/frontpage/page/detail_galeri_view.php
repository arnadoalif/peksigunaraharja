<!DOCTYPE html>
<html>
<head lang="en">
	<?php require_once(APPPATH .'views/include_front/head_style.php'); ?>
	<style type="text/css" media="screen">
		.gal {
	          -webkit-column-count: 3; /* Chrome, Safari, Opera */
              -moz-column-count: 3; /* Firefox */
               column-count: 3;
	    }	
	    .gal img{ width: 100%; padding: 1px;}
            @media (max-width: 500px) {
		
		.gal {
	        -webkit-column-count: 1; /* Chrome, Safari, Opera */
            -moz-column-count: 1; /* Firefox */
             column-count: 1;
	    }
		}
	</style>
</head>
<body>

<div id="wrapper">
	
	<?php $this->load->view('frontpage/nav_menu_front'); ?>

	<section id="inner-headline">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
							<li><a href="<?php echo base_url('galeri'); ?>">Galeri</a><i class="icon-angle-right"></i></li>
							<li class="active"><?php echo($data_detail_gallery[0]->nama_event); ?></li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section id="content">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<?php foreach ($data_detail_gallery as $dt_gallery): ?>
						<h3><?php echo $dt_gallery->nama_event; ?></h3>
						
						<div class="col-md-12">
							<div class="row">
								<hr>
								<div class="gal">
									<?php 
										$this->load->model('Gallery_model');
										$m_gallery = new Gallery_model();

										$data_album = $m_gallery->gallery_view_by_kode_gallery('tbl_detail_gallery', $dt_gallery->kode_gallery)->result();
									 ?>
									 <?php foreach ($data_album as $dt_album): ?>
											<img src="<?php echo base_url('storage_img/cover_album/'.$dt_album->nama_image); ?>" alt="<?php echo $dt_gallery->nama_event ?>">
									<?php endforeach ?>
								</div>
								
							</div>
						</div>



						<p>
							<?php echo $dt_gallery->deskripsi_gallery; ?>

						</p>
						<?php endforeach ?>
					</div>
				</div>




			</div>
		</section>

	<?php $this->load->view('frontpage/footer.php'); ?>
</div>
  <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>

	<?php require_once(APPPATH .'views/include_front/head_script.php'); ?>
</body>
</html>