<!DOCTYPE html>
<html>
<head lang="en">
	<?php require_once(APPPATH .'views/include_front/head_style.php'); ?>
</head>
<body>

<div id="wrapper">
	
	<?php $this->load->view('frontpage/nav_menu_front'); ?>

	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url(); ?> "><i class="fa fa-home"></i></a></li>
					<li class="active">Galeri</li>
				</ul>
			</div>
		</div>
	</div>
	</section>
	<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<!-- <h4 class="heading">Galeri</h4> -->

				<!-- <div id="filters-container" class="cbp-l-filters-button">
					<div data-filter="*" class="cbp-filter-item-active cbp-filter-item">All<div class="cbp-filter-counter"></div></div>
					<div data-filter=".penghargaan" class="cbp-filter-item">Penghargaan<div class="cbp-filter-counter"></div></div>
					<div data-filter=".video" class="cbp-filter-item">Video<div class="cbp-filter-counter"></div></div>
				</div> -->
				

				<div id="grid-container" class="cbp-l-grid-projects">
					<ul>
						<?php foreach ($data_gallery as $dt_gallery): ?>
						<li class="cbp-item">
							<div class="cbp-caption">
								<div class="cbp-caption-defaultWrap">
									<img src="<?php echo base_url('storage_img/cover_gallery/'.$dt_gallery->cover_album_name); ?> " alt="" />
								</div>
								<div class="cbp-caption-activeWrap">
									<div class="cbp-l-caption-alignCenter">
										<div class="cbp-l-caption-body">
											<a href="<?php echo base_url('storage_img/cover_gallery/'.$dt_gallery->cover_album_name); ?>" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="<?php echo $dt_gallery->nama_event ?>">Perbesar</a>
										</div>
									</div>
								</div>
							</div>
							<div class="cbp-l-grid-projects-title"><a href="<?php echo base_url('galeri/'.$dt_gallery->kata_kunci) ?>" data-title="<?php echo $dt_gallery->nama_event ?>"><?php echo $dt_gallery->nama_event ?></a></div>
							<div class="cbp-l-grid-projects-desc"><i class="fa fa-calendar"></i> <?php echo $dt_gallery->create_at ?></div>
						</li>
						<?php endforeach ?>
					</ul>
				</div>
				
				<!-- <div class="cbp-l-loadMore-button">
					<a href="#" class="cbp-l-loadMore-button-link">LOAD MORE</a>
				</div> -->

			</div>
		</div>
	</div>
	</section>

	
	<?php $this->load->view('frontpage/footer.php'); ?>
</div>
  <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>

	<?php require_once(APPPATH .'views/include_front/head_script.php'); ?>
</body>
</html>