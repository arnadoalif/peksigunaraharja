<footer>
		<section class="callaction">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
					    <div class="cta-text">
							<h2 align="center">Kualitas sudah terjamin bibit puyuh (99%) betina, jangan pikir panjang untuk jadi pelanggan berikutnya <span>PEKSI</span>, Pemesanan bibit puyuh kami antar dengan aman dan cepat</h2>
						</div>
					   
						<div class="col-md-6 col-md-offset-3">
								<a href="<?php echo base_url('page/pelanggan'); ?>" class="btn btn-block btn-theme btn-lg">INGIN JADI PELANGGAN </i></a>
						</div>
						
					</div>
				</div>
			</div>
		</section>

		<div class="container" id="hubungi_langsung">
			<div class="row">
				<div class="col-md-12">
					
					<div class="row text-center">
						<h2 align="center">Pemesanan Bibit Puyuh Secara Langusung</h2>
						<div class="col-lg-4">
							<div class="contact-detail-box">
				                 <img class="img-circle" src="<?php echo base_url('assets_front/img/avatar_pengiriman_lansung/mas_taufa.png'); ?>" style="width: 100px;height:100px;">
								<h4>Area Pemesanan Luar Pulau Jawa</h4>
								<p>Sulawesi, Kalimantan, Sumatra, Papua Dll</p>
								<h4 class="text-muted">Thofan : 0823-2499-8546</h4>
								<a class="btn btn-info" href="tel:082324998546" role="button"><i class="fa fa-phone"></i> Telepon</a>
								<a class="btn btn-success" target="_blank" href="https://goo.gl/EA8rSx" role="button"> Whatsapp</a>
								<br><br>
							</div>
						</div><!-- end col -->
						<div class="col-lg-4">
							<div class="contact-detail-box">
				                 <img class="img-circle" src="<?php echo base_url('assets_front/img/avatar_pengiriman_lansung/mas_budi.jpg'); ?>" style="width: 100px;height:100px;">
								<h4>Area Pemesanan Pulau Jawa</h4>
								<p>Seluruh Jawa Barat Sekitarnya</p>
								<h4 class="text-muted">Budi : 0815-6872-657</h4>
								<a class="btn btn-info" href="tel:08156872657" role="button"><i class="fa fa-phone"></i> Telepon</a>
								<a class="btn btn-success" target="_blank" href="https://api.whatsapp.com/send?phone=628156872657&text=Saya%20Mau%20Order" role="button"> Whatsapp</a>
								<br><br>
							</div>
						</div><!-- end col -->
						<div class="col-lg-4">
							<div class="contact-detail-box">
								<img class="img-circle" src="<?php echo base_url('assets_front/img/avatar_pengiriman_lansung/mas_rudi.png'); ?>" style="width: 100px;height:100px;">
								<h4>Area Pemesanan Pulau Jawa</h4>
								<p>Seluruh Jogja dan Jawa Tengah Sekitarnya</p>
								<h4 class="text-muted">Rudi : 0822-2154-5855</h4>
								<a class="btn btn-info" href="tel:082221545855" role="button"><i class="fa fa-phone"></i> Telepon</a>
								<a class="btn btn-success" target="_blank" href="https://goo.gl/ZHbgda" role="button"> Whatsapp</a>
								<br><br>
							</div>
						</div><!-- end col -->

						<div class="col-lg-4">
							<div class="contact-detail-box">
				                 <img class="img-circle" src="<?php echo base_url('assets_front/img/avatar_pengiriman_lansung/bayu_sales.jpeg'); ?>" style="width: 100px;height:100px;">
								<h4>Area Pemesanan Pulau Jawa Timur 1</h4>
								<p>MALANG, PASURUAN, PROBOLINGGO, LUMAJANG, BANYUWANGI </p>
								<h4 class="text-muted">Bayu : 0812-3310-2173</h4>
								<a class="btn btn-info" href="tel:0812-3310-2173" role="button"><i class="fa fa-phone"></i> Telepon</a>
								<a class="btn btn-success" target="_blank" href="https://api.whatsapp.com/send?phone=6281233102173&text=Saya%20Mau%20Order" role="button"> Whatsapp</a>
								<br><br>
							</div>
						</div><!-- end col -->
						<div class="col-lg-4">
							<div class="contact-detail-box">
								<img class="img-circle" src="<?php echo base_url('assets_front/img/avatar_pengiriman_lansung/andi_sales.jpeg'); ?>" style="width: 100px;height:100px;">
								<h4>Area Pemesanan Pulau Jawa Timur 2</h4>
								<p>Seluruh Jawa Timur</p>
								<h4 class="text-muted">Andi  : 0813-3333-6965</h4>
								<a class="btn btn-info" href="tel:081333336965" role="button"><i class="fa fa-phone"></i> Telepon</a>
								<a class="btn btn-success" target="_blank" href="https://api.whatsapp.com/send?phone=6281333336965&text=Saya%20Mau%20Order" role="button"> Whatsapp</a>
								<br><br>
							</div>
						</div><!-- end col -->

						<div class="col-lg-4">
							<!-- <div class="contact-detail-box">
								<img class="img-circle" src="<?php echo base_url('assets_front/img/avatar_pengiriman_lansung/mas_ifnu.jpeg'); ?>" style="width: 100px;height:100px;">
								<h4>Area Pemesanan Pulau Jawa Timur 1</h4>
								<p>TULUNGAGUNG, TRENGGAOLEK, PONOROGO, PACITAN, KEDIRI</p>
								<h4 class="text-muted">Ifnu : 0857-4211-7363</h4>
								<a class="btn btn-info" href="tel:085742117363" role="button"><i class="fa fa-phone"></i> Telepon</a>
								<a class="btn btn-success" target="_blank" href="https://goo.gl/GrGQow" role="button"> Whatsapp</a>
								<br><br>
							</div> -->
							<div class="contact-detail-box">
								<img class="img-circle" src="<?php echo base_url('assets_front/img/avatar_pengiriman_lansung/pak_dwi.png'); ?>" style="width: 100px;height:100px;">
								<h4>Supervisor / Support</h4>
								<p>Koordinator Sales & Konsultasi Puyuh</p>
								<h4 class="text-muted">Drh. Dwi J : 0812-2948-8897</h4>
								<a class="btn btn-info" href="tel:081229488897" role="button"><i class="fa fa-phone"></i> Telepon</a>
								<a class="btn btn-success" target="_blank" href="https://goo.gl/qLPoGd" role="button"> Whatsapp</a>
								<br><br>
							</div>
						</div> 


						<!-- <div class="col-md-6 col-md-offset-3">
							<div class="contact-detail-box">
								<img class="img-circle" src="<?php echo base_url('assets_front/img/avatar_pengiriman_lansung/pak_dwi.png'); ?>" style="width: 100px;height:100px;">
								<h4>Supervisor / Support</h4>
								<p>Koordinator Sales & Konsultasi Puyuh</p>
								<h4 class="text-muted">Drh. Dwi J : 0812-2948-8897</h4>
								<a class="btn btn-info" href="tel:081229488897" role="button"><i class="fa fa-phone"></i> Telepon</a>
								<a class="btn btn-success" target="_blank" href="https://goo.gl/qLPoGd" role="button"> Whatsapp</a>
								<br><br>
							</div>
						</div> -->

						<!-- <div class="col-sm-6">
							<div class="contact-detail-box">
								<img class="img-circle" src="<?php echo base_url('assets_front/img/avatar_pengiriman_lansung/mas_ifnu.jpeg'); ?>" style="width: 100px;height:100px;">
								<h4>Area Pemesanan Pulau Jawa</h4>
								<p>Seluruh Jawa Timur Sekitarnya</p>
								<h4 class="text-muted">Ifnu : 0857-4211-7363</h4>
								<a class="btn btn-info" href="tel:085742117363" role="button"><i class="fa fa-phone"></i> Telepon</a>
								<a class="btn btn-success" target="_blank" href="https://goo.gl/GrGQow" role="button"> Whatsapp</a>
								<br><br>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="contact-detail-box">
								<img class="img-circle" src="<?php echo base_url('assets_front/img/avatar_pengiriman_lansung/pak_dwi.png'); ?>" style="width: 100px;height:100px;">
								<h4>Supervisor / Support</h4>
								<p>Koordinator Sales & Konsultasi Puyuh</p>
								<h4 class="text-muted">Drh. Dwi J : 0812-2948-8897</h4>
								<a class="btn btn-info" href="tel:081229488897" role="button"><i class="fa fa-phone"></i> Telepon</a>
								<a class="btn btn-success" target="_blank" href="https://goo.gl/qLPoGd" role="button"> Whatsapp</a>
								<br><br>
							</div>
						</div> -->
						<!-- end col -->
					</div>

				</div>
			</div>
		</div>

	<div class="container">
		<div class="row">
		    <div class="col-sm-3 col-lg-3">
				<div class="widget">
					<h4>Alamat Perusahaan</h4>
					<address>
						<strong>PT. Peksi Gunaraharja</strong><br>
					Jl. Cangkringan Km 5, Ngasem, Selomartani, Kalasan, Sleman<br> Yogyakarta 55571</address>
					<p>
						<i class="icon-phone"></i> (0274) 2850063 <br>
						<i class="icon-envelope-alt"></i> peksigunaraharja@gmail.com
					</p>
				</div>
			</div>
			<div class="col-sm-3 col-lg-3">
				<div class="widget">
					<h4>Peksi Gunaraharja</h4>
					<ul class="link-list">
						<li><a href="<?php echo base_url('brosur'); ?>">Download Brosur</a></li>
						<li><a href="<?php echo base_url('artikel'); ?>">Artikel</a></li>
						<li><a href="<?php echo base_url('galeri'); ?>">Galeri</a></li>
						<li><a href="<?php echo base_url('kontak'); ?>">Kontak</a></li>
						<li><a href="<?php echo base_url('tentang_kami'); ?>">Tentang Kami</a></li>
					</ul>
				</div>
				
			</div>
			<div class="col-sm-3 col-lg-3">
			    <h4>Feedback Peksi</h4>
				<div class="widget">
				   <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FDOQ-puyuh-impor%2F656995837689480&amp;width&amp;height=800&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true" scrolling="YES" frameborder="0" style="border:none; overflow:hidden; height:135px;" allowtransparency="true"></iframe>
				</div>
			</div>
			
		</div>
	</div>
	<div id="sub-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="copyright">
						<p>Copyright &copy; 2017 - PT. Peksi Gunaraharja Yogyakarta <br> Seluruh material konten yang ada di website ini, baik berupa tulisan dan gambar merupakan hak cipta PT. Peksi Gunaraharja </p>
                        <div class="credits">
                            
                        </div>
					</div>
				</div>
				<div class="col-lg-6">
					<ul class="social-network">
						<li><a href="https://www.facebook.com/jualbibitpuyuh" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
						<li><a href="https://twitter.com/jualburungpuyuh" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	</footer>