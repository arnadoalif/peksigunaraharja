<!-- start header -->
	<header>
			<div class="top">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<ul class="topleft-info">
								<li>PT. PEKSI GUNARAHARJA</li>
							</ul>
						</div>
						<div class="col-md-6">
                        <div id="google_translate_element" class="hidden-xs"></div>
						<div id="sb-search" class="sb-search" style="z-index: -0;">

							<form>
								<input class="sb-search-input" placeholder="Enter your search term..." type="text" value="" name="search" id="search">
								<input class="sb-search-submit" type="submit" value="">
								<span class="sb-icon-search" title="Click to start searching"></span>
							</form>
						</div>
						</div>
					</div>
				</div>
			</div>	
			
        <div class="navbar navbar-default navbar-static-top" style="z-index: auto;">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                     <a class="navbar-brand" href="<?php echo base_url(); ?> "><img src="<?php echo base_url('assets_front/img/logo_peksi.jpg'); ?> " alt="" width="199" height="52" style="height: 75px;width: 79px; margin-top: -18px;" /></a>
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                    	<li class="active"><a href="<?php echo base_url(); ?> ">Beranda</a></li>
                        <li><a href="<?php echo base_url('brosur'); ?> ">Download Brosur</a></li>
                        <li><a href="<?php echo base_url('artikel'); ?> ">Artikel</a></li>
                        <li><a href="<?php echo base_url('galeri'); ?> ">Galeri</a></li>
                        <li><a href="<?php echo base_url('kontak'); ?> ">Kontak</a></li>
                        <li><a href="<?php echo base_url('tentang'); ?> ">Tentang Kami</a></li>
                        <li><a href="<?php echo base_url('lowongan'); ?> ">Lowongan Kerja</a></li>
                        <li><a href="<?php echo base_url('member'); ?> ">Masuk Member</a></li>
                    </ul>
                </div>
            </div>
        </div>
	</header>
	<!-- end header -->
	<section id="featured" class="bg">
	<!-- start slider -->

<!-- <script>
  (function() {
    var cx = '004636339729947288625:3ylg2_r1-h4';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search> -->
