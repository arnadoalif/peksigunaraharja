﻿<!DOCTYPE html>
<html>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<style type="text/css" media="screen">
	.img_bukti {
		max-width: 100%;
	}
</style>
<body class="with-side-menu">

	<?php $this->load->view('administrator/nav_menu'); ?>
	<?php $this->load->view('administrator/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">
			<?php if (isset($_SESSION['message_data'])): ?>
				<div class="alert alert-aquamarine alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['message_data'] ?>
				</div>
				<?php endif ?>

				<?php if (isset($_SESSION['error_data'])): ?>
				<div class="alert alert-danger alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['error_data'] ?>
				</div>
			<?php endif ?>
		
			<section class="card">
				<div class="card-block">
						<div class="row">
							<div class="col-xs-12 col-md-5 col-lg-5">
								<img class="img_bukti" src="<?php echo base_url('assets_pembayaran/upload_transfer/'.$detail_konfirmasi->nama_file_upload) ?>" alt="Bukti Pembayaran Tidak Tersedia">
							</div>
							<div class="col-xs-12 col-md-7 col-lg-7">
								<form action="<?php echo base_url('administrator/action_update_status_konfirmasi_order') ?> " method="POST">
									<input type="hidden" name="kode_order" value="<?php echo $detail_konfirmasi->kode_order ?> ">
									<input type="hidden" name="kode_pelanggan" value="<?php echo $detail_konfirmasi->kode_pelanggan ?> ">
									<input type="hidden" name="kode_transaksi" value="<?php echo $detail_konfirmasi->kode_transaksi ?> ">
									<input type="hidden" name="kode_bukti" value="<?php echo $detail_konfirmasi->kode_bukti ?>">
									<div class="table-responsive">
										<table class="table table-hover">
											<thead>
												<tr>
													<th>Nama Pelanggan</th>
													<th><?php echo $detail_konfirmasi->nama_pelanggan; ?></th>
													<th></th>
												</tr>
												<tr>
													<th>Nomor Telepon</th>
													<th><?php echo $detail_konfirmasi->nomor_telepon; ?></th>
													<th></th>
												</tr>
												<tr>
													<th>Untuk Pembayaran</th>
													<th>
														<?php if ($detail_konfirmasi->pembayaran == "uang_muka"): ?>
															<span class="label label-success">PEMBAYARAN UANG MUKA</span> 
														<?php elseif ($detail_konfirmasi->pembayaran == "uang_sisa"): ?>
															<span class="label label-success">PEMBAYARAN SISA</span> 
														<?php else: ?>
															<span class="label label-success">PEMBAYARAN LUNAS</span> 
														<?php endif ?>
													</th>
													<th>
														<select name="pembayaran" id="input" class="form-control" required="required">
															<option value="">Update Pembayaran</option>
															<option <?php echo $detail_konfirmasi->pembayaran == 'uang_muka' ? 'selected = "selected"': ''; ?> value="uang_muka">Uang Muka ( DP )</option>
															<option <?php echo $detail_konfirmasi->pembayaran == 'uang_sisa' ? 'selected = "selected"': ''; ?> value="uang_sisa">Uang Sisa</option>
															<option <?php echo $detail_konfirmasi->pembayaran == 'lunas' ? 'selected = "selected"': ''; ?> value="lunas">Lunas</option>
														</select>
													</th>
												</tr>
												<tr>
													<th>Jenis Rekening</th>
													<th>
														<?php if ($detail_konfirmasi->jenis_rekening == "bca"): ?>
															BCA (BANK CENTRAL ASIA)
														<?php elseif ($detail_konfirmasi->jenis_rekening == "mandiri"): ?>
															MANDIRI
														<?php else: ?>
															BNI (BANK NEGARA INDONESIA)
														<?php endif ?>	
													</th>
													<th><span class="label label-info"><i class="fa fa-check"></i> Check </span></th>
												</tr>
												<tr>
													<th>Nomor Rekening</th>
													<th><?php echo $detail_konfirmasi->nomor_rekening; ?></th>
													<th><span class="label label-info"><i class="fa fa-check"></i> Check </span></th>
												</tr>
												<tr>
													<th>A/N</th>
													<th><?php echo $detail_konfirmasi->atas_nama; ?></th>
													<th><span class="label label-info"><i class="fa fa-check"></i> Check </span></th>
												</tr>
												<tr>
													<th>Jumlah Order</th>
													<th> <?php echo number_format($detail_konfirmasi->jumlah_order, 0,".","."); ?> Ekor DOQ</th>
													<th></th>
												</tr>
												<tr>
													<th>Harga Satuan</th>
													<th>Rp. <?php echo number_format($detail_konfirmasi->satuan_doq, 0,".","."); ?></th>
													<th></th>
												</tr>
												<tr>
													<th>Uang Muka <?php echo $detail_konfirmasi->presentase; ?> %</th>
													<th>
														<?php if ($detail_konfirmasi->status_bayar_uang_muka == "belum_bayar"): ?>
															<span class="label label-danger">Rp. <?php echo number_format($detail_konfirmasi->uang_muka, 0,".","."); ?></th></span>
														<?php else: ?>
															<span class="label label-success">Rp. <?php echo number_format($detail_konfirmasi->uang_muka, 0,".","."); ?></th></span>
														<?php endif ?>
													<th>
														<?php if ($detail_konfirmasi->status_bayar_uang_muka == "belum_bayar"): ?>
															<select name="status_uang_muka" id="input" class="form-control" required="required">
																<option value="">Bukti Pembayaran ?</option>
																<option <?php echo $detail_konfirmasi->status_bayar_uang_muka == 'lunas' ? 'selected = "selected"': ''; ?> value="lunas">Lunas</option>
																<option <?php echo $detail_konfirmasi->status_bayar_uang_muka == 'belum_bayar' ? 'selected = "selected"': ''; ?> value="belum_bayar">Belum Bayar</option>
															</select>
														<?php else: ?>
															<input type="hidden" name="status_uang_muka" value="<?php echo $detail_konfirmasi->status_bayar_uang_muka ?>">
															<span class="label label-success"><i class="fa fa-check"></i> LUNAS</span>
														<?php endif ?>
													</th>
												</tr>
												<tr>
													<th>Sisa Pembayaran</th>
													<th>
														<?php if ($detail_konfirmasi->status_bayar_uang_sisa == "belum_bayar"): ?>
															<span class="label label-danger">Rp. <?php echo number_format($detail_konfirmasi->sisa_uang, 0,".","."); ?></th></span>
														<?php else: ?>
															<span class="label label-success">Rp. <?php echo number_format($detail_konfirmasi->sisa_uang, 0,".","."); ?></th></span>
														<?php endif ?>
													</th>
													<th>
														<?php if ($detail_konfirmasi->status_bayar_uang_sisa == "belum_bayar"): ?>
														<select name="status_uang_sisa" id="input" class="form-control" required="required">
															<option value="">Bukti Pembayaran ?</option>
															<option <?php echo $detail_konfirmasi->status_bayar_uang_sisa == 'lunas' ? 'selected = "selected"': ''; ?> value="lunas">Lunas</option>
															<option <?php echo $detail_konfirmasi->status_bayar_uang_sisa == 'belum_bayar' ? 'selected = "selected"': ''; ?> value="belum_bayar">Belum Bayar</option>
														</select>
														<?php else: ?>
															<input type="text" name="status_uang_sisa" value="<?php echo $detail_konfirmasi->status_bayar_uang_muka ?>">
															<span class="label label-success"><i class="fa fa-check"></i> LUNAS</span>
														<?php endif ?>
													</th>
												</tr>
												<tr>
													<th>Total Harga</th>
													<th><span class="label label-danger">Rp. <?php echo number_format($detail_konfirmasi->total_harga, 0,".","."); ?></span> </th>
													<th></th>
												</tr>
												<tr>
													<th>Update Status Pembayaran</th>
													<th colspan="2">
															<select name="status_bukti" id="input" class="form-control" required="required">
																<option value="">Bukti Pembayaran ?</option>
																<option <?php echo $detail_konfirmasi->status_bukti == 'menunggu' ? 'selected = "selected"': ''; ?> value="menunggu">Menunggu</option>
																<option <?php echo $detail_konfirmasi->status_bukti == 'valid' ? 'selected = "selected"': ''; ?> value="valid">Bukti Tranfer Cocok</option>
																<option <?php echo $detail_konfirmasi->status_bukti == 'unvalid' ? 'selected = "selected"': ''; ?> value="unvalid">Bukti Tranfer Tidak Cocok</option>
															</select>
													</th>
												</tr>
											</thead>
										</table>
									</div>
									<br><br>
									<button type="submit" class="btn btn-success">Simpan Konfirmasi</button>
									<button type="button" class="btn btn-danger">Batal</button>
								</form>
							</div>

						</div>
					
				</div>
			</section>

		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>