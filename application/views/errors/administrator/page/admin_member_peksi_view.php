<!DOCTYPE html>
<html>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<body class="with-side-menu">

	<?php $this->load->view('administrator/nav_menu'); ?>
	<?php $this->load->view('administrator/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Data Member</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="<?php echo base_url('administrator/'); ?> ">Beranda</a></li>
								<li class="active">Data Member</li>
							</ol>
						</div>
					</div>
				</div>
			</header>

			<section class="card">
				<div class="card-block">
					<table id="data_aktivitas" class="display table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>No</th>
							<th>Nama</th>
							<th>Nomor Telepon</th>
							<th>Kota</th>
							<th>Alamat</th>
							<th>Tanggal Daftar</th>
						</tr>
						</thead>
						<?php $i = 1; foreach ($data_member as $dt_pelanggan): ?>
						<tbody>
							<td><?php echo $i++ ?></td>
							<td><?php echo $dt_pelanggan->nama_pelanggan ?></td>
							<td><?php echo $dt_pelanggan->nomor_telepon ?></td>
							<td><?php echo $dt_pelanggan->kota ?></td>
							<td><?php echo $dt_pelanggan->alamat ?></td>
							<td><?php echo date("d-M-Y", strtotime($dt_pelanggan->tanggal_daftar)); ?></td>
						</tbody>
						<?php endforeach ?>

					</table>
				</div>
			</section>

		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>