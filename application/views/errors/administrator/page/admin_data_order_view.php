﻿<!DOCTYPE html>
<html>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<body class="with-side-menu">

	<?php $this->load->view('administrator/nav_menu'); ?>
	<?php $this->load->view('administrator/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">

		<div class="col-lg-12">
				<div class="col-xs-6 col-sm-6 col-md-3">
					<a href="">
						<article class="statistic-box yellow">
							<div>
								<div class="caption"><div>Di Proses</div></div>
								<div class="number"><?php echo $status_diproses ?></div>
							</div>
						</article>
					</a>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<a href="">
						<article class="statistic-box purple">
							<div>
								<div class="caption"><div>Terkirim</div></div>
								<div class="number"><?php echo $status_dikirim ?></div>
							</div>
						</article>
					</a>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<a href="">
						<article class="statistic-box green">
							<div>
								<div class="caption"><div>Diterima</div></div>
								<div class="number"><?php echo $status_diterima ?></div>
							</div>
						</article>
					</a>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<a href="<?php echo base_url('administrator/data_order') ?> ">
						<article class="statistic-box red">
							<div>
								<div class="caption"><div>Gagal</div></div>
								<div class="number"><?php echo $status_gagal ?></div><br>
							</div>
						</article>
					</a>
				</div>
			</div>


			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Data Pesan</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="<?php echo base_url('administrator/'); ?> ">Beranda</a></li>
								<li class="active">Data Pesan</li>
							</ol>
						</div>
					</div>
				</div>
			</header>

		<?php if (isset($_SESSION['sendmessage'])): ?>
		<div class="alert alert-success" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">×</span>
			</button>
			<?php echo $_SESSION['sendmessage'] ?>
		</div>
		<?php endif ?>
		<?php if (isset($_SESSION['validmessage'])): ?>
		<div class="alert alert-warning" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">×</span>
			</button>
			<?php echo $_SESSION['validmessage'] ?>
		</div>
		<?php endif ?>

			<section class="card">
				<div class="card-block">
					<table id="data_aktivitas" class="display table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>No</th>
							<th>Kode Pesan</th>
							<th>Nama</th>
							<th>Nomor Telepon</th>
							<th>Jumlah Pesan</th>
							<th>Kota</th>
							<th>Tanggal Kirim</th>
							<th>Status</th>
						</tr>
						</thead>
						
						<tbody>
						<?php $i = 1; foreach ($data_order as $dt_order): ?>
						<tr>
							<td><?php echo $i++ ?> </td>
							<td><a data-toggle="modal" href='#modal-id<?php echo $dt_order->kode_order ?> '><?php echo $dt_order->kode_order ?></a> </td>
							<td><?php echo $dt_order->nama_pelanggan ?></td>
							<td><?php echo $dt_order->nomor_telepon ?></td>
							<td><?php echo number_format($dt_order->jumlah_order) ?> </td>
							<td><?php echo $dt_order->kota ?> </td>
							<td><?php echo $dt_order->tanggal_kirim; ?></td>
							<td>
								<?php if ($dt_order->status_order == "menunggu"): ?>
									<span class="label label-warning">Menunggu</span>
								<?php elseif ($dt_order->status_order == "diproses"): ?>
									<?php if (($dt_order->status_uang_muka == "lunas") && ($dt_order->status_uang_sisa == "lunas")): ?>
										<span class="label label-success">Pembayaran Lunas</span>
									<?php else: ?>
										<span class="label label-info">Proses Transfer Uang</span>
									<?php endif ?>
									
								<?php elseif ($dt_order->status_order == "terkirim"): ?>
									<span class="label label-info">Pesanan Terkirim</span>
								<?php elseif ($dt_order->status_order == "diterima"): ?>
									<span class="label label-info">Di Terima</span>
								<?php else: ?>
									<span class="label label-danger">Gagal</span>
									<a class="btn btn-sm btn-danger" href="<?php echo base_url('administrator/delete_data_order/'.$dt_order->kode_order.'/'.$dt_order->kode_pelanggan) ?> " role="button"><span class="glyphicon glyphicon-trash"></span> Hapus Data</a>
								<?php endif ?>
							</td>
						</tr>

						<div class="modal fade" id="modal-id<?php echo $dt_order->kode_order ?>">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title">Detail Pesan <?php echo $dt_order->nama_pelanggan ?></h4>
									</div>
									<div class="modal-body">
										<section class="box-typical">
											<div class="profile-card">
												<div class="profile-card-name"><?php echo $dt_order->kode_order ?></div>
												<div class="profile-card-status"><?php echo $dt_order->nama_pelanggan ?></div>
												<div class="profile-card-location"><?php echo $dt_order->nomor_telepon ?></div>
												<div class="profile-card-name"><h2><?php echo $dt_order->jumlah_order ?> DOQ</h2></div>
											</div>
											<div class="profile-statistic tbl">
												<div class="tbl-row">
													<div class="tbl-cell">
														<b><?php echo date("d/m/Y", strtotime($dt_order->tanggal_order)); ?></b>
														Tanggal Order
													</div>
													<div class="tbl-cell">
														<b><?php echo $dt_order->tanggal_kirim; ?></b>
														Tanggal Kirim
													</div>
												</div>
											</div>
										</section><!--.box-typical-->
										<article class="card-user box-typical">
											<div class="row">
												<div class="col-md-6 " style="margin: 0 auto; float: none;">
														<form action="<?php echo base_url('administrator/action_ubah_status_order') ?>" method="POST">
															<br>
															<input type="hidden" name="kode_order" value="<?php echo $dt_order->kode_order ?> ">
															<input type="hidden" name="kode_pelanggan" value="<?php echo $dt_order->kode_pelanggan ?> ">
															<input type="hidden" name="kode_pelanggan" value="<?php echo $dt_order->kode_pelanggan ?> ">
															<input type="hidden" name="jumlah_order_old" value="<?php echo $dt_order->jumlah_order ?> ">
															<!-- <input type="hidden" name="presentase" value="<?php echo $presentase ?>"> -->
															<h4 align="center">KEUANGAN</h4>

															<?php if (!empty($dt_order->satuan_doq)): ?>
																<?php if ($dt_order->status_uang_muka == "lunas"): ?>
																<?php else: ?>
																	<div class="checkbox">
																		<input type="checkbox" id="check-1" class="update_harga">
																		<label for="check-1">Update Jumlah</label>
																	</div>
																<?php endif ?>
															<?php else: ?>
																<div class="checkbox">
																	<input type="checkbox" id="check-1" class="update_harga">
																	<label for="check-1">Update Jumlah</label>
																</div>
															<?php endif ?>

							 								<input type="text" id="jumlah_order" style="text-align: center;" name="jumlah_order" class="form-control key jumlah_order" value="<?php echo $dt_order->jumlah_order; ?>" placeholder="Jumlah Order">
															<br>
															<h5 align="center">Harga Satuan DOQ</h5>
															<?php if (!empty($dt_order->satuan_doq)): ?>
																<?php if ($dt_order->status_uang_muka == "lunas"): ?>
																	<input type="text" name="harga_satuan_doq" id="harga_satuan_doq" style="text-align: center;" class="form-control key satuan" value="<?php echo $dt_order->satuan_doq; ?>" readonly placeholder="Harga Satuan DOQ">
																<?php else: ?>
																	<input type="text" name="harga_satuan_doq" id="harga_satuan_doq" style="text-align: center;" class="form-control key satuan" value="<?php echo $dt_order->satuan_doq; ?>" placeholder="Harga Satuan DOQ">
																<?php endif ?>
															<?php else: ?>
																<input type="text" name="harga_satuan_doq" id="harga_satuan_doq" style="text-align: center;" class="form-control key satuan" value="<?php echo $dt_order->satuan_doq; ?>" placeholder="Harga Satuan DOQ">
															<?php endif ?>
															
															<br>
															<h5 align="center">Total Harga</h5>
															<?php if (!empty($dt_order->total_harga)): ?>
																	<input type="text" name="total_harga" style="text-align: center;" class="form-control" value="<?php echo number_format($dt_order->total_harga,0,".","."); ?>" id="total_harga" placeholder="Total Harga DOQ" readonly>
															<?php else: ?>
																   <input type="text" name="total_harga" id="total_harga" readonly style="text-align: center;" class="form-control" value="" placeholder="Total Harga DOQ" >
															<?php endif ?>

															<br>
															<h5 align="center">Uang Muka <input type="text" id="presentase" style="width: 40px; text-align: center;" name="presentase" class="key presentase" value="<?php echo $dt_order->presentase; ?>" placeholder="0"> %</h5>
															<?php 
																$presentase = $dt_order->presentase;
															 ?>
															<?php if (!empty($dt_order->uang_muka)): ?>
																	<input type="text" name="uang_muka" id="uang_muka" style="text-align: center;" class="form-control key satuan" value="<?php echo number_format($dt_order->uang_muka,0,".","."); ?>" placeholder="Harga Uang Muka" readonly>
															<?php else: ?>
																   <input type="text" name="uang_muka" id="uang_muka" style="text-align: center;" class="form-control key satuan" value="" placeholder="Harga Uang Muka" readonly>
															<?php endif ?>
															
															
															<br>
															<select name="status_order" id="input" class="form-control" required="required" style="display: block; margin: 0 auto;">
																<option <?php echo $dt_order->status_order == 'menunggu' ? 'selected = "selected"': ''; ?> value="menunggu">Menunggu</option>
																<option <?php echo $dt_order->status_order == 'diproses' ? 'selected = "selected"': ''; ?> value="diproses">Di Proses</option>
																<option <?php echo $dt_order->status_order == 'terkirim' ? 'selected = "selected"': ''; ?> value="terkirim">Pengiriman</option>
																<option <?php echo $dt_order->status_order == 'diterima' ? 'selected = "selected"': ''; ?> value="diterima">Di Terima</option>
																<option <?php echo $dt_order->status_order == 'gagal' ? 'selected = "selected"': ''; ?> value="gagal">Gagal Kirim</option>
															</select>
															<br>
															<button type="submit" class="btn btn-primary" style="display: block; margin: 0 auto;">Ubah Status</button>
															<br>
														</form>
												</div>
											</div>
										</article>

										<section class="box-typical">
											<header class="box-typical-header-sm">Alamat Pengiriman</header>
											<article class="profile-info-item">
												<header class="profile-info-item-header">
													<i class="fa fa-map-marker"></i>
													Kota <?php echo $dt_order->kota ?>
												</header>
												<div class="text-block text-block-typical">
													<p><?php echo $dt_order->alamat ?></p>
												</div>
											</article><!--.profile-info-item-->

										</section><!--.box-typical-->

									</div>
									<!-- <div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-primary">Save changes</button>
									</div> -->
								</div>
							</div>
						</div>

						<?php endforeach ?>
						</tbody>
						
					</table>
				</div>
			</section>

		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
<script type="text/javascript">
	$(document).ready(function() {
		$('#jumlah_order').prop("disabled", true);
		$(".update_harga").click(function(){
            if($(this).prop("checked") == true){
                $('#jumlah_order').prop("disabled", false);
            }
            else if($(this).prop("checked") == false){
                $('#jumlah_order').prop("disabled", true);
            }
        });
	});
	
$(document).ready(function() {
	function formatAngka(angka) {
		if (typeof(angka) != 'string') angka = angka.toString();
		var reg = new RegExp('([0-9]+)([0-9]{3})');
		while(reg.test(angka)) angka = angka.replace(reg, '$1.$2');
		return angka;
	}

    function calc() {
    	var presentase = ($.trim($(".presentase").val()) != "" && !isNaN($(".presentase").val())) ? parseInt($(".presentase").val()) : 0;
        var $num1 = ($.trim($(".jumlah_order").val()) != "" && !isNaN($(".jumlah_order").val())) ? parseInt($(".jumlah_order").val()) : 0;
        var $num2 = ($.trim($(".satuan").val()) != "" && !isNaN($(".satuan").val())) ? parseInt($(".satuan").val()) : 0;
        var total_harga = $num1 * $num2;
        $("#total_harga").val(formatAngka(total_harga));
        var uang_muka = (presentase/100 * total_harga);
        $('#uang_muka').val(formatAngka(uang_muka));
    }

    $(".key").keyup(function() {
        calc();
    });
});
</script>
</body>
</html>