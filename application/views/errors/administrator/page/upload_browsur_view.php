<!DOCTYPE html>
<html>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<body class="with-side-menu">

	<?php $this->load->view('administrator/nav_menu'); ?>
	<?php $this->load->view('administrator/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Upload brosur</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="<?php echo base_url('administrator/'); ?> ">Beranda</a></li>
								<li class="active">Brosur</li>
							</ol>
						</div>
					</div>
				</div>
			</header>

			<?php if (isset($_SESSION['message_data'])): ?>
				<div class="alert alert-aquamarine alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['message_data'] ?>
				</div>
				<?php endif ?>

			<?php if (isset($_SESSION['error_data'])): ?>
			<div class="alert alert-danger alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
				</button>
				<?php echo $_SESSION['error_data'] ?>
			</div>
			<?php endif ?>

			<div class="box-typical box-typical-padding">
				<form action="<?php echo base_url().'administrator/action_upload_browsur'; ?> " method="POST" enctype="multipart/form-data">
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">Judul Brosur</label>
						<div class="col-sm-9">
							<p class="form-control-static"><input type="text" name="judul_browsur" class="form-control" id="judul_browsur" required placeholder="Judul Browsur"></p>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-3 form-control-label">Status Brosur</label>
						<div class="col-sm-9">
							<div class="checkbox">
								<input type="checkbox" name="status" id="check-1" value="Terbaru">
								<label for="check-1">Terbaru</label>
							</div>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-3 form-control-label">File Brosur <br> <sup style="color : red; font-weight: bold;">* ukuran ( < 1MB) Type file jpg,jpeg,png </sup></label>
						<div class="col-sm-9">
							<p class="form-control-static"><input type="file" name="cover_browsur" class="form-control" id="cover_browsur"></p>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-3 form-control-label"></label>
						<div class="col-sm-9">
							<p class="form-control-static">
								<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Upload brosur</button>
								<button type="submit" class="btn btn-danger"> Batal</button>
							</p>
						</div>
					</div>
				</form>

			</div><!--.box-typical-->
			
			<section class="card">
				<div class="card-block">
					<table id="data_aktivitas" class="display table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>No</th>
							<th>Judul Browsur</th>
							<th>Tanggal Post</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						</thead>
						
						<tbody>
						<?php $i = 1; foreach ($data_browsur as $dt_browsur): ?>
						<tr>
							<td><?php echo $i++; ?></td>
							<td><?php echo $dt_browsur->nama_browsur; ?></td>
							<td><?php echo date("d-M-Y H:i:s", strtotime($dt_browsur->tanggal_upload)); ?></td>
							<td>
								<?php if ($dt_browsur->status == "1"): ?>
									<span class="label label-succes">Active</span>
								<?php else: ?>
									<span class="label label-warning">Pending</span>
								<?php endif ?></td>
							<td>
								<div class="btn-group">
									<a class="btn btn-danger btn-sm" href="<?php echo base_url().'index.php/administrator/action_delete_browsur/'.$dt_browsur->id_browsur; ?>" role="button"><span class="glyphicon glyphicon-trash"></span> Delete</a>
								</div>
							</td>
						</tr>
						<?php endforeach ?>

						</tbody>
					</table>
				</div>
			</section>

		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>