<div class="mobile-menu-left-overlay"></div>
	<nav class="side-menu">
	    <ul class="side-menu-list">
	        <li class="grey">
	            <a href="<?php echo base_url('administrator/'); ?>"><span class="lbl">Beranda</span></a>
	        </li>
	        <li class="grey">
	            <a href=" <?php echo base_url('administrator/data_gallery'); ?>"><span class="lbl">Data Galeri</span></a>
	        </li>
	        <!-- <li class="grey with-sub">
	            <span>
	                <span class="lbl">Aktivitas</span>
	            </span>
	            <ul>
	                <li><a href="<?php echo base_url('administrator/tambah_aktivitas'); ?>"><span class="lbl">Tambah Aktivitas</span></a></li>
	                <li><a href="<?php echo base_url('administrator/data_aktivitas'); ?>"><span class="lbl">Data Aktivitas</span></a></li>
	            </ul>
	        </li> -->
	        <li class="grey">
	            <a href=" <?php echo base_url('administrator/data_order'); ?>"><span class="lbl">Pesan DOQ</span></a>
	        </li>
	        <li class="grey">
	            <a href=" <?php echo base_url('administrator/konfirmasi_pembayaran'); ?>" class="label-right">
		            <span class="lbl">Konfirmasi Pembayaran</span>
		            <?php 
		            	$this->load->model('Bukti_transfer_model');
		            	$m_bukti_transfer = new Bukti_transfer_model();
		            	$data = $m_bukti_transfer->view_data_bukti_transfer_status_menunggu()->result();
		             ?>
		             <?php if (count($data) != 0): ?>
		             	<span class="label label-custom label-pill label-danger"><?php echo count($data); ?></span>
		             <?php else: ?>
		             <?php endif ?>
		            </a>
	        </li>
	        <li class="grey">
	            <a href=" <?php echo base_url('administrator/member_peksi'); ?>"><span class="lbl">Pelanggan Peksi</span></a>
	        </li>
	        <li class="grey">
	            <a href=" <?php echo base_url('administrator/browsur'); ?>"><span class="lbl">Brosur</span></a>
	        </li>
	        <li class="grey">
	            <a href=" <?php echo base_url('administrator/info_data_doq'); ?>"><span class="lbl">Info DOQ</span></a>
	        </li>
	        <li class="grey">
	            <a href=" <?php echo base_url('administrator/harga_jual'); ?>"><span class="lbl">Harga Jual</span></a>
	        </li>
	        <li class="grey">
	            <a href=" <?php echo base_url('administrator/setting_transfer'); ?>"><span class="lbl">Pengatuan Rekening</span></a>
	        </li>
	        <li class="grey">
	            <a href=" <?php echo base_url('administrator/pesan_pelanggan'); ?>"><span class="lbl">Testimoni</span></a>
	        </li>

	        
	    </ul>
	</nav><!--.side-menu-->