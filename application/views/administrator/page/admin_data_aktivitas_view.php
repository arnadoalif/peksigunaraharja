
<!DOCTYPE html>
<html>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<style type="text/css" media="screen">


</style>
<body class="with-side-menu">

	<?php $this->load->view('administrator/nav_menu'); ?>
	<?php $this->load->view('administrator/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Data Aktivitas</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="<?php echo base_url('administrator/'); ?>">Beranda</a></li>
								<li class="active">Data Aktivitas</li>
							</ol>
						</div>
					</div>
				</div>

				<?php if (isset($_SESSION['message_data'])): ?>
				<div class="alert alert-aquamarine alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['message_data'] ?>
				</div>
				<?php endif ?>

				<?php if (isset($_SESSION['error_data'])): ?>
				<div class="alert alert-danger alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['error_data'] ?>
				</div>
				<?php endif ?>

			</header>
			<section class="card">
				<div class="card-block">
					<table id="data_aktivitas" class="display table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>No</th>
							<th>Judul Berita</th>
							<th>Post By</th>
							<th>Tanggal Post</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						</thead>
						
						<tbody>
						<?php $i = 1; foreach ($data_aktivitas as $dt_aktivitas): ?>
						<tr>
							<td><?php echo $i++; ?></td>
							<td><a data-toggle="modal" href='#modal-id-detail<?php echo $dt_aktivitas->apikey_berita; ?>'></a> </td>
							<td>Admin</td>
							<td><?php echo date("d-M-Y H:i:s", strtotime($dt_aktivitas->tanggal_post)); ?></td>
							<td>
								<?php if ($dt_aktivitas->status_post == "1"): ?>
									<span class="label label-success">Active</span>
								<?php else: ?>
									<span class="label label-warning">Pending</span>
								<?php endif ?>
							</td>
							<td>
								<div class="btn-group">
									<a class="btn btn-primary btn-sm" data-toggle="modal" href='#modal-id' role="button"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
									<a class="btn btn-danger btn-sm" href="<?php echo base_url().'index.php/administrator/action_delete_aktivitas/'.$dt_aktivitas->apikey_berita; ?> " role="button"><span class="glyphicon glyphicon-trash"></span> Delete</a>
								</div>
							</td>
						</tr>

						<div class="modal fade" id="modal-id">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title">Modal title</h4>
									</div>
									<div class="modal-body">
										
										<form action="<?php echo base_url().'index.php/administrator/action_edit_aktivitas'; ?> " method="POST" enctype="multipart/form-data">
											<div class="form-group row">
												<input type="hidden" name="key" value= <?php echo $dt_aktivitas->apikey_berita; ?> placeholder="">
												<label class="col-sm-2 form-control-label">Judul Berita</label>
												<div class="col-sm-10">
													<p class="form-control-static"><input type="text" name="judul_berita" class="form-control" id="judul_berita" placeholder="Judul Berita" value="<?php echo $dt_aktivitas->judul_berita; ?> "></p>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-2 form-control-label">Kategori Berita</label>
												<div class="col-sm-10">
													<select name="kategori_berita" id="inputKategori_berita" class="form-control">
														<option value="" selected>Pilih Kategori</option>
														<option value="Burung Puyuh" <?php if($dt_aktivitas->kategori_berita =="Burung Puyuh") echo 'selected="selected"'; ?> >Burung Puyuh</option>
														<option value="Berita" <?php if($dt_aktivitas->kategori_berita =="Berita") echo 'selected="selected"'; ?> >Berita</option>
														<option value="Tips & Trik" <?php if($dt_aktivitas->kategori_berita =="Tips & Trik") echo 'selected="selected"'; ?> >Tips & Trik</option>
														<option value="Resep" <?php if($dt_aktivitas->kategori_berita =="Resep") echo 'selected="selected"'; ?> >Resep</option>
													</select>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-sm-2 form-control-label">Isi Berita</label>
												<div class="col-sm-10">
													<div class="summernote-theme-1">
														<textarea class="summernote" rows="10" cols="10" name="isi_berita">
															<?php echo $dt_aktivitas->isi_berita; ?>
														</textarea>
													</div>
												</div>
											</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										<button type="submit" class="btn btn-primary">Save changes</button>
									</div>
									</form>
								</div>
							</div>
						</div>


						<div class="modal fade" id="modal-id-detail">
							<div class="modal-dialog custom-class">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title"><?php echo $dt_aktivitas->judul_berita; ?></h4>
									</div>
									<div class="modal-body">
										
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-primary">Save changes</button>
									</div>
								</div>
							</div>
						</div>



						<?php endforeach ?>

						</tbody>
					</table>
				</div>
			</section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>