<!DOCTYPE html>
<html>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<body class="with-side-menu">

	<?php $this->load->view('administrator/nav_menu'); ?>
	<?php $this->load->view('administrator/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Pengaturan Rekening</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="<?php echo base_url('administrator/'); ?> ">Beranda</a></li>
								<li class="active">Pengaturan Rekening</li>
							</ol>
						</div>
					</div>
				</div>
			</header>

			<?php if (isset($_SESSION['message_data'])): ?>
				<div class="alert alert-aquamarine alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['message_data'] ?>
				</div>
				<?php endif ?>

			<?php if (isset($_SESSION['error_data'])): ?>
			<div class="alert alert-danger alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
				</button>
				<?php echo $_SESSION['error_data'] ?>
			</div>
			<?php endif ?>

			<?php if ($total_rekening == "3"): ?>
			<?php else: ?>
				<div class="box-typical box-typical-padding">
					<form action="<?php echo base_url().'administrator/action_insert_rekening'; ?> " method="POST" >
							<div class="form-group row">
								<label class="col-sm-3 form-control-label">Jenis Rekening</label>
								<div class="col-sm-9">
									<select name="jenis_rekening" id="jenis_rekening" class="form-control" required="required">
										<option value="bca">(BCA) BACK CENTRAL ASIA</option>
											<option value="mandiri">MANDIRI</option>
											<option value="bni">BNI</option>
											<option value="" selected>Pilih Jenis Rekening</option>
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 form-control-label">Nomor Rekening</label>
								<div class="col-sm-9">
									<p class="form-control-static"><input type="input" name="nomor_rekening" required class="form-control" id="nomor_rekening"></p>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 form-control-label">Atas Nama Rekening</label>
								<div class="col-sm-9">
									<p class="form-control-static"><input type="input" name="atas_nama" required class="form-control" id="atas_nama"></p>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 form-control-label"></label>
								<div class="col-sm-9">
									<p class="form-control-static">
										<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Tambah Rekening</button>
										<button type="submit" class="btn btn-danger"> Batal</button>
									</p>
								</div>
							</div>
					</form>
				</div><!--.box-typical-->
			<?php endif ?>
			
			<section class="card">
				<div class="card-block">
					<table id="data_aktivitas" class="display table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>No</th>
							<th>Jenis Bank</th>
							<th>Nomor Rekening</th>
							<th>A/N</th>
							<th>Aksi</th>
						</tr>
						</thead>
						
						<tbody>
						<?php $i = 1; foreach ($data_rekening as $dt_rekening): ?>
						<tr>
							<td><?php echo $i++; ?></td>
							<td>
								<?php if ($dt_rekening->jenis_rekening == "bca"): ?>
									BCA (BANK CENTRAL ASIA)
								<?php elseif ($dt_rekening->jenis_rekening == "mandiri"): ?>
									MANDIRI
								<?php else: ?>
									BNI (BANK NEGARA INDONESIA)
								<?php endif ?>
							</td>
							<td><?php echo $dt_rekening->nomor_rekening ?></td>
							<td><?php echo $dt_rekening->atas_nama ?></td>
							<td>
								<a class="btn btn-sm btn-primary" data-toggle="modal" href='#edit_rekening<?php echo $dt_rekening->kode_rekening ?>'><i class="fa fa-pencil"></i> Ubah</a>
							</td>
						</tr>
						

						<div class="modal fade" id="edit_rekening<?php echo $dt_rekening->kode_rekening ?>">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title">Modal title</h4>
									</div>
									<div class="modal-body">
										<form action="<?php echo base_url().'administrator/action_update_rekening'; ?> " method="POST" >
												<div class="form-group row">
													<input type="hidden" name="kode_rekening" value="<?php echo $dt_rekening->kode_rekening ?>">
													<label class="col-sm-3 form-control-label">Jenis Rekening</label>
													<div class="col-sm-9">
														<p class="form-control-static">
															<?php if ($dt_rekening->jenis_rekening == "bca"): ?>
																BCA (BANK CENTRAL ASIA)
															<?php elseif ($dt_rekening->jenis_rekening == "mandiri"): ?>
																MANDIRI
															<?php else: ?>
																BNI (BANK NEGARA INDONESIA)
															<?php endif ?>
														</p>
													</div>
												</div>

												<div class="form-group row">
													<label class="col-sm-3 form-control-label">Nomor Rekening</label>
													<div class="col-sm-9">
														<p class="form-control-static"><input type="input" name="nomor_rekening" required class="form-control" id="nomor_rekening" value="<?php echo $dt_rekening->nomor_rekening ?>"></p>
													</div>
												</div>

												<div class="form-group row">
													<label class="col-sm-3 form-control-label">Atas Nama Rekening</label>
													<div class="col-sm-9">
														<p class="form-control-static"><input type="input" name="atas_nama" value="<?php echo $dt_rekening->atas_nama ?>" required class="form-control" id="atas_nama"></p>
													</div>
												</div>

												<div class="form-group row">
													<label class="col-sm-3 form-control-label"></label>
													<div class="col-sm-9">
														<p class="form-control-static">
															<button type="submit" class="btn btn-success">Perbaharui Rekening</button>
														</p>
													</div>
												</div>
										</form>
									</div>
								</div>
							</div>
						</div>

						<?php endforeach ?>

						</tbody>
					</table>
				</div>
			</section>

		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>