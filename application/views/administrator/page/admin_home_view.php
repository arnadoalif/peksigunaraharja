﻿<!DOCTYPE html>
<html>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<style type="text/css">
			 .modal-lg { width: 100%; }
		</style>
<body class="with-side-menu">

	<?php $this->load->view('administrator/nav_menu'); ?>
	<?php $this->load->view('administrator/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">
			
			 <div class="col-lg-12">
	            	<div class="col-xs-6 col-sm-6 col-md-3">
	            		<a href="">
	            			<article class="statistic-box red">
	                        	<div>
	                        		<div class="caption"><div>Artikel</div></div>
	                                <div class="number"><?php echo $total_berita; ?></div>
	                            </div>
	                        </article>
	                    </a>
	            	</div>
	            	<div class="col-xs-6 col-sm-6 col-md-3">
	            		<a href="<?php echo base_url('administrator/member_peksi') ?>">
	            			<article class="statistic-box green">
	                        	<div>
	                                <div class="caption"><div>Member</div></div>
	                                <div class="number"><?php echo $total_member; ?></div>
	                            </div>
	                        </article>
	                    </a>
	            	</div>
	            	<div class="col-xs-6 col-sm-6 col-md-3">
	            		<a href="">
	            			<article class="statistic-box purple">
	                        	<div>
	                        		<div class="caption"><div>Testimoni</div></div>
	                                <div class="number"><?php echo $total_pesan ?></div>
	                            </div>
	                        </article>
	                    </a>
	            	</div>
	            	<div class="col-xs-6 col-sm-6 col-md-3">
	            		<a href="<?php echo base_url('administrator/data_order') ?> ">
	            			<article class="statistic-box yellow">
	                        	<div>
	                        		<div class="caption"><div>Order DOQ Menunggu</div></div>
	                                <div class="number"><?php echo $total_order ?></div><br>
	                            </div>
	                        </article>
	                    </a>
	            	</div>
	         </div>

			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h5>Data Post Artikel</h5>
						</div>
					</div>
				</div>

				<?php if (isset($_SESSION['message_data'])): ?>
				<div class="alert alert-aquamarine alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['message_data'] ?>
				</div>
				<?php endif ?>

				<?php if (isset($_SESSION['error_data'])): ?>
				<div class="alert alert-danger alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['error_data'] ?>
				</div>
				<?php endif ?>
			</header>

			<section class="card">
				<div class="card-block">
				    <a class="btn btn-info btn-sm" href="<?php echo base_url('administrator/tambah_aktivitas'); ?>" role="button"><span class="glyphicon glyphicon-plus"></span> Tambah Artikel </a><br><br>
					<table id="data_aktivitas" class="display table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>No</th>
							<th>Judul Berita</th>
							<th>Post By</th>
							<th>Tanggal Post</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						</thead>
						
						<tbody>
						<?php $i = 1; foreach ($data_aktivitas as $dt_aktivitas): ?>
						<tr>
							<td><?php echo $i++; ?></td>
							<td><?php echo $dt_aktivitas->judul_berita; ?><p><span><a href="<?php echo base_url('artikel/'.$dt_aktivitas->kata_kunci); ?>"><?php  echo base_url('artikel/'.$dt_aktivitas->kata_kunci) ?></a></span></p></td>
							<!-- <td><a data-toggle="modal" href='#modal-id-detail<?php echo $dt_aktivitas->apikey_berita; ?>'><?php echo $dt_aktivitas->judul_berita; ?></a> </td> -->
							<td>Admin</td>
							<td><?php echo date("d-M-Y H:i:s", strtotime($dt_aktivitas->tanggal_post)); ?></td>
							<td>
								<span class="label label-success">Active</span>
							</td>
							<td>
								<div class="btn-group">
									<a class="btn btn-primary btn-sm" href="<?php echo base_url('administrator/edit_berita/'.$dt_aktivitas->apikey_berita) ?>" role="button"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
									<a class="btn btn-danger btn-sm" href="<?php echo base_url().'index.php/administrator/action_delete_aktivitas/'.$dt_aktivitas->apikey_berita; ?> " role="button"><span class="glyphicon glyphicon-trash"></span> Delete</a>
								</div>
							</td>
						</tr>

						<?php endforeach ?>

						</tbody>
					</table>
				</div>
			</section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
<script type="text/javascript">
		setInterval("auto_refresh_function();", 500);
        function auto_refresh_function() {
          $('.number').load('http://demo1.com/demo/count_controller/table');
        }
</script>
</body>
</html>