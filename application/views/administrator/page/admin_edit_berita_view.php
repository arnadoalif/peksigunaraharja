<!DOCTYPE html>
<html>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<body class="with-side-menu">

	<?php $this->load->view('administrator/nav_menu'); ?>
	<?php $this->load->view('administrator/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">

			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Edit Data Aktivitas</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="<?php echo base_url('administrator/'); ?> ">Beranda</a></li>
								<li><a href="<?php echo base_url('administrator/'); ?> ">Data Aktivitas</a></li>
								<li class="active">Edit Data</li>
							</ol>
						</div>
					</div>
				</div>

				<?php if (isset($_SESSION['message_data'])): ?>
				<div class="alert alert-aquamarine alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['message_data'] ?>
				</div>
				<?php endif ?>

				<?php if (isset($_SESSION['error_data'])): ?>
				<div class="alert alert-danger alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['error_data'] ?>
				</div>
				<?php endif ?>

			</header>

		<?php foreach ($data_aktivitas as $dt_aktivitas): ?>
			
			<div class="box-typical box-typical-padding">
				<form action="<?php echo base_url().'index.php/administrator/action_edit_aktivitas'; ?> " method="POST" enctype="multipart/form-data">
					<div class="form-group row">
					<input type="hidden" name="key" value= <?php echo $dt_aktivitas->apikey_berita; ?> placeholder="">
					<label class="col-sm-2 form-control-label">Judul Berita</label>
					<div class="col-sm-10">
						<p class="form-control-static"><input type="text" name="judul_berita" class="form-control" id="judul_berita" placeholder="Judul Berita" value="<?php echo $dt_aktivitas->judul_berita; ?> "></p>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 form-control-label">Kategori Berita</label>
					<div class="col-sm-10">
						<select name="kategori_berita" id="inputKategori_berita" class="form-control">
							<option value="" selected>Pilih Kategori</option>
							<option value="Burung Puyuh" <?php if($dt_aktivitas->kategori_berita =="Burung Puyuh") echo 'selected="selected"'; ?>>Burung Puyuh</option>
							<option value="Kesehatan" <?php if($dt_aktivitas->kategori_berita =="Kesehatan") echo 'selected="selected"'; ?>>Kesehatan Burung Puyuh</option>
							<option value="Kandang" <?php if($dt_aktivitas->kategori_berita =="Kandang") echo 'selected="selected"'; ?>>Kandang Burung Puyuh</option>
							<option value="Kotoran" <?php if($dt_aktivitas->kategori_berita =="Kotoran") echo 'selected="selected"'; ?>>Kotoran Burung Puyuh</option>
							<option value="Resep Telur" <?php if($dt_aktivitas->kategori_berita =="Resep Telur") echo 'selected="selected"'; ?>>Resep Telur Burung Puyuh</option>
							<option value="Prospek" <?php if($dt_aktivitas->kategori_berita =="Prospek") echo 'selected="selected"'; ?>>Prospek Usaha Burung Puyuh</option>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 form-control-label">Isi Berita</label>
					<div class="col-sm-10">
						<div class="summernote-theme-1">
							<textarea class="summernote" rows="10" cols="10" name="isi_berita">
							<?php echo $dt_aktivitas->isi_berita; ?>
							</textarea>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-2 form-control-label">Kata Kunci Berita</label>
					<div class="col-sm-10">
						<div class="col-lg-12">
							<textarea id="tags-editor-textarea" name="tag"><?php echo $dt_aktivitas->tag_post; ?></textarea>
						</div>
					</div>
				</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label"></label>
						<div class="col-sm-10">
							<p class="form-control-static">
								<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-saved"></span> Update Berita</button>
								<a class="btn btn-info" href="<?php echo base_url('administrator/'); ?>" role="button">Cancel Berita</a>
							</p>
						</div>
					</div>
					
				</form>

			</div><!--.box-typical-->
		<?php endforeach ?>

		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>