﻿<!DOCTYPE html>
<html>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<body class="with-side-menu">

	<?php $this->load->view('administrator/nav_menu'); ?>
	<?php $this->load->view('administrator/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">
			
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Data Gallery</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="<?php echo base_url('administrator/'); ?>">Beranda</a></li>
								<li class="active">Data Gallery</li>
							</ol>
						</div>
					</div>
				</div>

				<?php if (isset($_SESSION['message_data'])): ?>
				<div class="alert alert-aquamarine alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['message_data'] ?>
				</div>
				<?php endif ?>

				<?php if (isset($_SESSION['error_data'])): ?>
				<div class="alert alert-danger alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['error_data'] ?>
				</div>
				<?php endif ?>

			</header>
			<section class="card">
				<div class="card-block">
					<a class="btn btn-info btn-sm" href="<?php echo base_url('administrator/tambah_gallery'); ?>" role="button"><span class="glyphicon glyphicon-plus"></span> Tambah Gallery </a><br><br>
					<table id="data_aktivitas" class="display table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>No</th>
							<th>Judul Event</th>
							<th>Tanggal Post</th>
							<th>Action</th>
						</tr>
						</thead>
						
						<tbody>
						<?php $i = 1; foreach ($data_gallery as $dt_gallery): ?>
						<tr>
							<td><?php echo $i++; ?></td>
							<td><?php echo $dt_gallery->nama_event ?><br>
								<a target="_blank" href="<?php echo base_url('galeri/'.$dt_gallery->kata_kunci) ?>" ><?php echo base_url('galeri/'.$dt_gallery->kata_kunci) ?></a>
							</td>
							<td><?php echo $dt_gallery->create_at ?></td>
							<td>
								<div class="btn-group">
									<a class="btn btn-primary btn-sm" href="<?php echo base_url().'administrator/edit_data_gallery/'.$dt_gallery->kode_gallery ?>"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
									<a class="btn btn-danger btn-sm" href="<?php echo base_url().'administrator/action_delete_gallery/'.$dt_gallery->kode_gallery; ?> " role="button"><span class="glyphicon glyphicon-trash"></span> Delete</a>
								</div>
							</td>
						</tr>

						<?php endforeach ?>

						</tbody>
					</table>
				</div>
			</section>

		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>