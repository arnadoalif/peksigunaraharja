<!DOCTYPE html>
<html>
<title>Admin PT.Peksi Gunaraharja</title>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<body class="with-side-menu">

	<?php $this->load->view('administrator/nav_menu'); ?>
	<?php $this->load->view('administrator/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-6 col-md-12">
					<h2 align="center">Total DOQ Tiap Minggunya</h2>
					<section class="widget widget-accordion" id="" role="tablist" >
						<?php foreach ($total_doq as $key => $dt_total_doq): ?>

							<article class="panel">
								<div class="panel-heading" role="tab" id="headingOne<?php echo $dt_total_doq->periode ?> ">
									<a data-toggle="collapse"
									   data-parent="#accordion"
									   href="#collapseOne<?php echo $dt_total_doq->periode ?>"
									   aria-expanded="true"
									   aria-controls="collapseOne<?php echo $dt_total_doq->periode ?>">
										<span class="glyphicon glyphicon-calendar"></span> <?php echo "Minggu ke- ".$dt_total_doq->minggu ?> Total DOQ <?php echo number_format($dt_total_doq->jumlah); ?>
										<i class="font-icon font-icon-arrow-down"></i>
									</a>
								</div>
								<div id="collapseOne<?php echo $dt_total_doq->periode ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne<?php echo $dt_total_doq->periode ?>">
									<div class="panel-collapse-in">
										<div class="user-card-row">
											<div class="tbl-row">
												<div class="tbl-cell">
													<div class="table-responsive">
														<table class="table table-hover">
															<thead>
																<tr>
																	<th style="text-align: center;">Tanggal</th>
																	<th style="text-align: center;">Total DOQ</th>
																	<th style="text-align: center;">Sudah dipesan</th>
																	<th style="text-align: center;">Sisa DOQ</th>
																</tr>
															</thead>

															<?php 
															$this->load->model('Migratesystem_model');
															$m_migrate_system = new Migratesystem_model();
															$data_detail =$m_migrate_system->detail_doq_order_member($dt_total_doq->periode)->result();
															 ?>

															<tbody>
															
																<?php foreach ($data_detail as $dt_detail): ?>
																	<tr>
																		<?php 
																			$sql_order = "select jumlah_order from tbl_order where tanggal_kirim = '$dt_detail->tgl'";
																			$jumlah = 0;
																			$total_order = $this->db->query($sql_order)->result();
																			foreach ($total_order as $value) {
																				$jumlah =  $jumlah + $value->jumlah_order;
																			}
																			$sisa = $dt_detail->total - $jumlah;
																		?>
																		<td style="text-align: center;"><?php echo $dt_detail->tgl ?></td>
																		<td style="text-align: center;">
																			<?php if ($sisa == '0'): ?>
																				<strike><?php echo number_format($dt_detail->total)  ?> Ekor </strike>
																			<?php else: ?>
																				<?php echo number_format($dt_detail->total)  ?> Ekor 
																			<?php endif ?>
																		</td>
																		<td style="text-align: center;">
																			<?php echo $jumlah; ?>
																		</td>
																		<td style="text-align: center;">
																			<?php 
																					echo ($sisa);
																			 ?>
																		</td>
																	</tr>
																<?php endforeach ?>
															
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
										
										
									</div>
								</div>
							</article>

						<?php endforeach ?>
						
					</section><!--.widget-accordion-->
				</div>
			</div><!--.row-->
			
			

		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>