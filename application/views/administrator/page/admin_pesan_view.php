<!DOCTYPE html>
<html>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<body class="with-side-menu">

	<?php $this->load->view('administrator/nav_menu'); ?>
	<?php $this->load->view('administrator/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Pesan Pelanggan</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="<?php echo base_url('administrator/'); ?> ">Beranda</a></li>
								<li class="active">Pesan Pelanggan</li>
							</ol>
						</div>
					</div>
				</div>
			</header>

			<section class="card">
				<div class="card-block">
					<table id="data_aktivitas" class="display table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>No</th>
							<th>Nama</th>
							<th>Pesan</th>
							<th>Tanggal Pesan</th>
						</tr>
						</thead>
						<?php $i = 1; foreach ($data_pesan as $dt_pesan): ?>
						<tbody>
							<td><?php echo $i++ ?></td>
							<td><a data-toggle="modal" href='#modal-id<?php echo $dt_pesan->id_pesan ?>'><?php echo $dt_pesan->nama_penulis ?></a></td>
							<td><?php echo $dt_pesan->isi_pesan ?></td>
							<td><?php echo $dt_pesan->tanggal_pesan ?></td>
						</tbody>
						

						<div class="modal fade" id="modal-id<?php echo $dt_pesan->id_pesan ?>">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title">Isi Pesan Dari <?php echo $dt_pesan->nama_penulis ?></h4>
									</div>
									<div class="modal-body">
										<p> Alamat : <?php echo $dt_pesan->nama_penulis ?></p>
										<p> Alamat : <?php echo $dt_pesan->alamat_lengkap ?></p>
										<p> Nomor Telepon : <?php echo $dt_pesan->nomor_telepon ?></p>
										<p> Email  : <?php echo $dt_pesan->email_penulis ?></p>
										<p>Pesan :</p>
										<p><strong><?php echo $dt_pesan->isi_pesan ?><strong></p>
									</div>
									<div class="modal-footer">
										<p class="pull-left"><?php echo date("d-M-Y H:i:s", strtotime($dt_pesan->tanggal_pesan)); ?></p>
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>

						<?php endforeach ?>

					</table>
				</div>
			</section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>