<!DOCTYPE html>
<html>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<body class="with-side-menu">

	<?php $this->load->view('administrator/nav_menu'); ?>
	<?php $this->load->view('administrator/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">
			
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Tambah Data Gallery</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="<?php echo base_url('administrator/'); ?> ">Beranda</a></li>
								<li>Gallery</li>
								<li class="active">Tambah Data</li>
							</ol>
						</div>
					</div>
				</div>

				<?php if (isset($_SESSION['message_data'])): ?>
				<div class="alert alert-aquamarine alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['message_data'] ?>
				</div>
				<?php endif ?>

				<?php if (isset($_SESSION['error_data'])): ?>
				<div class="alert alert-danger alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['error_data'] ?>
				</div>
				<?php endif ?>

			</header>

			<div class="box-typical box-typical-padding">
				<form action="<?php echo base_url().'administrator/action_tambah_gallery'; ?> " method="POST" enctype="multipart/form-data">
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Judul Event</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input type="text" name="nama_event" class="form-control" id="nama_event" placeholder="Judul Event"></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Cover Gallery</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input type="file" name="cover_gallery" class="form-control" id="cover_gallery"></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Gallery <sup style="color: red">masukan foto album</sup></label>
						<div class="col-sm-10">
							<p class="form-control-static"><input type="file" name="file_album[]" multiple="multiple" class="form-control" id="album"></p>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Deskripsi Gallery</label>
						<div class="col-sm-10">
							<div class="summernote-theme-1">
								<textarea class="summernote" rows="10" cols="10" name="deskripsi_gallery"></textarea>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label"></label>
						<div class="col-sm-10">
							<p class="form-control-static">
								<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-saved"></span> Save Gallery</button>
								<button type="submit" class="btn btn-danger"> Cancel Gallery</button>
							</p>
						</div>
					</div>
					
				</form>

			</div><!--.box-typical-->

		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>