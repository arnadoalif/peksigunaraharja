<!DOCTYPE html>
<html>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<body class="with-side-menu">

	<?php $this->load->view('administrator/nav_menu'); ?>
	<?php $this->load->view('administrator/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Setting</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="#">StartUI</a></li>
								<li><a href="#">Forms</a></li>
								<li class="active">Basic Inputs</li>
							</ol>
						</div>
					</div>
				</div>
			</header>

			<section class="card">
				<header class="card-header card-header-lg">
					Visi & Misi
				</header>
				<div class="card-block">
					<p class="card-text">
						
					</p>
				</div>
			</section>
			<section class="card">
				<header class="card-header card-header-lg">
					Profile 
				</header>
				<div class="card-block">
					<p class="card-text">Panel content</p>
				</div>
			</section>
			<section class="card">
				<header class="card-header card-header-lg">
					Penghargaan
				</header>
				<div class="card-block">
					<p class="card-text">Panel content</p>
				</div>
			</section>
			<section class="card">
				<header class="card-header card-header-lg">
					Update Logo Company
				</header>
				<div class="card-block">
					<p class="card-text">Panel content</p>
				</div>
			</section>

		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>