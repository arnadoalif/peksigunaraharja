﻿<!DOCTYPE html>
<html>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<body class="with-side-menu">

	<?php $this->load->view('administrator/nav_menu'); ?>
	<?php $this->load->view('administrator/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Tambah Data Aktivitas</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="<?php echo base_url('administrator/'); ?> ">Beranda</a></li>
								<li>Aktivitas</li>
								<li class="active">Tambah Data</li>
							</ol>
						</div>
					</div>
				</div>

				<?php if (isset($_SESSION['message_data'])): ?>
				<div class="alert alert-aquamarine alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['message_data'] ?>
				</div>
				<?php endif ?>

				<?php if (isset($_SESSION['error_data'])): ?>
				<div class="alert alert-danger alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['error_data'] ?>
				</div>
				<?php endif ?>

			</header>

			<div class="box-typical box-typical-padding">
				<form action="<?php echo base_url().'administrator/action_tambah_aktivitas'; ?> " method="POST" enctype="multipart/form-data">
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Judul Berita</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input type="text" name="judul_berita" class="form-control" id="judul_berita" placeholder="Judul Berita"></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Kategori Berita</label>
						<div class="col-sm-10">
							<select name="kategori_berita" id="inputKategori_berita" class="form-control">
								<option value="" selected>Pilih Kategori</option>
								<option value="Burung Puyuh">Burung Puyuh</option>
								<option value="Kesehatan">Kesehatan Burung Puyuh</option>
								<option value="Kandang">Kandang Burung Puyuh</option>
								<option value="Kotoran">Kotoran Burung Puyuh</option>
								<option value="Resep Telur">Resep Telur Burung Puyuh</option>
								<option value="Prospek">Prospek Usaha Burung Puyuh</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Cover Berita</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input type="file" name="cover_berita" class="form-control" id="judul_berita"></p>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Isi Berita</label>
						<div class="col-sm-10">
							<div class="summernote-theme-1">
								<textarea class="summernote" rows="10" cols="10" name="isi_berita"></textarea>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Kata Kunci Berita</label>
						<div class="col-sm-10">
							<div class="col-lg-12">
								<textarea id="tags-editor-textarea" name="tag">bibit puyuh, bibit puyuh peksi, jual doq, puyuh peksi, ternak puyuh, jual beli bibit, jual bibit puyuh, jual burung puyuh murah, bisnis bibit puyuh, bibit puyuh jogya</textarea>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label"></label>
						<div class="col-sm-10">
							<p class="form-control-static">
								<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-saved"></span> Save Berita</button>
								<button type="submit" class="btn btn-danger"> Cancel Berita</button>
							</p>
						</div>
					</div>
					
				</form>

			</div><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>