<!DOCTYPE html>
<html>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<style type="text/css" media="screen">
	
	@media (min-width: 868px) { 
	    #mobile {
		display: none;
	}  
	}
	@media (max-width: 768px) {

	    #desktop {
	    	display: none;
	    }

	    th {
    		font-size: smaller;
    	}

	    .table td {
	    	font-size: 12px;
	    }

	}
</style>
<body class="with-side-menu">

	<?php $this->load->view('administrator/nav_menu'); ?>
	<?php $this->load->view('administrator/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Konfirmasi Pembayaran</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="<?php echo base_url('administrator/'); ?> ">Beranda</a></li>
								<li class="active">Konfirmasi Pembayaran</li>
							</ol>
						</div>
					</div>
				</div>

				<?php if (isset($_SESSION['message_data'])): ?>
				<div class="alert alert-aquamarine alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['message_data'] ?>
				</div>
				<?php endif ?>

				<?php if (isset($_SESSION['error_data'])): ?>
				<div class="alert alert-danger alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['error_data'] ?>
				</div>
				<?php endif ?>
			</header>

			<section class="card">
				<div class="card-block">
					<table id="data_aktivitas" class="display table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>No</th>
							<th>Kode Order</th>
							<th>Pembayaran</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						</thead>
						<?php $i =1; foreach ($data_konfirmasi_transaksi as $dt_konfirmasi_transaksi): ?>
						<tbody>
							<td><?php echo $i++ ?></td>
							<td>#<?php echo $dt_konfirmasi_transaksi->kode_order ?></td>
							<td>
								<?php if ($dt_konfirmasi_transaksi->pembayaran == "uang_muka"): ?>
									PEMBAYARAN UANG MUKA
								<?php elseif ($dt_konfirmasi_transaksi->pembayaran == "uang_sisa"): ?>
									PEMBAYARAN SISA
								<?php else: ?>
									PEMBAYARAN LUNAS
								<?php endif ?>
							</td>
							<td>
								<?php if ($dt_konfirmasi_transaksi->status_bukti == "menunggu"): ?>
									<span id="mobile" class="label label-warning"><span class="glyphicon glyphicon-refresh"></span></span>
									<span id="desktop" class="label label-warning">Menunggu Verifikasi</span>
								<?php elseif ($dt_konfirmasi_transaksi->status_bukti == "unvalid"): ?>
									<span class="label label-danger">Tidak Valid</span>
								<?php else: ?>
									<span class="label label-success">Cocok</span>
								<?php endif ?>
							</td>
							<td>
								<?php if ($dt_konfirmasi_transaksi->status_bukti == "menunggu"): ?>

									<a id="mobile" class="btn-sm btn-info" href="<?php echo base_url('administrator/detail_konfirmasi/'.$dt_konfirmasi_transaksi->kode_order.'/'.$dt_konfirmasi_transaksi->kode_pelanggan.'/'.$dt_konfirmasi_transaksi->kode_transaksi.'/'.$dt_konfirmasi_transaksi->kode_bukti) ?>"><span class="glyphicon glyphicon-file"></span></a>

									<a id="desktop" class="btn btn-sm btn-info" href="<?php echo base_url('administrator/detail_konfirmasi/'.$dt_konfirmasi_transaksi->kode_order.'/'.$dt_konfirmasi_transaksi->kode_pelanggan.'/'.$dt_konfirmasi_transaksi->kode_transaksi.'/'.$dt_konfirmasi_transaksi->kode_bukti) ?>" role="button">Detail Konfirmasi</a>
								<?php else: ?>
									<span class="label label-success">Selesai Checkin</span>
								<?php endif ?>
							</td>
						</tbody>
						<?php endforeach ?>
					</table>
				</div>
			</section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>