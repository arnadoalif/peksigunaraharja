<!DOCTYPE html>
<html>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<body class="with-side-menu">

	<?php $this->load->view('administrator/nav_menu'); ?>
	<?php $this->load->view('administrator/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">
			
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Harga Jual</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="<?php echo base_url('administrator/'); ?> ">Beranda</a></li>
								<li class="active">Harga Jual</li>
							</ol>
						</div>
					</div>
				</div>

				<?php if (isset($_SESSION['message_data'])): ?>
				<div class="alert alert-aquamarine alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['message_data'] ?>
				</div>
				<?php endif ?>

				<?php if (isset($_SESSION['error_data'])): ?>
				<div class="alert alert-danger alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['error_data'] ?>
				</div>
				<?php endif ?>

			</header>

			<?php if (!empty($data_harga)): ?>
				
				<div class="box-typical box-typical-padding">
				<form action="<?php echo base_url().'administrator/action_update_harga'; ?> " method="POST" enctype="multipart/form-data">
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Harga Jual</label>
						<div class="col-sm-5">
							<p class="form-control-static">
								<input type="hidden" name="kode_harga" value="<?php echo $data_harga->kode_harga ?> " placeholder="">
							   <input type="text" name="harga_jual" class="form-control" placeholder="Harga Jual" value="<?php echo $data_harga->harga_jual ?>" required>
							</p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Jumlah</label>
						<div class="col-sm-5">
							<p class="form-control-static">
							   <input type="text" name="jumlah" class="form-control" placeholder="Jumlah DOQ" value="<?php echo $data_harga->jumlah ?>" required>
							</p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Presentase Penjualan</label>
						<div class="col-sm-5">
							<p class="form-control-static">
							   <input type="text" name="presentase_penjualan" class="form-control" placeholder="%" value="<?php echo $data_harga->presentase ?>" required>
							</p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Satuan</label>
						<div class="col-sm-5">
							<select name="satuan" id="inputSatuan" class="form-control" required="required">
								<option value="">Satuan</option>
								<option <?php echo $data_harga->satuan == 'Ekor' ? 'selected = "selected"': ''; ?> value="Ekor">\ Ekor</option>
								<option <?php echo $data_harga->satuan == 'Pack' ? 'selected = "selected"': ''; ?> value="Pack">\ Pack</option>
							</select>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-2 form-control-label"></label>
						<div class="col-sm-10">
							<p class="form-control-static">
								<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-saved"></span> Update Harga</button>
								<button type="submit" class="btn btn-danger"> Cancel Harga</button>
							</p>
						</div>
					</div>
				</form>
			</div><!--.box-typical-->

			<?php else: ?>
				
			<div class="box-typical box-typical-padding">
				<form action="<?php echo base_url().'administrator/action_insert_harga_jual'; ?> " method="POST" enctype="multipart/form-data">
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Harga Jual</label>
						<div class="col-sm-5">
							<p class="form-control-static">
							   <input type="text" name="harga_jual" class="form-control" placeholder="Harga Jual" required>
							</p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Jumlah</label>
						<div class="col-sm-5">
							<p class="form-control-static">
							   <input type="text" name="jumlah" class="form-control" placeholder="Jumlah DOQ" required>
							</p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Satuan</label>
						<div class="col-sm-5">
							<select name="satuan" id="inputSatuan" class="form-control" required="required">
								<option value="">Satuan</option>
								<option value="Ekor">\ Ekor</option>
								<option value="Pack">\ Pack</option>
							</select>
						</div>
					</div>
					
					<div class="form-group row">
						<label class="col-sm-2 form-control-label"></label>
						<div class="col-sm-10">
							<p class="form-control-static">
								<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-saved"></span> Masukan Harga</button>
								<button type="submit" class="btn btn-danger"> Cancel Harga</button>
							</p>
						</div>
					</div>
					
				</form>

			</div><!--.box-typical-->

			<?php endif ?>

		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>