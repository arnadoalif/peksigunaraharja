<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url('assets_front/js/jquery.min.js'); ?> "></script>
<script src="<?php echo base_url('assets_front/js/modernizr.custom.js'); ?> "></script>
<script src="<?php echo base_url('assets_front/js/jquery.easing.1.3.js'); ?> "></script>
<script src="<?php echo base_url('assets_front/js/bootstrap.min.js'); ?> "></script>
<script src="<?php echo base_url('assets_front/plugins/flexslider/jquery.flexslider-min.js'); ?> "></script> 
<script src="<?php echo base_url('assets_front/plugins/flexslider/flexslider.config.js'); ?> "></script>
<script src="<?php echo base_url('assets_front/js/jquery.appear.js'); ?> "></script>
<script src="<?php echo base_url('assets_front/js/stellar.js'); ?> "></script>
<script src="<?php echo base_url('assets_front/js/classie.js'); ?> "></script>
<script src="<?php echo base_url('assets_front/js/uisearch.js'); ?> "></script>
<script src="<?php echo base_url('assets_front/js/jquery.cubeportfolio.min.js'); ?> "></script>
<script src="<?php echo base_url('assets_front/js/google-code-prettify/prettify.js'); ?> "></script>
<script src="<?php echo base_url('assets_front/js/animate.js'); ?> "></script>
<script src="<?php echo base_url('assets_front/js/wow.min.js'); ?> "></script>
<script src="<?php echo base_url('assets_front/js/wow.js'); ?> "></script>
<script src="<?php echo base_url('assets_front/js/masonry.pkgd.min.js'); ?> "></script>
<script src="<?php echo base_url('assets_front/js/lazyload/jquery.lazyload.min.js'); ?> "></script>
<script src="<?php echo base_url('assets_front/js/custom.js'); ?> "></script>
<script type="text/JavaScript">
		//courtesy of BoogieJack.com
		//function killCopy(e){
		//return false
		//}
		//function reEnable(){
		//return true
		//}
		//document.onselectstart=new Function ("return false")
		//if (window.sidebar){
		//document.onmousedown=killCopy
		//document.onclick=reEnable
		//}
</script>

<!-- Start of LiveChat (www.livechatinc.com) code -->
<!-- <script type="text/javascript">
	window.__lc = window.__lc || {};
	window.__lc.license = 9044715;
	(function() {
	var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
	lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
	})();
</script> -->
	<!-- End of LiveChat code -->

<!--Start of Tawk.to Script-->
<script type="text/javascript">
	var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
	(function(){
	var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
	s1.async=true;
	s1.src='https://embed.tawk.to/59dda6a7c28eca75e4625572/default';
	s1.charset='UTF-8';
	s1.setAttribute('crossorigin','*');
	s0.parentNode.insertBefore(s1,s0);
	})();
</script>
<!--End of Tawk.to Script-->


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9W8kzf0p7qMy4S7ulgyaFTaodr5vsIYQ"></script>
<script>
jQuery(document).ready(function( $ ) {
    
    //Google Map
    var get_latitude = $('#google-map').data('latitude');
    var get_longitude = $('#google-map').data('longitude');

    function initialize_google_map() {
        var myLatlng = new google.maps.LatLng(get_latitude, get_longitude);
        var mapOptions = {
            zoom: 18,
            scrollwheel: false,
            center: myLatlng
        };
        var map = new google.maps.Map(document.getElementById('google-map'), mapOptions);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize_google_map);
    
});
</script>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		new WOW().init();
	});
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107915608-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-107915608-1');
</script>

<!-- <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
	(adsbygoogle = window.adsbygoogle || []).push({
	google_ad_client: "ca-pub-4856757937581125",
	enable_page_level_ads: true
	});
</script> -->

<!-- <script src="contactform/contactform.js"></script> -->

<!-- <div id="google_translate_element"></div> -->
<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'id', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, multilanguagePage: true, gaTrack: true, gaId: 'UA-107915608-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<!-- full calender js -->
<script src='<?php echo base_url('assets_front/fullcalendar/lib/moment.min.js'); ?>'></script>
<script src='<?php echo base_url('assets_front/fullcalendar/fullcalendar.min.js') ?>'></script>
<script id="wappalyzer" src="chrome-extension://gppongmhjkpfnbhagpmjfkannfbllamg/js/inject.js"></script>