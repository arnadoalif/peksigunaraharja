<!DOCTYPE html>
<html>
<title>Admin Member PT.Peksi Gunaraharja</title>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<body class="with-side-menu">

	<?php $this->load->view('memberpage/nav_menu'); ?>
	<?php $this->load->view('memberpage/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Checkout Proses</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="<?php echo base_url('member/'); ?> ">Beranda</a></li>
								<li class="active">Checkout Proses</li>
							</ol>
						</div>
					</div>
				</div>

				<?php if (isset($_SESSION['message_data'])): ?>
				<div class="alert alert-aquamarine alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['message_data'] ?>
				</div>
				<?php endif ?>

				<?php if (isset($_SESSION['error_data'])): ?>
				<div class="alert alert-danger alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['error_data'] ?>
				</div>
				<?php endif ?>

			</header>
			
			<form action="<?php echo base_url('member/action_insert_transaksi_order') ?>" method="POST">
			<?php $i = 1; foreach ($data_order as $dt_order): ?>
			<input type="hidden" name="kode_pelanggan" value="<?php echo $dt_order->kode_pelanggan; ?>">
			<input type="hidden" name="kode_order" value="<?php echo $dt_order->kode_order ?>">
			<section class="card">
				<div class="card-block">
					<div class="col-md-7 col-lg-7">
							<fieldset class="form-group">
								<label class="form-label semibold" for="nama_lengkap">Nama Lengkap</label>
								<input type="text" class="form-control" disabled readonly name="nama_lengkap" required id="nama_lengkap" value="<?php echo $dt_order->nama_pelanggan ?> " placeholder="Nama Lengkap">
							</fieldset>
							<fieldset class="form-group">
								<label class="form-label semibold" for="email_address">Email</label>
								<input type="text" name="email_address" disabled readonly class="form-control" required id="email_address" value="<?php echo $dt_order->email_address ?>" placeholder="Email">
							</fieldset>
							<fieldset class="form-group">
								<label class="form-label semibold" for="nomor_telepon">Nomor Telepon</label>
								<input type="text" name="nomor_telepon" disabled readonly class="form-control" required id="nomor_telepon" value="<?php echo $dt_order->nomor_telepon ?>" placeholder="Nomor Telepon">
							</fieldset>
							<fieldset class="form-group">
								<label class="form-label semibold" for="alamat">Alamat</label>
								<input type="text" name="alamat" disabled readonly class="form-control" required id="alamat" value="<?php echo $dt_order->alamat ?>" placeholder="Alamat">
							</fieldset>
							<fieldset class="form-group">
								<label class="form-label semibold" for="kota">Provinsi</label>
								<input type="text" name="kota" disabled readonly class="form-control" required id="kota" value="<?php echo $dt_order->kota ?>" placeholder="Alamat">
							</fieldset>
					</div>
					<div class="col-md-5 col-lg-5">
						<section class="card">
								<header class="card-header card-header-lg">
									Pembayaran Melalui
								</header>
							<div class="card-block">
								<p class="card-text">Transfer ke rekening kami melalui ATM, Internet bangking, SMS banking atau setoran tunai <br><br> Pilih bank yang akan di gunakan</p>
								<ul class="list-group">
									<li class="list-group-item">
										<div class="radio">
											<input type="radio" name="jenis_rekening" id="radio-1" value="bca" selected required>
											<label for="radio-1"><img style="width: 55%;" src="<?php echo base_url('assets/img/peyment/icon-bca.png') ?> " alt=""></label>
										</div>
									</li>
									<!-- <li class="list-group-item">
										<div class="radio">
											<input type="radio" name="jenis_rekening" id="radio-2" value="mandiri">
											<label for="radio-2"><img style="width: 55%;" src="<?php echo base_url('assets/img/peyment/mandiri.png') ?> " alt=""> </label>
										</div>
									</li>
									<li class="list-group-item">
										<div class="radio">
											<input type="radio" name="jenis_rekening" id="radio-3" value="bni">
											<label for="radio-3"><img style="width: 35%;" src="<?php echo base_url('assets/img/peyment/bni.png') ?> " alt=""> </label>
										</div>
									</li> -->
								</ul>
							</div>
						</section>
					</div>
				</div>
				<div class="card-block">
					<div class="col-md-12 col-lg-12">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Pesanaan</th>
										<th>Keterangan</th>
										<th>Tanggal Pesan</th>
										<th>Tanggal Kirim</th>
										<th>Jumlah DOQ</th>
										<th>Harga Satuan</th>
										<th>Total DOQ</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><?php echo $i++; ?></td>
										<td>DOQ</td>
										<td>Day Of Quail</td>
										<td><?php echo date("d/m/Y", strtotime($dt_order->tanggal_order)); ?></td>
										<td><?php echo $dt_order->tanggal_kirim ?></td>
										<td><?php echo number_format($dt_order->jumlah_order,0,".","."); ?></td>
										<td>Rp. <?php echo number_format($dt_order->satuan_doq,0,".","."); ?></td>
										<td><b>Rp. <?php echo number_format($dt_order->total_harga,0,".","."); ?></b></td>
										<td>*</td>
									</tr>
									<tr>
										<td colspan="7" align="right">Uang Muka <b> <?php echo $dt_order->presentase ?>% </b></td>
										<input type="hidden" name="uang_muka" value="<?php echo $dt_order->uang_muka ?>">
										<td><b>Rp. <?php echo number_format($dt_order->uang_muka,0,".","."); ?></b></td>
										<td>
											<?php if ($dt_order->status_uang_muka != "Lunas"): ?>
												<span class="label label-danger">Belum Lunas</span>
											<?php else: ?>
												<span class="label label-success">Lunas</span>
											<?php endif ?>
										</td>
									</tr>
									<tr>
										<td colspan="7" align="right">Sisa</td>
										<td><b>Rp. <?php $sisa = ($dt_order->total_harga - $dt_order->uang_muka); echo number_format($sisa,0,".","."); ?></b></td>
										<input type="hidden" name="sisa_uang" value="<?php echo $sisa ?>">
										<td>
											<?php if ($dt_order->status_uang_sisa != "Lunas"): ?>
												<span class="label label-danger">Belum Lunas</span>
											<?php else: ?>
												<span class="label label-success">Lunas</span>
											<?php endif ?>
										</td>
									</tr>
									<tr>
										<td colspan="7" align="right">Total Harga</td>
										<td><b>Rp. <?php echo number_format($dt_order->total_harga,0,".","."); ?></b></td>
										<input type="hidden" name="total_harga" value="<?php echo $dt_order->total_harga ?>">
										<td>
										    <?php if ($dt_order->status_uang_sisa != "Lunas" && $dt_order->status_uang_muka != "Lunas"): ?>
											<?php else: ?>
												<span class="label label-success">Lunas</span>
											<?php endif ?>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<button type="submit" class="btn btn-success">Checkout Pesanan</button>
					</div>

				</div>
			</section>
			<?php endforeach ?>
			</form>

		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
<script>
	jQuery(document).ready(function($) {
            $.ajax({
            type: "GET",
            url: "http://localhost:8081/peksi/assets_front/js/indonesia.json",
            dataType: "json",
            success: function (data) {
                $.each(data, function(i, value) {
                    $('#provinsi').append($('<option>').text(value).attr('value', value));
                });
            },
            error: function (result) {
                console.log("Error");
            }
        });
      });
</script>
</body>
</html>