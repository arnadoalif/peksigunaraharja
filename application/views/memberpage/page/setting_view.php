<!DOCTYPE html>
<html>
<title>Admin Member PT.Peksi Gunaraharja</title>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<body class="with-side-menu">

	<?php $this->load->view('memberpage/nav_menu'); ?>
	<?php $this->load->view('memberpage/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Pengaturan Account</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="<?php echo base_url('member/') ?>">Beranda</a></li>
								<li class="active">Pengaturan Account</li>
							</ol>
						</div>
					</div>
				</div>
			</header>

			<div class="box-typical box-typical-padding">
				<form action="<?php echo base_url('member/action_update_member') ?>" method="post">
					<div class="form-group row">
						<input type="hidden" name="kode_pelanggan" value="<?php echo $data_member->kode_pelanggan ?>" placeholder="">
						<label class="col-sm-2 form-control-label">Nama</label>
						<div class="col-sm-10">
							<p class="form-control-static"><input type="text" name="nama_pelanggan" class="form-control" id="inputPassword" placeholder="Nama Pelanggan" value="<?php echo $data_member->nama_pelanggan ?>"></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Nomor Telepon</label>
						<div class="col-sm-10">
							<p class="form-control-static">
								<input type="text" name="nomor_telepon" class="form-control" id="inputPassword" placeholder="Nomor Telepon" value="<?php echo $data_member->nomor_telepon ?>">
							</p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Kota</label>
						<div class="col-sm-10">
							<p class="form-control-static">
								<select name="kota" id="provinsi" class="form-control" required="required">
									<option value="<?php echo $data_member->kota ?>" selected><?php echo $data_member->kota ?></option>
								</select>
							</p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">Alamat Pengiriman</label>
						<div class="col-sm-10">
							<p class="form-control-static">
								<textarea name="alamat" id="inputAlamat" class="form-control" rows="3" required="required"><?php echo $data_member->alamat ?></textarea>
							</p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label"></label>
						<div class="col-sm-10">
							<button type="submit" class="btn btn-success">Perbaharui</button>
							<a class="btn btn-danger" href="<?php echo base_url('member/') ?>" role="button">Batal</a>
						</div>
					</div>
					
				</form>

			</div><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
<script type="text/javascript">
        jQuery(document).ready(function($) {
        	var base_url = window.location.origin;
            $.ajax({
            type: "GET",
            url: base_url+"/assets_front/js/indonesia.json",
            dataType: "json",
            success: function (data) {
                $.each(data, function(i, value) {
                    $('#provinsi').append($('<option>').text(value).attr('value', value));
                });
            },
            error: function (result) {
                console.log("Error");
            }
        });
        });
    </script>
</body>
</html>