﻿<!DOCTYPE html>
<html>
<title>Admin Member PT.Peksi Gunaraharja</title>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<body class="with-side-menu">

	<?php $this->load->view('memberpage/nav_menu'); ?>
	<?php $this->load->view('memberpage/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Pesan DOQ</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="<?php echo base_url('member/'); ?> ">Beranda</a></li>
								<li class="active">Pesan DOQ</li>
							</ol>
						</div>
					</div>
				</div>

				<?php if (isset($_SESSION['message_data'])): ?>
				<div class="alert alert-aquamarine alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['message_data'] ?>
				</div>
				<?php endif ?>

				<?php if (isset($_SESSION['error_data'])): ?>
				<div class="alert alert-danger alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['error_data'] ?>
				</div>
				<?php endif ?>

			</header>

			<div class="box-typical box-typical-padding">
				<form action="<?php echo base_url().'member/action_order'; ?> " method="POST" enctype="multipart/form-data">
					<div class="form-group row">
						<label class="col-sm-3 form-control-label"><strong>Minggu Ke </strong></label>
						<div class="col-sm-5">
							<p class="form-control-static">
							<?php echo substr($periode, 2); ?>
							<input type="hidden" name="periode" value="<?php echo$periode?>">
							</p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label"><strong>Tanggal Kirim</strong></label>
						<div class="col-sm-5">
							<p class="form-control-static">
							<?php
								$sql = "select * from Dbo.getHari ('$tgl')";
								$data_order = $this->db->query($sql)->result_array();
							?>
							<?php echo($data_order[0]['NmHari']); echo ", ".$tgl; ?>
							<input type="hidden" name="tanggal_kirim" value="<?php echo $tgl ?>">
							</p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label"><strong>Total DOQ Minggu Ke - <?php echo substr($periode, 2); ?></strong></label>
						<div class="col-sm-4">
							<p class="form-control-static">
								<?php echo number_format($total); ?>
								<input type="hidden" name="total_doq" value="<?php echo $total ?>">
							</p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label"><strong>Sisa DOQ Minggu Ke - <?php echo substr($periode, 2); ?></strong></label>
						<div class="col-sm-4">
							<p class="form-control-static">
								<?php 
									$sql_order = "select jumlah_order from tbl_order where tanggal_kirim = '$tgl'";
									$jumlah = 0;
									$total_order = $this->db->query($sql_order)->result();
									foreach ($total_order as $value) {
										$jumlah =  $jumlah + $value->jumlah_order;
									}
									$sisa = $total - $jumlah;
									echo(number_format($sisa));
								 ?>
								<input type="hidden" name="sisa" value="<?php echo $sisa ?>">
							</p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label"><strong>Jumlah Pesan</strong></label>
						<div class="col-sm-4">
							<p class="form-control-static">
								<input type="text" name="jumlah_order" value="" class="form-control" id="jumlah_order" placeholder="Jumlah Kirim" required> 
							</p>
						</div>
					</div>
					
		
					<div class="form-group row">
						<label class="col-sm-3 form-control-label"></label>
						<div class="col-sm-4">
							<p class="form-control-static">
							<?php if (count($data_order) == 3): ?>
								<span class="label label-danger">Max Order DOQ 3x</span>
							<?php else: ?>
								<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-shopping-cart"></span> Pesan Sekarang</button>
							<?php endif ?>
								<a class="btn btn-danger" href="<?php echo base_url('member') ?> " role="button">Batal Order</a>
							</p>
						</div>
					</div>
					
				</form>

			</div><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>