<!DOCTYPE html>
<html>
<title>Admin Member PT.Peksi Gunaraharja</title>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<body class="with-side-menu">

	<?php $this->load->view('memberpage/nav_menu'); ?>
	<?php $this->load->view('memberpage/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">
			
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h5>Riwayat Pesanan</h5>
						</div>
					</div>
				</div>
			</header>

			<section class="card">
				<div class="card-block">
					<table id="data_aktivitas" class="display table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>No</th>
							<th>Tanggal Kirim</th>
							<th>Tanggal Pesan</th>
							<th>Jumlah Pesan</th>
							<th>Total Harga</th>
							<th>Status Pesan</th>
						</tr>
						</thead>
						
						<tbody>
						<?php $i=1; foreach ($data_order as $dt_order): ?>
						<tr>
							<td><?php echo $i++; ?></td>
							<td><?php echo date("d-m-Y", strtotime($dt_order->tanggal_kirim)); ?></td>
							<td><?php echo date("d-m-Y", strtotime($dt_order->tanggal_order)); ?></td>
							<td><?php echo number_format($dt_order->jumlah_order); ?> ekor</td>
							<td>
								<?php if (empty($dt_order->total_harga)): ?>
									<span class="label label-warning">Menunggu Admin</span>
								<?php else: ?>
									<?php echo "Rp.".$dt_order->total_harga ?>
								<?php endif ?>
							</td>
							<td>
								<?php if ($dt_order->status_order == "diterima"): ?>
									<span class="label label-success">Diterima</span>
								<?php elseif ($dt_order->status_order == "gagal"): ?>
									<span class="label label-danger">Gagal</span>
								<?php endif ?>
							</td>
						</tr>
						<?php endforeach ?>

						</tbody>
					</table>
				</div>
			</section>

		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>