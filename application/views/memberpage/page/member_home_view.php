<!DOCTYPE html>
<html>
<title>Admin Member PT.Peksi Gunaraharja</title>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<style type="text/css" media="screen">
	
	@media (min-width: 868px) { 
	    #mobile {
		display: none;
	}  
	}
	@media (max-width: 768px) {

	    #desktop {
	    	display: none;
	    }

	    th {
    		font-size: smaller;
    	}

	    .table td {
	    	font-size: 12px;
	    }

	}
</style>

<body class="with-side-menu">

	<?php $this->load->view('memberpage/nav_menu'); ?>
	<?php $this->load->view('memberpage/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">

			<div class="col-lg-12">
				<div class="col-xs-6 col-sm-6 col-md-3">
					<a href="<?php echo base_url('member/list_order') ?>">
						<article class="statistic-box yellow">
							<div>
								<div class="caption"><div>Di Proses</div></div>
								<div class="number"><?php echo $status_diproses ?></div>
							</div>
						</article>
					</a>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<a href="<?php echo base_url('member/list_order') ?>">
						<article class="statistic-box purple">
							<div>
								<div class="caption"><div>Terkirim</div></div>
								<div class="number"><?php echo $status_dikirim ?></div>
							</div>
						</article>
					</a>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<a href="<?php echo base_url('member/history_order') ?>">
						<article class="statistic-box green">
							<div>
								<div class="caption"><div>Diterima</div></div>
								<div class="number"><?php echo $status_diterima ?></div>
							</div>
						</article>
					</a>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<a href="<?php echo base_url('member/list_order') ?>">
						<article class="statistic-box red">
							<div>
								<div class="caption"><div>Gagal</div></div>
								<div class="number"><?php echo $status_gagal ?></div><br>
							</div>
						</article>
					</a>
				</div>
			</div>

			<div class="row">

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h2 align="center">Total DOQ Tiap Minggunya</h2>
					<section class="widget widget-accordion" id="" role="tablist" >
						<?php foreach ($total_doq as $key => $dt_total_doq): ?>

							<article class="panel">
								<div class="panel-heading" role="tab" id="headingOne<?php echo $dt_total_doq->periode ?> ">
									<a data-toggle="collapse"
									   data-parent="#accordion"
									   href="#collapseOne<?php echo $dt_total_doq->periode ?>"
									   aria-expanded="true"
									   aria-controls="collapseOne<?php echo $dt_total_doq->periode ?>">
										<span class="glyphicon glyphicon-calendar"></span> <?php echo "Minggu ke- ".$dt_total_doq->minggu ?> Total DOQ <?php echo number_format($dt_total_doq->jumlah); ?>
										<i class="font-icon font-icon-arrow-down"></i>
									</a>
								</div>
								<div id="collapseOne<?php echo $dt_total_doq->periode ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne<?php echo $dt_total_doq->periode ?>">
									<div class="panel-collapse-in">
										<div class="user-card-row">
											<div class="tbl-row">
												<div class="tbl-cell">
													<div class="table-responsive">
														<table class="table table-hover">
															<thead>
																<tr>
																	<th style="text-align: center;">Tanggal Pengiriman</th>
																	<th style="text-align: center;">Total DOQ</th>
																	<th style="text-align: center;">Sudah dipesan</th>
																	<th style="text-align: center;">Sisa DOQ</th>
																	<th style="text-align: center;">
																		<?php if (count($data_order) == 3): ?>
																			Pemberitahuan
																		<?php else: ?>
																			Aksi
																		<?php endif ?>
																	</th>
																</tr>
															</thead>

															<?php 
															$this->load->model('Migratesystem_model');
															$m_migrate_system = new Migratesystem_model();
															$data_detail =$m_migrate_system->detail_doq_order_member($dt_total_doq->periode)->result();
															 ?>

															<tbody>
															
																<?php foreach ($data_detail as $dt_detail): ?>
																	<tr>
																		<?php 
																			$sql_order = "select jumlah_order from tbl_order where tanggal_kirim = '$dt_detail->tgl'";
																			$jumlah = 0;
																			$total_order = $this->db->query($sql_order)->result();
																			foreach ($total_order as $value) {
																				$jumlah =  $jumlah + $value->jumlah_order;
																			}
																			$sisa = $dt_detail->total - $jumlah;
																		?>
																		<td style="text-align: center;"><?php echo $dt_detail->tgl ?></td>
																		<td style="text-align: center;">
																			<?php if ($sisa == '0'): ?>
																				<strike><?php echo number_format($dt_detail->total)  ?> Ekor </strike>
																			<?php else: ?>
																				<?php echo number_format($dt_detail->total)  ?> Ekor 
																			<?php endif ?>
																		</td>
																		<td style="text-align: center;">
																			<?php echo $jumlah; ?>
																		</td>
																		<td style="text-align: center;">
																			<?php 
																				echo ($sisa);
																			 ?>
																		</td>
																		<td style="text-align: center;">
																			<?php if (count($data_order) == 3): ?>
																				<span class="label label-danger">Max Pesan DOQ 3x</span>
																			<?php else: ?>
																				<?php 
																						$kode_pelanggan = $this->session->userdata('kode_pelanggan');
																						$sql_check_order = "select * from tbl_order where kode_pelanggan = '$kode_pelanggan' and tanggal_kirim = '$dt_detail->tgl'";
																						$cek_order = $this->db->query($sql_check_order)->result();
																				 ?>
																				 <?php if (count($cek_order) > 0): ?>
																				 		<?php if ($cek_order[0]->status_order == "diterima"): ?>
																				 			<?php if ($sisa == 0): ?>
																				 				<span class="label label-info">DOQ Habis</span>
																				 			<?php else: ?>

																				 				<a id="desktop" class="btn btn-sm btn-success" href="<?php echo base_url('member/order/'.$dt_detail->periode.'/'.sha1(str_replace("/", "", $dt_detail->tgl))); ?> " role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Pesan Sekarang</a> 

																				 				<a id="mobile" class="btn btn-sm btn-success" href="<?php echo base_url('member/order/'.$dt_detail->periode.'/'.sha1(str_replace("/", "", $dt_detail->tgl))); ?> " role="button"><span class="glyphicon glyphicon-shopping-cart"></span></a>

																				 			<?php endif ?>
																				 		<?php else: ?>
																				 			<span id="desktop" class="label label-info">Pemesanan Tersedia</span>
																				 			<span id="mobile" class="label label-info"> <span class="glyphicon glyphicon-pushpin"></span> </span>
																				 		<?php endif ?>
																				 <?php else: ?>
																				 		<?php if ($sisa == '0'): ?>
																							<span class="label label-info">Stok Habis</span>
																						<?php else: ?>
																							<a id="desktop" class="btn btn-sm btn-success" href="<?php echo base_url('member/order/'.$dt_detail->periode.'/'.sha1(str_replace("/", "", $dt_detail->tgl))); ?> " role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Pesan Sekarang</a>

																				 				<a id="mobile" class="btn btn-sm btn-success" href="<?php echo base_url('member/order/'.$dt_detail->periode.'/'.sha1(str_replace("/", "", $dt_detail->tgl))); ?> " role="button"><span class="glyphicon glyphicon-shopping-cart"></span></a>
																						<?php endif ?>
																				 	
																				 <?php endif ?>
																			<?php endif ?>
																		</td>
																	</tr>
																<?php endforeach ?>
															
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
										
										
									</div>
								</div>
							</article>

						<?php endforeach ?>
						
					</section><!--.widget-accordion-->
				</div>
			</div><!--.row-->
			
			

		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>