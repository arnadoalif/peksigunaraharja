<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title></title>
		<link rel="stylesheet" href="<?php echo base_url('assets/css/lib/bootstrap/bootstrap.min.css'); ?> ">
		<style type="text/css">
			.invoice-title h2, .invoice-title h3 {
			    display: inline-block;
			}

			.table > tbody > tr > .no-line {
			    border-top: none;
			}

			.table > thead > tr > .no-line {
			    border-bottom: none;
			}

			.table > tbody > tr > .thick-line {
			    border-top: 2px solid;
			}
		</style>
	</head>
	<body>
	<?php foreach ($data_order as $dt_order): ?>
		
	
		<div class="container">
			<div class="row">
				<div class="col-xs-5">
					<div class="panel panel-default">
						<div class="panel-heading">
							<img src="<?php echo base_url('assets_front/img/logo_peksi.jpg') ?>" class="img-responsive" alt="Image" style="width: 15%;">
						</div>
						<div class="panel-body">
							
						</div>
					</div>
				</div>
				<div class="col-xs-5 col-xs-offset-8 text-right" style="margin-left: 19em;">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4># <?php echo $dt_order->kode_order ?></h4>
						</div>
						<div class="panel-body">
							<p>
								<?php 
									    $tanggal_order = date("d/m/Y", strtotime($dt_order->tanggal_order));
										$sql = "select * from Dbo.getHari ('$tanggal_order')";
										$data_order = $this->db->query($sql)->result_array();
									 ?>
								TANGGAL PESAN	<?php echo($data_order[0]['NmHari'].","); echo " ".$tanggal_order; ?> <br>
								<?php 
										$sql = "select * from Dbo.getHari ('$dt_order->tanggal_kirim')";
										$data_order = $this->db->query($sql)->result_array();
									 ?>
								TANGGAL KIRIM	<?php echo($data_order[0]['NmHari'].","); echo " ".$dt_order->tanggal_kirim; ?> <br>
							</p>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-5">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>From: PT. Peksi Gunaraharja</h4>
						</div>
						<div class="panel-body">
							<p>
								Jl. Cangkringan Km 5, Ngasem, Selomortani, Kalasan, Sleman<br>
								Yogyakarta 55571<br>
								Telepon (0274) 285063</p>
							</p>
						</div>
					</div>
				</div>
				<div class="col-xs-5 col-xs-offset-8 text-right" style="margin-left: 19em;">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>To :<?php echo $dt_order->nama_pelanggan ?></h4>
						</div>
						<div class="panel-body">
							<p>
								<?php echo $dt_order->nomor_telepon ?> <br>
								<?php echo $dt_order->alamat ?> <br>
								<?php echo $dt_order->kota ?> <br>
							</p>
						</div>
					</div>
				</div>
			</div> <!-- / end client details section -->

			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><strong>UANG MUKA <?php echo $dt_order->presentase ?>% : Rp <?php echo number_format($dt_order->uang_muka,0,".","."); ?></strong></h3>
						</div>
						<div class="panel-body">
							<div class="table-responsive table">
								<table class="table table-condensed">
									<thead>
										<tr>
											<th>No</th>
											<th>Nama Pesanaan</th>
											<th>Keterangan</th>
											<th>Jumlah DOQ</th>
											<th>Harga Satuan</th>
											<th>Total DOQ</th>
										</tr>

									</thead>
									<tbody>
										<!-- foreach ($order->lineItems as $line) or some such thing here -->
										<tr>
											<td>1</td>
											<td class="text-center">DOQ</td>
											<td class="text-center">Day Of Quail</td>
											<td class="text-right"><?php echo number_format( $dt_order->jumlah_order,0,".",".");  ?></td>
											<td class="text-center">Rp. <?php echo number_format( $dt_order->satuan_doq,0,".",".");  ?></td>
											<td class="text-right">Rp. <?php echo number_format($dt_order->total_harga ,0,".",".");  ?></td>
										</tr>
										
										<tr>
											<td class="thick-line"></td>
											<td class="thick-line"></td>
											<td class="thick-line"></td>
											<td class="thick-line"></td>
											<th class="thick-line text-center"><b>Uang Muka <?php echo $dt_order->presentase ?>%</b></th>
											<td class="thick-line text-right">Rp. <?php echo number_format($dt_order->uang_muka, 0,".","."); ?></td>
										</tr>
										<tr>
											<td class="thick-line"></td>
											<td class="thick-line"></td>
											<td class="thick-line"></td>
											<td class="thick-line"></td>
											<th class="no-line text-center"><b>Sisa</b></th>
											<td class="no-line text-right">Rp. <?php $sisa = ($dt_order->total_harga - $dt_order->uang_muka); echo number_format($sisa,0,".","."); ?></td>
										</tr>
										<tr>
											<td class="thick-line"></td>
											<td class="thick-line"></td>
											<td class="thick-line"></td>
											<td class="thick-line"></td>
											<th class="no-line text-center"><b>Total Harga</b></th>
											<td class="no-line text-right">Rp. <?php echo number_format($dt_order->total_harga,0,".","."); ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-5">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h5>Metode Pembayaran</h5>
						</div>
						<div class="panel-body">
							<p style="font-weight: blod; font-family: arial;">
									Tagihan untuk invoice #<?php echo $dt_order->kode_order ?> bisa Anda bayarkan melalui: <br>
									Nama Bank : 
								<?php if ($dt_order->jenis_rekening == "bca"): ?>
									BCA (BANK CENTRAL ASIA)
								<?php elseif ($dt_order->jenis_rekening == "mandiri"): ?>
									MANDIRI
								<?php else: ?>
									BNI (BANK NEGARA INDONESIA)
								<?php endif ?> <br>
									No. Rek.  : <?php echo $dt_order->nomor_rekening ?><br>
									a/n       : <?php echo $dt_order->atas_nama; ?><br>
							</p>
						</div>
					</div>
				</div>
				<div class="col-xs-5 col-xs-offset-8 text-right" style="margin-left: 5em;">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h5>Catatan :</h5>
						</div>
						<div class="panel-body">
							<ul>
							    <li>Lakukan pembayaran sesuai jumlah uang muka (DP) yang tercantum pada invoice</li>
							    <li>Kirimkan pembayaran ke bank yang susuai dengan bank pilihan Anda saat melanjutkan pesanan.</li>
							    <li>Lakukan konfirmasi segera setelah pembayaran dikirimkan</li>
							</ul>
						</div>
					</div>
				</div>
			</div> <!-- / end client details section -->
		</div>

	<?php endforeach ?>
	</body>
</html>ml