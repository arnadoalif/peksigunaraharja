<!DOCTYPE html>
<html>
<title>Admin Member PT.Peksi Gunaraharja</title>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<style type="text/css" media="screen">
	@media (max-width: 768px) {

	    th {
    		font-size: smaller;
    	}

	    .table td {
	    	font-size: 12px;
	    }

	}
</style>
<body class="with-side-menu">

	<?php $this->load->view('memberpage/nav_menu'); ?>
	<?php $this->load->view('memberpage/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">
			<section class="card">
				<div class="card-block">
						<div class="col-md-12 col-lg-12">
							<div class="table-responsive">
								<table id="data_aktivitas" class="table table-hover">
									<thead>
										<tr>
											<th>No</th>
											<th>Pesan #</th>
											<th>Uang Muka</th>
											<th>Sisa</th>
											<th>Total Harga</th>
										</tr>
									</thead>
									<?php $i =1; foreach ($data_detail as $dt_detail): ?>
										<tbody>
											<tr>
												<td><?php echo $i++ ?></td>
												<td><?php echo $dt_detail->kode_order ?> </td>
												<td>Rp. <?php echo number_format($dt_detail->uang_muka ,0,".",".") ?> 
													<?php if ($dt_detail->status_bayar_uang_muka == "lunas"): ?>
														<span class="label label-success">Lunas</span>
													<?php else: ?>
														<span class="label label-danger">Belum Lunas</span>
													<?php endif ?>
												</td>
												<td>Rp. <?php echo number_format($dt_detail->sisa_uang ,0,".",".") ?> 
													<?php if ($dt_detail->status_bayar_uang_sisa == "lunas"): ?>
														<span class="label label-success">Lunas</span>
													<?php else: ?>
														<span class="label label-danger">Belum Lunas</span>
													<?php endif ?>

												</td>
												<td>Rp. <?php echo number_format($dt_detail->total_harga ,0,".","."); ?>
													<?php if ($dt_detail->status_bayar_uang_muka == "lunas" && $dt_detail->status_bayar_uang_sisa == "lunas"): ?>
														<span class="label label-success">Lunas</span>
													<?php else: ?>
														<span class="label label-danger">Belum Lunas</span>
													<?php endif ?>
												</td>
													
											</tr>
										</tbody>
									<?php endforeach ?>
								</table>
							</div>
						</div>
				</div>
			</section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>