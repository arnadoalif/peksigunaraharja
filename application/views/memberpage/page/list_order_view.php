<!DOCTYPE html>
<html>
<title>Admin Member PT.Peksi Gunaraharja</title>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<body class="with-side-menu">

	<?php $this->load->view('memberpage/nav_menu'); ?>
	<?php $this->load->view('memberpage/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">

			<div class="col-lg-12">
				<div class="col-xs-6 col-sm-6 col-md-3">
					<a href="">
						<article class="statistic-box yellow">
							<div>
								<div class="caption"><div>Di Proses</div></div>
								<div class="number"><?php echo $status_diproses ?></div>
							</div>
						</article>
					</a>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<a href="">
						<article class="statistic-box purple">
							<div>
								<div class="caption"><div>Terkirim</div></div>
								<div class="number"><?php echo $status_dikirim ?></div>
							</div>
						</article>
					</a>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<a href="">
						<article class="statistic-box green">
							<div>
								<div class="caption"><div>Diterima</div></div>
								<div class="number"><?php echo $status_diterima ?></div>
							</div>
						</article>
					</a>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<a href="<?php echo base_url('administrator/data_order') ?> ">
						<article class="statistic-box red">
							<div>
								<div class="caption"><div>Gagal</div></div>
								<div class="number"><?php echo $status_gagal ?></div><br>
							</div>
						</article>
					</a>
				</div>
			</div>

			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h5>Data Order</h5>
						</div>
					</div>
				</div>

				<?php if (isset($_SESSION['message_data'])): ?>
				<div class="alert alert-aquamarine alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['message_data'] ?>
				</div>
				<?php endif ?>

				<?php if (isset($_SESSION['error_data'])): ?>
				<div class="alert alert-danger alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['error_data'] ?>
				</div>
				<?php endif ?>
			</header>

			<section class="card">
				<div class="card-block">
				    <!-- <?php if (count($data_order) == 3): ?>
				    <?php else: ?>
				    	 <a class="btn btn-info btn-sm" href="<?php echo base_url('member/order'); ?>" role="button"><span class="glyphicon glyphicon-plus"></span> Tambah Order </a><br><br>
				    <?php endif ?> -->

					<table id="data_aktivitas" class="display table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>No</th>
							<th>Tanggal Pesan</th>
							<th>Tanggal Kirim</th>
							<th>Jumlah Pesan</th>
							<th>Satuan DOQ</th>
							<th>Uang Muka</th>
							<th>Total Harga</th>
							<th>Aksi</th>
						</tr>
						</thead>
						
						<tbody>
						<?php $i=1; foreach ($data_order as $dt_order): ?>
						<tr>
							<td><?php echo $i++; ?></td>
							<td><?php echo date("d/m/Y", strtotime($dt_order->tanggal_order)); ?></td>
							<td><?php echo $dt_order->tanggal_kirim; ?></td>
							<td><?php echo number_format($dt_order->jumlah_order,0,".",".");?> ekor</td>
							<td><?php echo number_format($dt_order->satuan_doq,0,".",".");?> ekor</td>
							<td><?php echo "Rp. ".number_format($dt_order->uang_muka,0,".","."); ?></td>
							<td>
								<?php if (empty($dt_order->total_harga)): ?>
									<span class="label label-warning">Menunggu Admin</span>
								<?php else: ?>
									<?php echo "Rp. ".number_format($dt_order->total_harga,0,".",".");?>
								<?php endif ?>
							</td>
							<td>
								<?php if ($dt_order->status_order == "menunggu"): ?>
									<span class="label label-warning">Menunggu</span>
									<a class="btn btn-sm btn-danger" href="<?php echo base_url('member/batal_order/'.$dt_order->kode_order.'/'.$dt_order->kode_pelanggan) ?> " role="button"><span class="glyphicon glyphicon-trash"></span> Batal Order</a>
								<?php elseif ($dt_order->status_order == "diproses"): ?>
									<?php if ($dt_order->status_checkout == "active"): ?>
										<?php if (($dt_order->status_uang_muka == "lunas") && ($dt_order->status_uang_sisa == "lunas")): ?>
											<span class="label label-success">Pembayaran LUNAS</span>
										<?php else: ?>
											<a class="btn btn-sm btn-info" href="<?php echo base_url('member/upload_bukti_tranfer/'.$dt_order->kode_order.'/'.$dt_order->kode_pelanggan) ?> " role="button"><span class="glyphicon glyphicon-upload"></span> Upload Bukti Tranfer</a>
										<?php endif ?>
									<?php else: ?>
										<a class="btn btn-sm btn-info" href="<?php echo base_url('member/peyment_order/'.$dt_order->kode_order.'/'.$dt_order->kode_pelanggan) ?> " role="button"><span class="glyphicon glyphicon-check"></span> Lanjut Order</a>
									<?php endif ?>

									<?php if (($dt_order->status_uang_muka == "lunas" || $dt_order->status_uang_sisa == "lunas")): ?>
										
									<?php else: ?>
										<a class="btn btn-sm btn-danger" href="<?php echo base_url('member/batal_order/'.$dt_order->kode_order.'/'.$dt_order->kode_pelanggan) ?> " role="button"><span class="glyphicon glyphicon-trash"></span> Batal Order</a>
									<?php endif ?>

								<?php elseif ($dt_order->status_order == "terkirim"): ?>
									<span class="label label-info">Terkirim</span>
								<?php else: ?>
									<span class="label label-danger">Gagal</span>
								<?php endif ?>
							</td>
							
						</tr>
						<?php endforeach ?>

						</tbody>
					</table>
				</div>
			</section>

			
		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>