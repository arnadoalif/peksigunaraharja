<!DOCTYPE html>
<html>
<?php require_once(APPPATH .'views/include/head_style.php'); ?>
<body class="with-side-menu">

	<?php $this->load->view('memberpage/nav_menu'); ?>
	<?php $this->load->view('memberpage/main_menu'); ?>

	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Upload Bukti Tranfer</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="<?php echo base_url('member/'); ?> ">Beranda</a></li>
								<li><a href="<?php echo base_url('member/list_order'); ?> ">Data Pesanan</a></li>
								<li class="active">Upload Bukti Tranfer</li>
							</ol>
						</div>
					</div>
				</div>
			</header>

			<?php if (isset($_SESSION['message_data'])): ?>
				<div class="alert alert-aquamarine alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<?php echo $_SESSION['message_data'] ?>
				</div>
				<?php endif ?>

			<?php if (isset($_SESSION['error_data'])): ?>
			<div class="alert alert-danger alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
				</button>
				<?php echo $_SESSION['error_data'] ?>
			</div>
			<?php endif ?>

			<div class="box-typical box-typical-padding">
				<form action="<?php echo base_url().'member/action_upload_bukti'; ?> " method="POST" enctype="multipart/form-data">
					<input type="hidden" name="kode_order" value="<?php echo $data_transfer->kode_order?>">
					<input type="hidden" name="kode_pelanggan" value="<?php echo $data_transfer->kode_pelanggan?>">
					<input type="hidden" name="kode_transaksi" value="<?php echo $data_transfer->kode_transaksi?>">
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">Upload Bukti <br> <sup style="color : red; font-weight: bold;">* ukuran ( < 1MB) Type file jpg,jpeg,png </sup></label>
						<div class="col-sm-9">
							<p class="form-control-static"><input type="file" name="bukti_transaksi" class="form-control" id="bukti_transaksi"></p>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-3 form-control-label">Pembayaran</label>
						<div class="col-sm-9">
							<select name="pembayaran" id="input" class="form-control" required="required">
								<option value="uang_muka">Uang Muka ( DP )</option>
								<option value="uang_sisa">Uang Sisa</option>
								<option value="lunas">Lunas</option>
								<option value="" selected>Pilih Pembayaran</option>
							</select>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-3 form-control-label"></label>
						<div class="col-sm-9">
							<p class="form-control-static">
								<button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Upload Bukti</button>
								<button type="submit" class="btn btn-danger"> Batal</button>
							</p>
						</div>
					</div>
				</form>

			</div><!--.box-typical-->
			
			<section class="card">
				<div class="card-block">
					<table id="data_aktivitas" class="display table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>No</th>
							<th>File Bukti</th>
							<th>Pembayaran</th>
							<th>Status</th>
						</tr>
						</thead>
						
						<tbody>
						<?php $i = 1; foreach ($data_upload_bukti as $dt_upload_bukti): ?>
							
						<tr>
							<td><?php echo $i++; ?></td>
							<td>
								<img src="<?php echo base_url('assets_pembayaran/upload_transfer/'.$dt_upload_bukti->nama_file_upload.'') ?>" class="img-responsive" alt="Image" style="width: 10%;">			
							</td>
							<td>
								<?php if ($dt_upload_bukti->pembayaran == "uang_muka"): ?>
									PEMBAYARAN UANG MUKA
								<?php elseif ($dt_upload_bukti->pembayaran == "uang_sisa"): ?>
									PEMBAYARAN SISA
								<?php else: ?>
									PEMBAYARAN LUNAS
								<?php endif ?>
							</td>
							<td>
								<?php if ($dt_upload_bukti->status_bukti == "menunggu"): ?>
									<span class="label label-warning">Menunggu Verifikasi</span>
								<?php elseif ($dt_upload_bukti->status_bukti == "valid"): ?>
									<span class="label label-success">Cocok</span>
								<?php else: ?>
									<span class="label label-danger">Tidak Valid</span>
								<?php endif ?>
									
							</td>
						</tr>
						<?php endforeach ?>
						
						</tbody>
					</table>
				</div>
			</section>

		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>