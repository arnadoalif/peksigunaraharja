<div class="mobile-menu-left-overlay"></div>
	<nav class="side-menu">
	    <ul class="side-menu-list">
	        <li class="grey">
	            <a href="<?php echo base_url('member/'); ?>"><span class="lbl">Beranda</span></a>
	        </li>
	        <li class="grey">
	            <a href="<?php echo base_url('member/list_order'); ?>"><span class="lbl">Data Pesanan</span></a>
	        </li>
	        <li class="grey">
	            <a href="<?php echo base_url('member/detail_pembayaran'); ?>"><span class="lbl">Pembayaran</span></a>
	        </li>
	        <li class="grey">
	            <a href=" <?php echo base_url('member/history_order'); ?>"><span class="lbl">Riwayat Pesan</span></a>
	        </li>
	        <!-- <li class="grey">
	            <a href=" <?php echo base_url('member/setting'); ?>"><span class="lbl">Pengaturan</span></a>
	        </li> -->
	    </ul>
	</nav><!--.side-menu-->