﻿<!DOCTYPE html>
<html>
<head lang="en">
    <?php require_once(APPPATH .'views/include_front/head_style.php'); ?>
</head>
<body>

<div id="wrapper">
    
    <?php $this->load->view('frontpage/nav_menu_front'); ?>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-4">
                
            </div>
            <div class="col-xs-12 col-sm-8 col-md-4">
                

                <form role="form" action="<?php echo base_url().'/loginmember/action_login'; ?>" method="POST" class="register-form">
                    <h2>Login Member <small></h2>
                   <?php if (isset($_SESSION['error_data'])): ?>
                    <div class="alert alert-warning alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['error_data'] ?>
                    </div>
                    <?php endif ?>
                    <div class="form-group">
                        <input type="text" name="username" required id="username" class="form-control input-md" placeholder="Username" tabindex="3">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" required id="password" class="form-control input-md" placeholder="Password" tabindex="3">
                    </div>
                    
                    <div class="row">
                         <div class="col-md-6 col-lg-6">
                            <div class="form-group">
                                <input type="submit" id="submit" value="Login" class="btn btn-theme btn-block btn-md" tabindex="7">
                            </div>
                         </div>
                         <div class="col-md-6 col-lg-6">
                            <div class="form-group">
                                <a class="btn btn-theme btn-block btn-md" href="<?php echo base_url('page/pelanggan') ?> " role="button">Daftar Pelanggan</a>
                            </div>
                         </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <?php $this->load->view('frontpage/footer_promosi.php'); ?>
</div>
  <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>

    <?php require_once(APPPATH .'views/include_front/head_script.php'); ?>
</body>
</html>