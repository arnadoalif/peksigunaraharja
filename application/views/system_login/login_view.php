<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Login</title>
    <?php require_once(APPPATH .'views/include/head_style.php'); ?>
</head>
<body>

    <div class="page-center">
        <div class="page-center-in">
            <div class="container-fluid">
                <form class="sign-box" method="POST" action="<?php echo base_url().'index.php/login/action_login'; ?> ">
                    <div class="sign-avatar">
                        <img src="<?php echo base_url('assets/img/avatar-sign.png'); ?>" alt="">
                    </div>
                    <header class="sign-title">Masuk Admin</header>
                    <?php if (isset($_SESSION['error_data'])): ?>
                    <div class="alert alert-warning alert-fill alert-border-left alert-close alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['error_data'] ?>
                    </div>
                    <?php endif ?>
                    <div class="form-group">
                        <input type="text" class="form-control" name="username" placeholder="Username" />
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Password"/>
                    </div>
                    <button type="submit" class="btn btn-rounded">Masuk</button>
                </form>
            </div>
        </div>
    </div><!--.page-center-->


<?php require_once(APPPATH .'views/include/head_script.php'); ?>
</body>
</html>