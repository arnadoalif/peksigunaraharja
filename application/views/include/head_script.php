<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<?php //echo base_url('assets/js/lib/jquery/jquery.min.js'); ?> "></script>
<script src="<?php echo base_url('assets/js/lib/tether/tether.min.js'); ?> "></script>
<script src="<?php echo base_url('assets/js/lib/bootstrap/bootstrap.min.js'); ?> "></script>
<script src="<?php echo base_url('assets/js/plugins.js'); ?> "></script>

<script src="<?php echo base_url('assets/js/lib/summernote/summernote.min.js'); ?>"></script>
	<script>
		$(document).ready(function() {
			$('.summernote').summernote({
				height: 300,                 // set editor height
				minHeight: null,             // set minimum height of editor
				maxHeight: null,             // set maximum height of editor
				focus: true
			});
		});
	</script>

<script src="<?php echo base_url('assets/js/lib/datatables-net/datatables.min.js'); ?> "></script>
	<script>
		$(function() {
			$('#data_aktivitas').DataTable(
                {
                    responsive: true,
                }  
                );
		});

        $(function() {
            $('#konfirmasi_pembayaran').DataTable(
                {
                    responsive: true,
                }  
                );
        });
	</script>

    <script type="text/javascript" src="<?php echo base_url('assets/js/lib/match-height/jquery.matchHeight.min.js'); ?> "></script>
    <script>
        $(function() {
            $('.page-center').matchHeight({
                target: $('html')
            });

            $(window).resize(function(){
                setTimeout(function(){
                    $('.page-center').matchHeight({ remove: true });
                    $('.page-center').matchHeight({
                        target: $('html')
                    });
                },100);
            });
        });
    </script>

    <script type="text/javascript" src="<?php echo base_url('assets/js/lib/jqueryui/jquery-ui.min.js'); ?> "></script>
    <!-- <script type="text/javascript" src="<?php echo base_url('assets/js/lib/lobipanel/lobipanel.min.js'); ?> "></script> -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/lib/match-height/jquery.matchHeight.min.js'); ?> "></script>
    
    <script src="<?php echo base_url('assets/js/lib/jquery-tag-editor/jquery.caret.min.js'); ?> "></script>
    <script src="<?php echo base_url('assets/js/lib/jquery-tag-editor/jquery.tag-editor.min.js'); ?> "></script>
    <script src="<?php echo base_url('assets/js/lib/bootstrap-select/bootstrap-select.min.js'); ?> "></script>
    <script src="<?php echo base_url('assets/js/lib/select2/select2.full.min.js'); ?> "></script>

    <script>
        $(function() {
            $('#tags-editor-textarea').tagEditor();
        });
    </script>
    <script src="<?php echo base_url('assets/js/lib/input-mask/jquery.mask.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/lib/input-mask/input-mask-init.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/app.js'); ?> "></script>
<script src="<?php //echo base_url('assets/js/jspdf.js'); ?> "></script>
<script src="<?php //echo base_url('assets/js/pdfFromHTML.js'); ?> "></script>