<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seo extends CI_Controller {

	public function index()
	{
		redirect('/','refresh');
	}
	
	public function web_master() {
		$this->load->view('google2853deb7ec073ff5');
	}

	public function get_data() {
		$data = $this->db->query("SELECT judul_berita FROM tbl_data_berita")->result();
		echo '<pre>';
		
		// print_r(count($data));
		for ($i = 0; $i < count($data) ; $i++) {
			$judul_berita = $data[$i]->judul_berita;
			echo 'Judul Berita : '.$judul_berita."<br>";
			$remove_strip = str_replace("-"," ", $judul_berita);
			echo 'Remove Tanda :'. $remove_strip."<br>";
			$kata_kunci = $this->slug($remove_strip);
			echo 'Kata Kunci :'. $kata_kunci."<br>";
			echo '-------------------------------------------------------<br>';

		}
	}

	public function edit_data_kata_kunci() {
		$this->load->model('Berita_model');
		$m_berita = new Berita_model();


		$data = $this->db->query("SELECT apikey_berita, judul_berita FROM tbl_data_berita")->result();
		echo '<pre>';
		
		// print_r(count($data));
		for ($i = 0; $i < count($data) ; $i++) {
			$apikey = $data[$i]->apikey_berita;
			$judul_berita = $data[$i]->judul_berita;
			echo 'Apikey Berita :'. $apikey."<br>";
			echo 'Judul Berita : '.$judul_berita."<br>";
			$remove_strip = str_replace("-"," ", $judul_berita);
			echo 'Remove Tanda :'. $remove_strip."<br>";
			$kata_kunci = $this->slug($remove_strip);
			echo 'Kata Kunci :'. $kata_kunci."<br>";
			
			$edit_data = array(
				'kata_kunci' => $kata_kunci
			);
			print_r($edit_data);

			$m_berita->update_berita($apikey, $edit_data);

			echo '-------------------------------------------------------<br>';

		}
	}

	public function slug($string, $space="-") {
        $string = utf8_encode($string);
        // if (function_exists('iconv')) {
        //     $string = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
        // }

        $string = preg_replace("/[^a-zA-Z0-9 \-]/", "", $string);
        $string = trim(preg_replace("/\\s+/", " ", $string));
        $string = strtolower($string);
        $string = str_replace(" ", $space, $string);

        return $string;
    }

}

/* End of file Seo.php */
/* Location: ./application/controllers/Seo.php */