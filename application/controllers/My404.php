<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My404 extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('Order_model');
		$m_order = new Order_model();
		$m_order->delete_masa_tenggang();
	}

	public function index() 
    { 
        $this->output->set_status_header('404'); 
        $this->load->view('page_404_view');//loading in my template 
    } 

}

/* End of file My404.php */
/* Location: ./application/controllers/My404.php */