<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect('login');
		}
		
		//Do your magic here
		$this->load->library('user_agent');
		date_default_timezone_set('Asia/Jakarta');
		$this->load->helper("file");
		$this->load->model('Order_model');
		$m_order = new Order_model();
		$m_order->delete_masa_tenggang();

	}

	public function index()
	{
		$this->load->model('Member_model');
		$this->load->model('Berita_model');
		$this->load->model('Pesan_model');
		$this->load->model('Order_model');
		$m_berita = new Berita_model();
		$m_member = new Member_model();
		$m_pesan  = new Pesan_model();
		$m_order  = new Order_model();

		$data['data_aktivitas'] = $m_berita->view_berita()->result();
		$data['total_berita'] = $m_berita->view_berita()->num_rows();
		$data['total_member'] = $m_member->view_member()->num_rows();
		$data['total_pesan']  = $m_pesan->view_pesan()->num_rows();
		$data['total_order']  = $m_order->view_order()->num_rows();


		$this->load->view('administrator/page/admin_home_view', $data);
	}

	public function data_order() {
		$this->load->model('Order_model');
		$this->load->model('Hargajual_model');
		$m_order = new Order_model();
		$m_hargajual = new Hargajual_model();

		$data['status_diproses']   = $m_order->count_status_order_all('tbl_order', 'diproses')->num_rows();
		$data['status_dikirim']    = $m_order->count_status_order_all('tbl_order', 'terkirim')->num_rows();
		$data['status_diterima']   = $m_order->count_status_order_all('tbl_order', 'diterima')->num_rows();
		$data['status_gagal']      = $m_order->count_status_order_all('tbl_order', 'gagal')->num_rows();
		
		$sql = "
		         SELECT tbl_order.kode_order, tbl_order.kode_pelanggan, tbl_order.jumlah_order, tbl_order.status_uang_muka, tbl_order.status_uang_sisa, tbl_order.satuan_doq, tbl_order.uang_muka, tbl_order.tanggal_order, tbl_order.tanggal_kirim, tbl_order.presentase,
		            tbl_order.total_harga, tbl_order.status_order, tbl_member.nomor_telepon, 
                    tbl_member.nama_pelanggan, tbl_member.alamat, tbl_member.kota, tbl_member.email_address FROM tbl_order INNER JOIN
                    tbl_member ON tbl_order.kode_pelanggan = tbl_member.kode_pelanggan WHERE tbl_order.status_order != 'diterima' ORDER BY kode_order DESC
		       ";
		$prsentase = $m_hargajual->view_harga_jual()->result_array();
		$data['data_order'] = $this->db->query($sql)->result();
		if (!empty($prsentase[0]['presentase'])) {
			$data['presentase'] = $prsentase[0]['presentase'];
		} else {
			$data['presentase'] = "No Set";
		}
		
		
		return $this->load->view('administrator/page/admin_data_order_view', $data);
	}


	public function tambah_aktivitas() {
		$this->load->view('administrator/page/admin_tambah_aktivitas_view');
	}

	public function edit_berita($apikey_berita) {
		$this->load->model('Berita_model');
		$m_berita = new Berita_model();

		$valid = $this->db->query("SELECT * FROM tbl_data_berita")->num_rows();
		if ($valid > 0) {
			$data['data_aktivitas'] = $m_berita->get_detail_data($apikey_berita)->result();
			$this->load->view('administrator/page/admin_edit_berita_view', $data);
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Data berita tidak ada.');
			redirect($this->agent->referrer());
		}
	}

	public function tambah_gallery() {
		$this->load->view('administrator/page/admin_tambah_gallery_view');
	}

	public function data_gallery() {
		$this->load->model('Gallery_model');
		$m_gallery = new Gallery_model();
		$data['data_gallery'] = $m_gallery->gallery_view_all()->result();
		$this->load->view('administrator/page/admin_data_gallery_view', $data, FALSE);
	}

	public function edit_data_gallery($kode_gallery) {
		$this->load->model('Gallery_model');
		$m_gallery = new Gallery_model();

		$valid = $this->db->query("SELECT * FROM tbl_gallery WHERE kode_gallery = '$kode_gallery'")->num_rows();
		if ($valid > 0) {
			$data['data_gallery'] = $m_gallery->gallery_view_by_kode_gallery('tbl_gallery', $kode_gallery)->result();
			$this->load->view('administrator/page/admin_edit_gallery_view', $data);
		} else {
			redirect('administrator/data_gallery/','refresh');
		}
	}


	public function data_aktivitas() {
		$this->load->model('Berita_model');
		$m_berita = new Berita_model();

		$data['data_aktivitas'] = $m_berita->view_berita()->result();
		$this->load->view('administrator/page/admin_data_aktivitas_view', $data);
	}

	public function harga_jual() {
		$this->load->model('Hargajual_model');
		$m_hargajual = new Hargajual_model();
		$data['data_harga'] = $m_hargajual->view_harga_jual()->row();
		$this->load->view('administrator/page/admin_harga_jual_view', $data);
	}

	public function setting_transfer() {
		$this->load->model('Rekening_model');
		$m_rekening = new Rekening_model();
		$data['data_rekening'] = $m_rekening->view_data_rekening()->result();
		$data['total_rekening'] = count($data['data_rekening']);
		$this->load->view('administrator/page/admin_setting_rekening_view', $data);
	}

	public function konfirmasi_pembayaran() {
		$this->load->model('Bukti_transfer_model');
		$m_bukti_transfer = new Bukti_transfer_model();
		$data['data_konfirmasi_transaksi'] = $m_bukti_transfer->view_data_bukti_transfer('tbl_upload_bukti_transfer')->result();
		$this->load->view('administrator/page/admin_konfirmasi_pembayaran_view', $data);
	}

	public function detail_konfirmasi($kode_order, $kode_pelanggan, $kode_transaksi, $kode_bukti) {
		
		$sql = "

			SELECT tbl_member.nomor_telepon, tbl_member.nama_pelanggan, tbl_upload_bukti_transfer.nama_file_upload, tbl_upload_bukti_transfer.pembayaran, tbl_upload_bukti_transfer.status_bukti,
			tbl_transaksi_order.jenis_rekening, tbl_transaksi_order.nomor_rekening, tbl_transaksi_order.atas_nama, tbl_transaksi_order.uang_muka, tbl_transaksi_order.status_bayar_uang_muka,
			tbl_transaksi_order.sisa_uang, tbl_transaksi_order.status_bayar_uang_sisa, tbl_transaksi_order.total_harga, tbl_transaksi_order.status_total_harga, tbl_order.presentase, tbl_order.jumlah_order, tbl_order.satuan_doq, tbl_transaksi_order.kode_transaksi, tbl_transaksi_order.kode_order, tbl_transaksi_order.kode_pelanggan, tbl_upload_bukti_transfer.kode_bukti
			FROM  tbl_member INNER JOIN
			tbl_upload_bukti_transfer ON tbl_member.kode_pelanggan = tbl_upload_bukti_transfer.kode_pelanggan INNER JOIN
			tbl_transaksi_order ON tbl_member.kode_pelanggan = tbl_transaksi_order.kode_pelanggan INNER JOIN
			tbl_order ON tbl_member.kode_pelanggan = tbl_order.kode_pelanggan
			WHERE tbl_transaksi_order.kode_order = '$kode_order' and tbl_transaksi_order.kode_pelanggan = '$kode_pelanggan' and tbl_transaksi_order.kode_transaksi = '$kode_transaksi' and tbl_upload_bukti_transfer.kode_bukti = '$kode_bukti'
		 ";

		 $data['detail_konfirmasi'] = $this->db->query($sql)->row();
		 $this->load->view('administrator/page/admin_detail_bukti_transaksi_view', $data);
	}

	public function info_data_doq() {
		$kode_pelanggan = $this->session->userdata('kode_pelanggan');
		$this->load->model('Order_model');
		$m_order = new Order_model();
		$this->load->model('Migratesystem_model');
		$m_migrate_system = new Migratesystem_model();
		$data['total_doq'] = $m_migrate_system->view_total_harga_doq()->result();
		$this->load->view('administrator/page/admin_info_doq_view', $data);
	}


	/**
	
		TODO:
		- system crud data berita
		- tambah_data_aktivitas
		- edit_data_aktivitas
		- delete_data_aktivitas
	
	 */

	public function action_tambah_aktivitas() {
		$this->load->model('Berita_model');
		
		$judul_berita = $this->input->post('judul_berita');
		$remove_strip = str_replace("-"," ", $judul_berita);
		$kata_kunci   = $this->slug($remove_strip);

		$file_error = $_FILES['cover_berita']['error'];

		if ($file_error > 0) {
			$new_file_name = "";
		} else {
			$file_name = $_FILES['cover_berita']['name'];
			$file_size = $_FILES['cover_berita']['size'];
			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = mt_rand(1, 99999).'_cover_berita.'.$file_type; 

			$config['upload_path'] = './assets/upload_image/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$config['file_name'] = $new_file_name;

			$this->load->library('upload', $config);

			if (!is_dir('./assets/upload_image/')) {
				mkdir('./assets/upload_image/', 0777);
				if ( ! $this->upload->do_upload('cover_berita')){
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Berita gagal di tambahakan.');
					redirect($this->agent->referrer());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}

			} else {
				if ( ! $this->upload->do_upload('cover_berita')){
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Berita gagal di tambahakan.');
					redirect($this->agent->referrer());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}

			}
		}

		$data_berita = array(
			    'apikey_berita'   => implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 30), 6)),
				'judul_berita'    => $judul_berita,
				'kata_kunci'	  => $kata_kunci,
				'kategori_berita' => $this->input->post('kategori_berita'),
				'post_by'         => 'admin',
				'isi_berita'      => $this->input->post('isi_berita'),
				'cover_berita'    => $new_file_name,
				'tag_post'		  => $this->input->post('tag'),
				'tanggal_post'    => date("Y-m-d H:i:s"),
				'tanggal_update'  => ''
			);

		$m_berita = new Berita_model();
		$m_berita->tambah_berita($data_berita);
		$this->session->set_flashdata('message_data', '<strong>Success</strong> Berita berhasil di tambahkan.');
		redirect($this->agent->referrer());
	}	

	

	public function action_edit_aktivitas() {
		$this->load->model('Berita_model');
		
		$apikey = $this->input->post('key');
		$judul_berita = $this->input->post('judul_berita');
		$remove_strip = str_replace("-"," ", $judul_berita);
		$kata_kunci = $this->slug($remove_strip);

		$valid_data = $this->db->query("SELECT * FROM tbl_data_berita WHERE apikey_berita = '$apikey'");
		if ($valid_data->num_rows() > 0) {
			
			$data_berita = array(
					'judul_berita'    => $judul_berita,
					'kata_kunci'	  => $kata_kunci,	
					'kategori_berita' => $this->input->post('kategori_berita'),
					'isi_berita'      => $this->input->post('isi_berita'),
					'tag_post'		  => $this->input->post('tag')
				);

			$m_berita = new Berita_model();
			$m_berita->update_berita($apikey, $data_berita);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Berita berhasil di perbaharui.');
			redirect($this->agent->referrer());

		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan data tidak dapat di perbaharui.');
			redirect($this->agent->referrer());
		}
		
	}

	public function action_delete_aktivitas($apikey) {
		$this->load->model('Berita_model');
		$m_model =new Berita_model();
		$valid_data = $this->db->query("SELECT * FROM tbl_data_berita WHERE apikey_berita = '$apikey'");
		if ($valid_data->num_rows() > 0) {

			$cover_berita = $valid_data->row('cover_berita');
			$path = './assets/upload_image/'.$cover_berita;
			is_readable($path);
			unlink($path);
			$m_model->delete_berita($apikey);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Berita berhasil di hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Berita gagal di hapus.');
			redirect($this->agent->referrer());
		}
	}


	public function setting() {
		$this->load->view('administrator/page/admin_setting_page_view');
	}

	public function browsur() {
		$this->load->model('Browsur_model');
		$m_browsur = new Browsur_model();
		$data['data_browsur'] = $m_browsur->get_data_browsur();
		$this->load->view('administrator/page/upload_browsur_view', $data);
	}

	public function action_upload_browsur() {
		$this->load->model('Browsur_model');
		$file = $_FILES['cover_browsur'];

		$status = "0";
		if ($this->input->post('status') == 'Terbaru') {
			$status = "1";
		}

		$file_name = $_FILES['cover_browsur']['name'];
		$file_size = $_FILES['cover_browsur']['size'];

		$config['upload_path'] = './assets/upload_browsur/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$config['file_name'] = $file_name;

		$this->load->library('upload', $config);

		if (!is_dir('./assets/upload_browsur/')) {
			mkdir('./assets/upload_browsur/', 0777);
			if ( ! $this->upload->do_upload('cover_browsur')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Browsur gagal di tambahakan.');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

		} else {
			if ( ! $this->upload->do_upload('cover_browsur')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Browsur gagal di tambahakan.');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

		}

		$data_browsur = array(
				'nama_browsur'   => $this->input->post('judul_browsur'),
				'file_name'    => str_replace(' ', '_', $file_name),
				'tanggal_upload'   => date("Y-m-d H:i:s"),
				'status'  => $status
			);
		
		$m_browsur = new Browsur_model();
		$m_browsur->insert_browsur('tbl_dt_browsur', $data_browsur);
		$this->session->set_flashdata('message_data', '<strong>Success</strong> Browsur berhasil di tambahkan.');
		redirect($this->agent->referrer());
	}

	public function action_delete_browsur($id_browsur) {
		$this->load->model('Browsur_model');
		$m_browsur =new Browsur_model();
		$valid_data = $this->db->query("SELECT * FROM tbl_dt_browsur WHERE id_browsur = '$id_browsur'");
		if ($valid_data->num_rows() > 0) {

			$file_name = $valid_data->row('file_name');
			$path = './assets/upload_browsur/'.$file_name;
			is_readable($path);
			unlink($path);
			$m_browsur->delete_browsur('tbl_dt_browsur',$id_browsur);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Browsur berhasil di hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Browsur gagal di hapus.');
			redirect($this->agent->referrer());
		}
	}

	public function member_peksi() {
		$this->load->model('Member_model');
		$m_member = new Member_model();
		$data['data_member'] = $m_member->get_data_member();
		$this->load->view('administrator/page/admin_member_peksi_view', $data);
	}

	public function pesan_pelanggan() {
		$this->load->model('Pesan_model');
		$m_pesan = new Pesan_model();
		$data['data_pesan'] = $m_pesan->get_data_pesan();
		//echo "<pre>";
		//print_r($data['data_pesan']);
		$this->load->view('administrator/page/admin_pesan_view', $data);
	}

	public function action_insert_harga_jual() {
		$this->load->model('Hargajual_model');
		$m_hargajual = new Hargajual_model();

		$harga_jual = $this->input->post('harga_jual');
		$jumlah     = $this->input->post('jumlah');
		$satuan     = $this->input->post('satuan');
		// $tanggal_periode = $this->input->post('tanggal_periode');

		$data_harga = array(
				'harga_jual' => $harga_jual,
				'jumlah'     => $jumlah,
				'satuan'     => $satuan,
				'status_harga' => 'active',
				'tanggal_periode' => date("Y-m-d"),
				'tanggal_update' => date("Y-m-d")
			); 

		$m_hargajual->insert_harga_jual('tbl_harga_doq',$data_harga);
		$this->session->set_flashdata('message_data', '<strong>Success</strong> Harga jual berhasil di tambahakan.');
		redirect($this->agent->referrer());
	}

	public function action_update_harga() {
		$this->load->model('Hargajual_model');
		$m_hargajual = new Hargajual_model();
		$kode_harga = $this->input->post('kode_harga');
		$harga_jual = $this->input->post('harga_jual');
		$jumlah     = $this->input->post('jumlah');
		$satuan     = $this->input->post('satuan');
		$presentase = $this->input->post('presentase_penjualan');
		// $tanggal_periode = $this->input->post('tanggal_periode');

		$data_harga = array(
				'harga_jual' => $harga_jual,
				'jumlah'     => $jumlah,
				'satuan'     => $satuan,
				'presentase' => $presentase,
				'status_harga' => 'active',
				'tanggal_periode' => date("Y-m-d"),
				'tanggal_update' => date("Y-m-d")
			); 

		$m_hargajual->update_harga_jual('tbl_harga_doq', $data_harga, $kode_harga);
		$this->session->set_flashdata('message_data', '<strong>Success</strong> Harga jual berhasil di ubah.');
		redirect($this->agent->referrer());
	}

	public function action_delete_harga($kode_harga) {
		$this->load->model('Hargajual_model');
		$check_data = $this->db->query("SELECT * FROM tbl_harga_doq WHERE kode_harga =  '$kode_harga'")->num_rows();
		if ($check_data > 0) {
			$m_hargajual = new Hargajual_model();
			$m_hargajual->delete_harga_jual('tbl_harga_doq', $kode_harga);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Data berhasil di hapus.');
			redirect($this->agent->referrer());

		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Data gagal di hapus.');
			redirect($this->agent->referrer());
		}
		
	}

	public function action_insert_slide() {

	}

	public function action_delete_slide($kode_slide) {
		$this->load->model('Slide_model');
		$m_slide = new Slide_model();

		$valid = $this->db->query("SELECT * FROM tbl_slide WHERE kode_slide = '$kode_slide'")->num_rows();

		if ($valid > 0) {
			//$m_slide->delete_slide('tbl_slide',$kode_slide);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Data berhasil di hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Data gagal di hapus.');
			redirect($this->agent->referrer());
		}
	}

	public function action_ubah_status_order() {
		$this->load->model('Order_model');
		$m_order = new Order_model();
		$jumlah_order = 0;

		if (empty($this->input->post('jumlah_order'))) {
			$jumlah_order = $this->input->post('jumlah_order_old');
		} else {
			$jumlah_order = $this->input->post('jumlah_order');
		}

		$kode_pelanggan = $this->input->post('kode_pelanggan');
		$kode_order     = $this->input->post('kode_order');
		$status_order   = $this->input->post('status_order');
		$satuan_doq = $this->input->post('harga_satuan_doq');
		$presentase  = $this->input->post('presentase');
		$total_harga    = str_replace(".", "", $this->input->post('total_harga'));
		$uang_muka      = str_replace(".", "", $this->input->post('uang_muka'));

		$check_data = $m_order->check_data_order('tbl_order', $kode_order, $kode_pelanggan)->num_rows();
		if ($check_data > 0) {
			$data_status_order = array(
					'status_order' => $status_order, 
					'total_harga' => $total_harga,
					'jumlah_order' => $jumlah_order, 
					'satuan_doq' => $satuan_doq, 
					'uang_muka' => $uang_muka,
					'presentase' => $presentase,
					'status_uang_muka' => "Belum Lunas",
					'status_uang_sisa' => "Belum Lunas",
					'tgl_masa_tenggang' => date('Y-m-d',strtotime("+7 day"))
					);
			$m_order->update_data_order('tbl_order', $kode_order, $kode_pelanggan, $data_status_order);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> update data dengan kode'.$kode_order.' berhasil di ubah status menjadi '.$status_order.'.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong>  update data dengan kode'.$kode_order.' gagal di ubah status menjadi '.$status_order.'.');
			redirect($this->agent->referrer());
		}	

	}

	public function delete_data_order($kode_order, $kode_pelanggan) {
		$this->load->model('Order_model');
		$m_order = new Order_model();

		$valid_kode = $this->db->query("SELECT * FROM tbl_order WHERE kode_order = '$kode_order' AND kode_pelanggan = '$kode_pelanggan'")->num_rows();
		
		if ($valid_kode > 0) {
			$m_order->delete_order('tbl_order', $kode_order, $kode_pelanggan);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Order di batalkan.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> terjadi kesalahan saat batal order.');
			redirect($this->agent->referrer());
		}
	}

	public function action_insert_rekening() {
		$this->load->model('Rekening_model');
		$m_rekening = new Rekening_model();

		$jenis_rekening = $this->input->post('jenis_rekening');
		$nomor_rekening = $this->input->post('nomor_rekening');
		$atas_nama      = $this->input->post('atas_nama');

		$data_rekening = array(
				'jenis_rekening' => $jenis_rekening,
				'nomor_rekening' => $nomor_rekening,
				'atas_nama' => $atas_nama,
				'tanggal_update' => date("Y-m-d H:i:s")
			);
		$m_rekening->insert_rekening('tbl_rekening', $data_rekening);
		$this->session->set_flashdata('message_data', '<strong>Success</strong> data rekening berhasil di bambahkan');
		redirect($this->agent->referrer());
	}

	public function action_update_rekening() {
		$this->load->model('Rekening_model');
		$m_rekening = new Rekening_model();

		$kode_rekening  = $this->input->post('kode_rekening');
		$nomor_rekening = $this->input->post('nomor_rekening');
		$atas_nama      = $this->input->post('atas_nama');

		$data_rekening = array(
				'nomor_rekening' => $nomor_rekening,
				'atas_nama' => $atas_nama,
				'tanggal_update' => date("Y-m-d H:i:s")
			);
		$m_rekening->update_rekening('tbl_rekening', $data_rekening, $kode_rekening);
		$this->session->set_flashdata('message_data', '<strong>Success</strong> data rekening berhasil di bambahkan');
		redirect($this->agent->referrer());
	}

	public function action_update_status_konfirmasi_order() {
		$this->load->model('Transaksi_order_model');
		$this->load->model('Order_model');
		$this->load->model('Bukti_transfer_model');

		$m_bukti_transfer = new Bukti_transfer_model();
		$m_transaksi_model = new Transaksi_order_model();
		$m_order  = new Order_model();

		$kode_order 		= $this->input->post('kode_order');
		$kode_pelanggan 	= $this->input->post('kode_pelanggan');
		$kode_transaksi 	= $this->input->post('kode_transaksi');
		$kode_bukti         = $this->input->post('kode_bukti');
		$pembayaran     	= $this->input->post('pembayaran');
		$status_uang_muka 	= $this->input->post('status_uang_muka');
		$status_uang_sisa 	= $this->input->post('status_uang_sisa');
		$status_bukti		= $this->input->post('status_bukti');

		/**
		 *
		 * memjadi status total harga
		 *
		 */
		
		if (($status_uang_muka == "lunas") && ($status_uang_sisa == "lunas")) {
			$status_total_harga = "lunas";
		} else {
			$status_total_harga = "belum_lunas";
		}
		
		/**
		 *
		 * update status tbl_upload_bukti_transfer
		 *
		 */
		$where_status_upload_bukti = array(
				'pembayaran' => $pembayaran,
				'status_bukti' => $status_bukti
		);
		
		/**
		 *
		 * update status tbl_transaksi_order
		 *
		 */
		$where_status_transaksi_order = array(
				'status_bayar_uang_muka' => $status_uang_muka,
				'status_bayar_uang_sisa' => $status_uang_sisa,
				'status_total_harga'     => $status_total_harga
		);
		
		/**
		 *
		 * update status tbl_order
		 *
		 */
		$where_status_order = array(
				'status_uang_muka' => $status_uang_muka,
				'status_uang_sisa' => $status_uang_sisa
			);

		$valid_check_data = $m_transaksi_model->valid_data_update_status('tbl_transaksi_order', $kode_order, $kode_pelanggan, $kode_transaksi)->num_rows();
		if ($valid_check_data > 0) {
			$m_bukti_transfer->update_status_upload_file('tbl_upload_bukti_transfer', $kode_order, $kode_pelanggan, $kode_transaksi, $kode_bukti, $where_status_upload_bukti);
			$m_transaksi_model->update_status_pembayaran('tbl_transaksi_order', $kode_order, $kode_pelanggan, $kode_transaksi, $where_status_transaksi_order);
			$m_order->update_status_pembayaran_order('tbl_order', $kode_order, $kode_pelanggan, $where_status_order);
			redirect('administrator/konfirmasi_pembayaran','refresh');
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> terjadi kesalahan saat pembaharuan konfirmasi / hubungi admin.');
			redirect($this->agent->referrer());
		}
	}

	public function slug($string, $space="-") {
        $string = utf8_encode($string);
        if (function_exists('iconv')) {
            $string = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
        }

        $string = preg_replace("/[^a-zA-Z0-9 \-]/", "", $string);
        $string = trim(preg_replace("/\\s+/", " ", $string));
        $string = strtolower($string);
        $string = str_replace(" ", $space, $string);

        return $string;
    }



    /* gallery input */

   public function action_tambah_gallery() {
			$this->load->model('Gallery_model');
			$m_gallery = new Gallery_model();
			echo '<pre>';
			$nama_event   = $this->input->post('nama_event');
			$remove_strip = str_replace("-"," ", $nama_event);
			$kata_kunci_gallery = $this->slug($remove_strip);
			$kode_gallery = implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 18), 6));

			$deskripsi_gallery = $this->input->post('deskripsi_gallery');

			// upload cover album
			$file_name_cover  	= $_FILES['cover_gallery']['name'];
			$error      		= $_FILES['cover_gallery']['error'];
			$just_name_cover 	= pathinfo($file_name_cover, PATHINFO_FILENAME);
			$file_type 			= pathinfo($file_name_cover, PATHINFO_EXTENSION);
			$new_file_name_cover_gallery = $kata_kunci_gallery.'_'.substr($kode_gallery,0,-14).'_cover_gallery.'.$file_type;

			$config['upload_path'] = './storage_img/cover_gallery/';
			$config['allowed_types'] = 'jpg|png';
			$config['file_name'] = $new_file_name_cover_gallery;
			
			if ($error > 0) {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Gambar tidak ada');
				redirect($this->agent->referrer());
			} else {

				$this->load->library('upload', $config);
				if (!is_dir('./storage_img/cover_gallery/')) {
					mkdir('./storage_img/cover_gallery/', 0777);
					if ( ! $this->upload->do_upload('cover_gallery')){
						$error = array('error' => $this->upload->display_errors());
						$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat menambahkan cover album');
						redirect($this->agent->referrer());
					}
					else{
						$data = array('upload_data' => $this->upload->data());
					}

				} else {
					if ( ! $this->upload->do_upload('cover_gallery')){
						$error = array('error' => $this->upload->display_errors());
						$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat menambahkan cover album');
						redirect($this->agent->referrer());
					}
					else{
						$data = array('upload_data' => $this->upload->data());
					}
				}
			}


			// // upload multiple file album
			$array_name = array();
			$data_array_file = each($_FILES['file_album']['name']);
			
			if ($data_array_file['value'] == "") {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Gambar tidak ada');
				redirect($this->agent->referrer());
			} else {
				for ($i = 0; $i < count($_FILES['file_album']['name']); $i++) {
					
					$_FILES['userfile']['name']     = $_FILES['file_album']['name'][$i];
				    $_FILES['userfile']['type']     = $_FILES['file_album']['type'][$i];
				    $_FILES['userfile']['tmp_name'] = $_FILES['file_album']['tmp_name'][$i];
				    $_FILES['userfile']['error']    = $_FILES['file_album']['error'][$i];
				    $_FILES['userfile']['size']     = $_FILES['file_album']['size'][$i];
				
					$file_name = $_FILES['userfile']['name'];
					$just_name = pathinfo($file_name, PATHINFO_FILENAME);
					$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
					$new_file_name = $just_name.'_'.substr($kode_gallery,0,-14).'_gallery.'.$file_type;

					$array_name[] = $new_file_name;
					
					if (!is_dir('./storage_img/cover_album/')) {
						mkdir('./storage_img/cover_album/', 0777);
						move_uploaded_file($_FILES['userfile']['tmp_name'], './storage_img/cover_album/'. $new_file_name);

					} else {
						move_uploaded_file($_FILES['userfile']['tmp_name'], './storage_img/cover_album/'. $new_file_name);
					}
				}
			}

			// save file name gambar
			for ($index = 0; $index < count($array_name) ; $index++) {
				$data_img_gallery = array(
					'kode_gallery'	=> $kode_gallery,
					'nama_image' 	=> $array_name[$index],
					'tgl_upload' 	=> date("Y-m-d H:i:s")
				);
				$m_gallery->gallery_insert_data_album('tbl_detail_gallery', $data_img_gallery);
			}

			// save data_name gallery
			$data_gallery = array(
						'kode_gallery'	 	=> $kode_gallery,
						'nama_event' 		=> $nama_event,
						'kata_kunci' 		=> $kata_kunci_gallery,
						'deskripsi_gallery' => $deskripsi_gallery,
						'cover_album_name' 	=> $new_file_name_cover_gallery,
						'create_at' 		=> date("Y-m-d H:i:s")
					);

			$m_gallery->gallery_insert_data('tbl_gallery', $data_gallery);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Gallery berhasil ditambahkan.');
			redirect($this->agent->referrer());
	}

	public function action_edit_gallery() {
			$this->load->model("Gallery_model");
			$m_gallery = new Gallery_model();


			$kode_gallery = $this->input->post('kode_gallery');
			$nama_event   = $this->input->post('nama_event');

			$remove_strip 		 = str_replace("-"," ", $nama_event);
			$kata_kunci_gallery  = $this->slug($remove_strip);

			$deskripsi_gallery = $this->input->post('deskripsi_gallery');
			$img_old		   = $this->input->post('img_old');

			$valid = $this->db->query("SELECT * FROM tbl_gallery WHERE kode_gallery = '$kode_gallery'")->num_rows();
			if ($valid > 0) {
				if ($_FILES['cover_gallery']['tmp_name'] == '') {

					$data_gallery = array(
						'nama_event' 		=> $nama_event,
						'kata_kunci' 		=> $kata_kunci_gallery,
						'deskripsi_gallery' => $deskripsi_gallery,
						'cover_album_name' 	=> $img_old
					);

					$m_gallery->gallery_update_data('tbl_gallery', $kode_gallery, $data_gallery);
					$this->session->set_flashdata('message_data', '<strong>Success</strong> Gallery berhasil diupdate.');
					redirect("administrator/view_data_gallery", 'refresh');

				} else {

					$dir_remove_file_old = "./storage_img/cover_gallery/".$img_old;
					if (file_exists($dir_remove_file_old)) {
						unlink($dir_remove_file_old);
					}

					// upload cover album
					$file_name_cover  	= $_FILES['cover_gallery']['name'];
					$error      		= $_FILES['cover_gallery']['error'];
					$just_name_cover 	= pathinfo($file_name_cover, PATHINFO_FILENAME);
					$file_type 			= pathinfo($file_name_cover, PATHINFO_EXTENSION);
					$new_file_name_cover_gallery = $kata_kunci_gallery.'_'.substr($kode_gallery,0,-14).'_cover_gallery.'.$file_type;

					$config['upload_path'] = './storage_img/cover_gallery/';
					$config['allowed_types'] = 'jpg|png';
					$config['file_name'] = $new_file_name_cover_gallery;

					$this->load->library('upload', $config);
					if (!is_dir('./storage_img/cover_gallery/')) {
						mkdir('./storage_img/cover_gallery/', 0777);
						if ( ! $this->upload->do_upload('cover_gallery')){
							$error = array('error' => $this->upload->display_errors());
							$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat menambahkan cover album');
							redirect($this->agent->referrer());
						}
						else{
							$data = array('upload_data' => $this->upload->data());
						}

					} else {
						if ( ! $this->upload->do_upload('cover_gallery')){
							$error = array('error' => $this->upload->display_errors());
							$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat menambahkan cover album');
							redirect($this->agent->referrer());
						}
						else{
							$data = array('upload_data' => $this->upload->data());
						}
					}

					$data_gallery = array(
						'nama_event' 		=> $nama_event,
						'kata_kunci' 		=> $kata_kunci_gallery,
						'deskripsi_gallery' => $deskripsi_gallery,
						'cover_album_name' 	=> $new_file_name_cover_gallery
					);

					$m_gallery->gallery_update_data('tbl_gallery', $kode_gallery, $data_gallery);
					$this->session->set_flashdata('message_data', '<strong>Success</strong> Gallery berhasil diupdate.');
					redirect("administrator/data_gallery", 'refresh');
				}
			} else {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat edit gallery id data tidak di kenali');
				redirect($this->agent->referrer());
		
			}
	}

	public function action_delete_gallery($kode_gallery) {
		$this->load->model('Gallery_model');
		$m_gallery = new Gallery_model();
		$get_file_name_album 			= $m_gallery->gallery_view_by_kode_gallery('tbl_detail_gallery', $kode_gallery)->result();
		$get_file_name	= $this->db->query("SELECT cover_album_name FROM tbl_gallery WHERE kode_gallery = '$kode_gallery'")->row();

		$valid = $this->db->query("SELECT * FROM tbl_gallery WHERE kode_gallery = '$kode_gallery'")->num_rows();
		if ($valid) {
		    $dir_remove_file_old = "./storage_img/cover_gallery/".$get_file_name->cover_album_name;

		if (file_exists($dir_remove_file_old)) {
			unlink($dir_remove_file_old);
		}

		foreach ($get_file_name_album as $dt_get_file_album) {
			$dir_remove_file_album_old = "./storage_img/cover_album/".$dt_get_file_album->nama_image;
			if (file_exists($dir_remove_file_album_old)) {
				unlink($dir_remove_file_album_old);
			}
		}

			$m_gallery->gallery_delete_data('tbl_gallery', $kode_gallery);
			$m_gallery->gallery_delete_data('tbl_detail_gallery', $kode_gallery);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> gallery berhasil hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat hapus gallery');
			redirect($this->agent->referrer());
		}
	}
    

}

/* End of file Administrator.php */
/* Location: ./application/controllers/Administrator.php */