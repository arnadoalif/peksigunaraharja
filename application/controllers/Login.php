<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{

		parent::__construct();
		$this->load->library('user_agent');
		//Do your magic here
		$this->load->model('Order_model');
		$m_order = new Order_model();
		$m_order->delete_masa_tenggang();
	}

	public function index()
	{
		$this->load->view('system_login/login_view');
	}


	public function action_login() {
		$this->load->model('Login_model');
		$m_login = new Login_model();

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if (!empty($username) && !empty($password)) {
			$where = array(
			'username' => $username,
			'password' => sha1($password)
			);
			$valid = $m_login->cek_login('tbl_admin', $where)->num_rows();

			if ($valid > 0) {
				$data = $m_login->cek_login('tbl_admin', $where)->row();
				$data_session = array(
						'apikey_admin' => $data->apikey_admin,
						'nama_admin' => $data->nama_admin,
						'status' =>'login'
					);
				$this->session->set_userdata($data_session);
				redirect(base_url('administrator'),'refresh');
			} else {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Periksa kembali login.');
				redirect($this->agent->referrer());
			}

		} else {
			$this->session->set_flashdata('username', $username);
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Periksa kembali login.');
			redirect($this->agent->referrer());
		}
	}


	public function logout() {
		$this->session->unset_userdata(array('apikey_admin'=> '', 'nama_admin' => '', 'status' => ''));
		session_destroy();
		redirect(base_url());
	}



}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */