<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if($this->session->userdata('status') != "login_member"){
			redirect('loginmember');
		}
		$this->load->library('user_agent');
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Order_model');
		$m_order = new Order_model();
		$m_order->delete_masa_tenggang();
	}

	public function index()
	{	
		$kode_pelanggan = $this->session->userdata('kode_pelanggan');
		$this->load->model('Migratesystem_model');
		$this->load->model('Order_model');
		$m_order = new Order_model();
		$m_migrate_system = new Migratesystem_model();

		$data['total_doq'] = $m_migrate_system->view_total_harga_doq()->result();
		$data['data_order'] = $m_order->list_order($kode_pelanggan)->result();
		$data['status_diproses']   = $m_order->count_status_order('tbl_order', $kode_pelanggan, 'diproses')->num_rows();
		$data['status_dikirim']    = $m_order->count_status_order('tbl_order', $kode_pelanggan, 'terkirim')->num_rows();
		$data['status_diterima']    = $m_order->count_status_order('tbl_order', $kode_pelanggan, 'diterima')->num_rows();
		$data['status_gagal']      = $m_order->count_status_order('tbl_order', $kode_pelanggan, 'gagal')->num_rows();
		$this->load->view('memberpage/page/member_home_view', $data);
	}


	public function order($periode, $kode_tanggal) {
		$kode_pelanggan = $this->session->userdata('kode_pelanggan');
		$this->load->model('Migratesystem_model');
		$m_migrate_system = new Migratesystem_model();

		$data_doq = $m_migrate_system->view_doq_data_order($periode)->result();
		$data = "";
		foreach ($data_doq as $value) {
			if ($kode_tanggal == sha1(str_replace("/", "", $value->tgl))) {
				$data = array(
					'periode' => $value->periode,
					'tgl' => $value->tgl,
					'total' => $value->total
					);
			} 
		}
		if (!empty($data)) {
			$this->load->model('Order_model');
			$m_order = new Order_model();
			$data['data_order'] = $m_order->list_order($kode_pelanggan)->result();
			$this->load->view('memberpage/page/order_barang_view', $data);
		} else {
			redirect('member/','refresh');
		}
	}

	public function list_order() {
		$kode_pelanggan = $this->session->userdata('kode_pelanggan');
		$this->load->model('Order_model');
		$m_order = new Order_model();

		$data['data_order'] = $m_order->list_order($kode_pelanggan)->result();
		$data['status_diproses']   = $m_order->count_status_order('tbl_order', $kode_pelanggan, 'diproses')->num_rows();
		$data['status_dikirim']    = $m_order->count_status_order('tbl_order', $kode_pelanggan, 'terkirim')->num_rows();
		$data['status_diterima']    = $m_order->count_status_order('tbl_order', $kode_pelanggan, 'diterima')->num_rows();
		$data['status_gagal']      = $m_order->count_status_order('tbl_order', $kode_pelanggan, 'gagal')->num_rows();
		$this->load->view('memberpage/page/list_order_view', $data);
	}

	public function history_order() {
		$this->load->model('Order_model');
		$m_order = new Order_model();
		$kode_pelanggan = $this->session->userdata('kode_pelanggan');
		$data['data_order'] = $m_order->view_histori_order($kode_pelanggan)->result();
		
		$this->load->view('memberpage/page/histori_view', $data);
	}

	public function setting() {
		$this->load->model('Member_model');
		$kode_pelanggan = $this->session->userdata('kode_pelanggan');
		$m_member = new Member_model();
		$data['data_member'] = $m_member->get_data_where_kode_member('tbl_member', $kode_pelanggan)->row();
		$this->load->view('memberpage/page/setting_view', $data);
	}


	public function invoice_order($kode_order) {
		$this->load->model('Invoice_model');
		$m_invoice = new Invoice_model();

		$kode_pelanggan = $this->session->userdata('kode_pelanggan');
		$valid_invoice  = $m_invoice->get_data_invoice($kode_order, $kode_pelanggan)->result();

		if (count($valid_invoice) > 0) {
			$data['data_invoice'] = $valid_invoice;
			//$this->load->view('memberpage/page/invoice_order_view_bak', $data);

			$this->load->library('pdf');
			$this->pdf->load_view('welcome_message');
			$this->pdf->Output();

		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> terjadi kesalahan invoice.');
			redirect($this->agent->referrer());
		}
	}

	public function upload_bukti_tranfer($kode_order, $kode_pelanggan) {
		$this->load->model('Transaksi_order_model');
		$this->load->model('Bukti_transfer_model');
		$m_transaksi_order = new Transaksi_order_model();
		$m_bukti_transfer = new Bukti_transfer_model();
 
		$valid = $m_transaksi_order->get_data_tranfer_upload_bukti('tbl_transaksi_order', $kode_order, $kode_pelanggan)->num_rows();
		if ($valid > 0) {
			$sql = "SELECT * FROM tbl_transaksi_order WHERE kode_order = '$kode_order' AND kode_pelanggan = '$kode_pelanggan'";
			$data['data_transfer'] = $this->db->query($sql)->row();
			$data['data_upload_bukti'] = $m_bukti_transfer->view_data_bukti_transfer_by_member('tbl_upload_bukti_transfer', $kode_order, $kode_pelanggan)->result();
			$this->load->view('memberpage/page/member_upload_bukti_tranfer_view', $data);

		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> terjadi kesalahan akses halaman.');
			redirect($this->agent->referrer());
		}
	}

	public function detail_pembayaran() {
		$this->load->model('Transaksi_order_model');
		$kode_pelanggan = $this->session->userdata('kode_pelanggan');
		$m_transaksi_order = new Transaksi_order_model();

		$data['data_detail'] = $m_transaksi_order->view_detail_pembayaran($kode_pelanggan)->result();
		$this->load->view('memberpage/page/member_detail_pembayaran_view' , $data);
	}


	/**
	
		TODO:
		- System action
		- backend system data proses
	
	 */
	
	public function action_order() {
		$kode_pelanggan = $this->session->userdata();
		$kode_pelanggan = $this->session->userdata('kode_pelanggan');
		$nomor_telepon = $this->session->userdata('nomor_telepon');
		$data_member = $this->db->query("SELECT * FROM tbl_member WHERE kode_pelanggan = '$kode_pelanggan'")->row();
		$this->load->model('Order_model');
		$this->load->model('Notif_model');
		$m_order = new Order_model();
		$m_notif = new Notif_model();

		$kode_pelanggan = $this->session->userdata('kode_pelanggan');
		$total_doq      = $this->input->post('total_doq');
		$periode        = $this->input->post('periode');
		$ori_periode    = trim($periode);
		$jumlah_order   = $this->input->post('jumlah_order');
		$tanggal_order  = date("Y-m-d H:i:s");
		$tanggal_kirim  = $this->input->post('tanggal_kirim');
		$status_order   = "menunggu";
		$status_checkout = "";
		$sisa               = $this->input->post('sisa');
		if ($jumlah_order <= $sisa) {
			$check_data = $this->db->query('SELECT * FROM tbl_order')->num_rows();
			if ($check_data > 0) {

				$get_id =  $this->db->order_by('kode_order', 'desc')->get('tbl_order')->result_array();
				$id_key = $get_id[0]['kode_order'];
				print_r($id_key);
				$kode = substr($id_key, 9);
				$kode_order = $m_order->buat_kode_order($kode, '', 5);

				$data_order = array(
					'kode_order' => 'PEKSI'.trim($periode).$kode_order,
					'periode' 		 => $periode,
					'kode_pelanggan' => $kode_pelanggan,
					'jumlah_order' 	 => $jumlah_order,
					'tanggal_order'  => $tanggal_order,
					'tanggal_kirim'  =>$tanggal_kirim,
					'status_order'   => $status_order,
					'status_checkout' => $status_checkout,
					'status_uang_muka' => "",
					'status_uang_sisa' => ""
				);

				$message = "Pemberitahuan Order DOQ secara online periode ".$periode." dengan nama pelanggan ".$data_member->nama_pelanggan." nomor telepon ".$data_member->nomor_telepon." jumlah order ".$jumlah_order." asal pemesan dari ".$data_member->kota." status menunggu confirmasi";
				$subject = "Order DOQ periode ".$periode;

				//$m_notif->send_mail_order('benden.alif@gmail.com', $subject, $message);
				//$m_notif->notif_telegram($message);
				
				$data_id_chat = $m_notif->check_status_notif_tele_on()->result();
				foreach ($data_id_chat as $dt_chat) {
					$m_notif->notif_telegram_all_member($message, $dt_chat->id_chat);
				}

				$m_order->insert_data_order($data_order);
				$this->session->set_flashdata('message_data', '<strong>Success</strong> Order berhasil tunggu konfirmasi oleh admin untuk menghubungi anda.');
				redirect('member/list_order');

			} else {
				$data_order = array(
					'kode_order' => 'PEKSI'.trim($periode).'00001',
					'periode' 		 => $periode,
					'kode_pelanggan' => $kode_pelanggan,
					'jumlah_order' 	 => $jumlah_order,
					'tanggal_order'  => $tanggal_order,
					'tanggal_kirim'  =>$tanggal_kirim,
					'status_order'   => $status_order,
					'status_checkout' => $status_checkout,
					'status_uang_muka' => "",
					'status_uang_sisa' => ""
				);

				$message = "Pemberitahuan Order DOQ secara online periode ".$periode." dengan nama pelanggan ".$data_member->nama_pelanggan." nomor telepon ".$data_member->nomor_telepon." jumlah order ".$jumlah_order." asal pemesan dari ".$data_member->kota." status menunggu confirmasi";
				$subject = "Order DOQ periode ".$periode;

				//$m_notif->send_mail_order('benden.alif@gmail.com', $subject, $message);
				//$m_notif->notif_telegram($message);
				$data_id_chat = $m_notif->check_status_notif_tele_on()->result();
				foreach ($data_id_chat as $dt_chat) {
					$m_notif->notif_telegram_all_member($message, $dt_chat->id_chat);
				}

				$m_order->insert_data_order($data_order);
				$this->session->set_flashdata('message_data', '<strong>Success</strong> Order berhasil tunggu konfirmasi oleh admin untuk menghubungi anda.');
				
				redirect('member/list_order');

			}
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> jumlah order melebihi total pada periode ini.');
			redirect($this->agent->referrer());
		}
		
		
	}

	public function action_update_member() {
		$this->load->model('Member_model');
		$m_member = new Member_model();

		$kode_pelanggan = $this->input->post('kode_pelanggan');
		$nama_pelanggan = $this->input->post('nama_pelanggan');
		$nomor_telepon  = $this->input->post('nomor_telepon');
		$alamat         = $this->input->post('alamat');
		$kota           = $this->input->post('kota');

		$valid = $this->db->query("SELECT * FROM tbl_member WHERE kode_pelanggan = '$kode_pelanggan'")->num_rows();
		if ($valid > 0) {
			$where = array(
					'nama_pelanggan' => $nama_pelanggan,
					'nomor_telepon' => $nomor_telepon,
					'alamat' => $alamat,
					'kota' => $kota
				);
			$m_member->update_member('tbl_member', $kode_pelanggan, $where);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Update data berhasil.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> terjadi kesalahan saat update data.');
			redirect($this->agent->referrer());
		}
	}

	public function batal_order($kode_order, $kode_pelanggan) {
		$this->load->model('Order_model');
		$this->load->model('Transaksi_order_model');

		$m_order = new Order_model();
		$m_transaksi_order = new Transaksi_order_model();

		$valid_kode = $this->db->query("SELECT * FROM tbl_order WHERE kode_order = '$kode_order' AND kode_pelanggan = '$kode_pelanggan'")->num_rows();
		if ($valid_kode > 0) {
			$m_order->delete_order('tbl_order', $kode_order, $kode_pelanggan);
			$m_transaksi_order->delete_transfer_order('tbl_transaksi_order', $kode_order, $kode_pelanggan);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Order di batalkan.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> terjadi kesalahan saat batal order.');
			redirect($this->agent->referrer());
		}

	}

	public function peyment_order($kode_order,$kode_pelanggan) {
		$this->load->model('Order_model');
		$m_order = new Order_model();

		$valid_kode = $this->db->query("SELECT * FROM tbl_order WHERE kode_order = '$kode_order' AND kode_pelanggan = '$kode_pelanggan'")->num_rows();
		if ($valid_kode > 0) {
			$sql_order = "

			SELECT tbl_order.kode_order, tbl_order.kode_pelanggan, tbl_order.periode, tbl_order.jumlah_order, tbl_order.tanggal_order, tbl_order.tanggal_kirim, tbl_order.satuan_doq, tbl_order.total_harga, tbl_order.uang_muka, tbl_order.presentase,
                         tbl_order.status_order, tbl_member.nama_pelanggan, tbl_member.nomor_telepon, tbl_member.email_address, tbl_member.alamat, tbl_member.kota, tbl_order.status_uang_muka, tbl_order.status_uang_sisa
				FROM  tbl_order INNER JOIN
                      tbl_member ON tbl_order.kode_pelanggan = tbl_member.kode_pelanggan where tbl_member.kode_pelanggan = '$kode_pelanggan' and tbl_order.kode_order = '$kode_order'
			 ";

			$data['data_order'] = $this->db->query($sql_order)->result();
			$this->load->view('memberpage/page/member_peyment_order_view', $data);
		} else {	
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> terjadi kesalahan saat batal order.');
			redirect($this->agent->referrer());
		}
	}

	public function action_insert_transaksi_order() {
		$this->load->model('Rekening_model');
		$this->load->model('Transaksi_order_model');
		$this->load->model('Order_model');

		$m_rekening = new Rekening_model();
		$m_transaksi_order = new Transaksi_order_model();
		$m_order = new Order_model();

		$data_rekening = $m_rekening->get_name_rekening('tbl_rekening', $this->input->post('jenis_rekening'))->result();
		
		$kode_pelanggan = $this->input->post('kode_pelanggan');
		$kode_order     = $this->input->post('kode_order');
		$jenis_rekening = $this->input->post('jenis_rekening');
		$nomor_rekening = $data_rekening[0]->nomor_rekening;
		$atas_nama      = $data_rekening[0]->atas_nama;
		$uang_muka      = $this->input->post('uang_muka');
		$sisa_uang      = $this->input->post('sisa_uang');
		$total_harga    = $this->input->post('total_harga');

		$check_data = $this->db->query('SELECT * FROM tbl_transaksi_order')->num_rows();
		
		if ($check_data > 0) {

			$get_id =  $this->db->order_by('kode_transaksi', 'desc')->get('tbl_transaksi_order')->result_array();
			$id_key = $get_id[0]['kode_transaksi'];
			$kode = substr($id_key, 5);
			$kode_transaksi = 'OTDOQ'.$m_transaksi_order->buat_kode_transaksi($kode, '', 5);

			$data_transaksi = array(
				'kode_transaksi' => $kode_transaksi,
				'kode_order' => $kode_order,
				'kode_pelanggan' => $kode_pelanggan,
				'jenis_rekening' => $jenis_rekening,
				'nomor_rekening' => $nomor_rekening,
				'atas_nama'      => $atas_nama,
				'uang_muka'      => $uang_muka,
				'status_bayar_uang_muka' => 'belum_bayar',
				'sisa_uang'      => $sisa_uang,
				'status_bayar_uang_sisa' => 'belum_bayar',
				'total_harga'    => $total_harga,
				'status_total_harga' => 'belum_bayar',
				'tanggal_checkout' => date("Y-m-d H:i:s")
			);
			$m_transaksi_order->insert_transaksi_order('tbl_transaksi_order', $data_transaksi);
			$where_data = array('status_checkout'=> 'active');
			$m_order->update_status_checkout('tbl_order', $kode_order, $kode_pelanggan, $where_data);
			redirect('member/download_invoice/'.$kode_transaksi.'/'.$kode_pelanggan.'/'.$kode_order.'','refresh');
		}  else{
			$data_transaksi = array(
				'kode_transaksi' => 'OTDOQ00001',
				'kode_order' => $kode_order,
				'kode_pelanggan' => $kode_pelanggan,
				'jenis_rekening' => $jenis_rekening,
				'nomor_rekening' => $nomor_rekening,
				'atas_nama'      => $atas_nama,
				'uang_muka'      => $uang_muka,
				'status_bayar_uang_muka' => 'belum_bayar',
				'sisa_uang'      => $sisa_uang,
				'status_bayar_uang_sisa' => 'belum_bayar',
				'total_harga'    => $total_harga,
				'status_total_harga' => 'belum_bayar',
				'tanggal_checkout' => date("Y-m-d H:i:s")
			);
			$m_transaksi_order->insert_transaksi_order('tbl_transaksi_order', $data_transaksi);
			$kode_transaksi = "OTDOQ00001";
			$where_data = array('status_checkout'=> 'active');
			$m_order->update_status_checkout('tbl_order', $kode_order, $kode_pelanggan, $where_data);
			redirect('member/download_invoice/'.$kode_transaksi.'/'.$kode_pelanggan.'/'.$kode_order.'','refresh');
		}
	}

	public function download_invoice($kode_transaksi, $kode_pelanggan, $kode_order) {
		$this->load->model('Order_model');
		$m_order = new Order_model();

		$valid_kode = $this->db->query("SELECT * FROM tbl_transaksi_order WHERE kode_order = '$kode_order' AND kode_pelanggan = '$kode_pelanggan' AND kode_transaksi = '$kode_transaksi'")->num_rows();
			if ($valid_kode > 0) {
				$sql_order = "

				SELECT tbl_order.kode_order, tbl_order.kode_pelanggan, tbl_order.periode, tbl_order.jumlah_order, tbl_order.tanggal_order, tbl_order.tanggal_kirim, tbl_order.satuan_doq, tbl_order.total_harga, tbl_order.presentase,
				tbl_order.uang_muka, tbl_order.status_order, tbl_member.kota, tbl_member.alamat, tbl_member.nama_pelanggan, tbl_member.nomor_telepon, tbl_transaksi_order.jenis_rekening,
				tbl_transaksi_order.nomor_rekening, tbl_transaksi_order.atas_nama
				FROM  tbl_order INNER JOIN
				tbl_member ON tbl_order.kode_pelanggan = tbl_member.kode_pelanggan INNER JOIN
				tbl_transaksi_order ON tbl_order.kode_order = tbl_transaksi_order.kode_order where tbl_member.kode_pelanggan = '$kode_pelanggan' and tbl_order.kode_order = '$kode_order' and tbl_transaksi_order.kode_transaksi = '$kode_transaksi'
				 ";

				$data['data_order'] = $this->db->query($sql_order)->result();
				//$this->load->view('memberpage/page/invoice_member_view', $data);
				$this->load->library('pdf');
				$this->pdf->AddPage('L');
				$this->pdf->SetDisplayMode('fullpage');
				$this->pdf->KeepColumns = true;
				$this->pdf->load_view('memberpage/page/invoice_member_view', $data);
				$this->pdf->Output('invoice_order_doq.pdf', 'D');

				redirect('member/list_order');
			} else {	
				redirect('member/list_order');
			}
	}

	public function action_upload_bukti() {
		$this->load->model('Bukti_transfer_model');
		$m_bukti_transfer = new Bukti_transfer_model();

		$kode_order 	= $this->input->post('kode_order');
		$kode_pelanggan = $this->input->post('kode_pelanggan');
		$kode_transaksi = $this->input->post('kode_transaksi');
		$pembayaran 	= $this->input->post('pembayaran');

		$file_name = $_FILES['bukti_transaksi']['name'];
		$file_size = $_FILES['bukti_transaksi']['size'];
		$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
		$new_file_name = mt_rand(1, 99999).'_'.$kode_order.'_bukti_transfer.'.$file_type; 
		$config['upload_path'] = './assets_pembayaran/upload_transfer/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$config['file_name'] = $new_file_name;

		$this->load->library('upload', $config);

		$file_error = $_FILES['bukti_transaksi']['error'];

		if ($file_error > 0) {
			$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Bukti Transaksi Tidak Tersedia.');
				redirect($this->agent->referrer());
		} else {

			if (!is_dir('./assets_pembayaran/upload_transfer/')) {
			mkdir('./assets_pembayaran/upload_transfer/', 0777);
			if ( ! $this->upload->do_upload('bukti_transaksi')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat upload bukti check jenis file dan ukuran file.');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

		} else {
			if ( ! $this->upload->do_upload('bukti_transaksi')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Terjadi kesalahan saat upload bukti check jenis file dan ukuran file.');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

		}

		$data_upload_bukti = array(
				'kode_transaksi' => $kode_transaksi,
				'kode_order' => $kode_order,
				'kode_pelanggan' => $kode_pelanggan,
				'nama_file_upload' => $new_file_name,
				'pembayaran' => $pembayaran,
				'status_bukti' => "menunggu"
			);
		
		$m_bukti_transfer->insert_data_bukti($data_upload_bukti);
		$this->session->set_flashdata('message_data', '<strong>Success</strong> Bukti trasfer berhasil di upload membutuhkan waktu 24 jam untuk verifikasi bukti.');
		redirect($this->agent->referrer());


		}
	}

}

/* End of file Member.php */
/* Location: ./application/controllers/Member.php */