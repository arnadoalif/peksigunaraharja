<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Notif_model');
		// $m_notif = new Notif_model();
		// $m_notif->check_id_chat_telegram();
		
		$this->load->model('Order_model');
		$m_order = new Order_model();
		$m_order->delete_masa_tenggang();
		
	}

	public function index()
	{
		$this->load->model('Totaldoq_model');
		$this->load->model('Hargajual_model');
		$this->load->model('Migratesystem_model');
		$this->load->model('Berita_model');

		$m_hargajual = new Hargajual_model();
		$m_total_doq = new Totaldoq_model();
		$m_migrate_system = new Migratesystem_model();

		$m_berita = new Berita_model();
		$data['data_aktivitas'] = $m_berita->view_berita()->result();
		$data['total_data'] = $m_berita->view_berita()->num_rows();

		$data['total_doq'] = $m_migrate_system->view_total_harga_doq()->result();
		$data['total_penjualan'] = $m_migrate_system->view_total_penjualan()->result();
		$data['data_harga'] = $m_hargajual->view_harga_jual()->row();
		
		$this->load->view('frontpage/page/beranda_view', $data);
	}

	public function json_periode() {
		$this->load->model('Migratesystem_model');
		$m_migrate_system = new Migratesystem_model();
		// $data['test_periode_view'] = $m_migrate_system->query_new_mas_rian()->result();
		$data['test_periode_view'] = $m_migrate_system->query_new_mas_rian_tot_penjualan()->result();

		$json_periode = array();
		$data_json = json_encode($data['test_periode_view']);
		$data = json_decode($data_json);
		foreach ($data as $key => $dt_json) {
			$json_periode[$key] = array(
							'backgroundColor' => $dt_json->backgroundColor,
							'title' => number_format($dt_json->title, 0, ".","."). " Pembeli",
							'start' => $dt_json->start
						);
		}
		$data = json_encode($json_periode);
		print_r($data);
	
		return $data;
	}

	public function lowongan_kerja() {
		$this->load->view('frontpage/page/lowongan_kerja_view');
	}

	/**
	
		TODO:
		- bagain menu tentang
		- * profil
		- * visi & misi
		- * penghargaan
		- * kontak
		- * produk
	
	 */
	

	public function visi_misi() {
		$this->load->view('frontpage/page/visi_misi_view');
	}

	public function kontak() {
		$this->load->view('frontpage/page/kontak_view');
	}

	public function register() {
		$this->load->view('frontpage/page/register_view');
	}

	public function tentang_kami() {
		//$this->load->view('frontpage/page/tentang_kami_view');
		$this->load->view('frontpage/page/check_video_embeded_view');
	}

	public function pelanggan() {
		$this->load->view('frontpage/page/input_data_pelanggan_view');
	}

	public function galeri() {
		$this->load->model('Gallery_model');
		$m_gallery = new Gallery_model();
		$data['data_gallery'] = $m_gallery->gallery_view_all()->result();
		$this->load->view('frontpage/page/galeri_view', $data);
	}

	public function detail_galeri($kata_kunci_gallery) {
		$this->load->model('Gallery_model');
		$m_gallery = new Gallery_model();

		$valid = $this->db->query("SELECT * FROM tbl_gallery WHERE kata_kunci = '$kata_kunci_gallery'")->num_rows();
		if ($valid > 0) {
			$data['data_detail_gallery'] = $m_gallery->gallery_view_by_kata_kunci('tbl_gallery', $kata_kunci_gallery)->result();
			$this->load->view('frontpage/page/detail_galeri_view', $data);
		} else {
			redirect('','refresh');
		}
		
	}

	public function browsur_peksi() {
		$this->load->model('Browsur_model');
		$m_browsur = new Browsur_model();
		$data['data_browsur'] = $m_browsur->get_data_browsur();
		$this->load->view('frontpage/page/download_browsur_view', $data);
	}

	public function tentang_kami_local() {
		$this->load->view('frontpage/page/check_video_local_view');
	}

	public function tentang_kami_youtube() {
		$this->load->view('frontpage/page/check_video_embeded_view');
	}

	/**
	
		TODO:
		- proses system

	
	 */
	
	public function artikel() {
		$this->load->model('Berita_model');
		$m_berita = new Berita_model();

		$jumlah_artikel = $m_berita->jumlah_data('tbl_data_berita');
		// $this->load->library('pagination');
		
		// $config['base_url'] = base_url().'page/artikel/';
		// $config['total_rows'] =$jumlah_artikel;
		// $config['per_page'] = 8;

		// $this->load->library('pagination');
		
		// $config['num_links'] = $jumlah_artikel;
		//  $config['use_page_numbers'] = TRUE;
		//  $config['full_tag_open'] = '<ul class="pagination">';
		//  $config['full_tag_close'] = '</ul>';
		//  $config['prev_link'] = '&laquo;';
		//  $config['prev_tag_open'] = '<li>';
		//  $config['prev_tag_close'] = '</li>';
		//  $config['next_tag_open'] = '<li>';
		//  $config['next_tag_close'] = '</li>';
		//  $config['cur_tag_open'] = '<li class="active"><a href="#">';
		//  $config['cur_tag_close'] = '</a></li>';
		//  $config['num_tag_open'] = '<li>';
		//  $config['num_tag_close'] = '</li>';
		//  $config['next_link'] = '&raquo;';

		
		// $this->pagination->initialize($config);

		// $from = $this->uri->segment(3);
		// $this->pagination->initialize($config);		
		// $data['data_aktivitas'] = $m_berita->data($config['per_page'],$from);
		$data['data_aktivitas'] = $m_berita->view_berita()->result();
		$data['total_data'] = $m_berita->view_berita()->num_rows();

		$this->load->view('frontpage/page/artikel_view',$data);
	}

	public function detail_artikel($kata_kunci) {
		$this->load->model('Berita_model');
		$m_berita = new Berita_model();
		$check = $m_berita->get_detail_data_kata_kunci($kata_kunci)->num_rows();
		if ($check > 0) {
			$data['data_artikel'] = $m_berita->get_detail_data_kata_kunci($kata_kunci)->row();
			$data['data_aktivitas'] = $m_berita->view_limit_data(15);
			$data['judul_berita'] = $m_berita->get_detail_data_kata_kunci($kata_kunci)->row('judul_berita');
			$data['tag_post'] = $m_berita->get_detail_data_kata_kunci($kata_kunci)->row('tag_post');
			$data['count_artikel'] = $m_berita->view_berita()->result();
			$this->load->view('frontpage/page/detail_berita_view', $data);
		} else {
			redirect('artikel');
		}
	}

	public function action_kontak_pelanggan() {
		$this->load->model('Pesan_model');

		$nama_penulis  = $this->input->post('nama_penulis');
		$email_penulis = $this->input->post('email_penulis');
		$nomor_telepon = $this->input->post('nomor_telepon');
		$alamat_lengkap = $this->input->post('alamat_lengkap');
		$isi_pesan     = $this->input->post('isi_pesan');

		$data_pesan = array(
				'email_penulis' => $email_penulis,
				'nama_penulis'  => $nama_penulis,
				'nomor_telepon' => $nomor_telepon,
				'alamat_lengkap' => $alamat_lengkap,
				'isi_pesan'     => $isi_pesan,
				'status_pesan'  => '3',
				'tanggal_pesan' => date("Y-m-d H:i:s")
			);

		$m_pesan = new Pesan_model();
		$m_pesan->insert_pesan('tbl_pesan', $data_pesan);
		$this->session->set_flashdata('sendmessage', '<strong>Pesan Anda telah dikirim</strong> Terimakasih.');
		redirect($this->agent->referrer());
	}

	public function download($filename = NULL) {
	    $this->load->helper('download');
	    $data = file_get_contents(base_url('assets/upload_browsur/'.$filename));
	    force_download($filename, $data);
	}

	public function action_register_pelanggan() {
		$this->load->model('Member_model');
		$m_member = new Member_model();

		$nama_pelanggan  = $this->input->post('nama_pelanggan');
		$username        = $this->input->post('username');
		$nomor_telepon   = $this->input->post('nomor_telepon');
		$email_address   = $this->input->post('email_address');
		$password        = $this->input->post('password');
		$alamat          = $this->input->post('alamat');
		$kota            = $this->input->post('kota');
		$wilayah         = $this->input->post('wilayah');
		$koordinat       = $this->input->post('koordinat');

		$check_row = $this->db->query('SELECT * FROM tbl_member')->num_rows();

		if ($check_row > 0) {
			$where = array(
				'nomor_telepon' => $nomor_telepon,
				'email_address' => $email_address
			);
			$check_data = $m_member->check_vallid_data('tbl_member', $where)->num_rows();

			if ($check_data > 0) {
				$this->session->set_flashdata('validmessage', '<strong>Maff data sudah pelanggan sudah tersedia. </strong>');
				redirect($this->agent->referrer());
				
			} else {
				$get_id =  $this->db->order_by('kode_pelanggan', 'desc')->get('tbl_member')->result();
				$id_key = $get_id[0]->kode_pelanggan;
				$kode_pelanggan = $m_member->buatkode($id_key, 'PLG', 5);

				$data_pelanggan_baru = array(
					'kode_pelanggan' => $kode_pelanggan,
					'nama_pelanggan' => $nama_pelanggan,
					'username'       => $username,
					'email_address'  => $email_address,
					'nomor_telepon'  => $nomor_telepon,
					'password'       => sha1($password),
					'confirm_password' => $password,
					'alamat'         => $alamat,
					'kota'           => $kota,
					'koordinat'      => $koordinat,
					'wilayah_daftar' => $wilayah,
					'tanggal_daftar' => date("Y-m-d H:i:s")
				);

				$m_member->insert_member('tbl_member',$data_pelanggan_baru);
				$data_session = array(
						'kode_pelanggan' => $kode_pelanggan,
						'nomor_telepon'  => $nomor_telepon,
						'status' =>'login_member'
					);

				$this->session->set_userdata($data_session);
				redirect(base_url('member'),'refresh');
				}

		} else {
			$data_pelanggan_baru = array(
				'kode_pelanggan' => 'PLG00001',
				'nama_pelanggan' => $nama_pelanggan,
				'username'       => $username,
				'email_address'  => $email_address,
				'nomor_telepon'  => $nomor_telepon,
				'password'       => sha1($password),
				'confirm_password' => $password,
				'alamat'         => $alamat,
				'kota'           => $kota,
				'koordinat'      => $koordinat,
				'wilayah_daftar' => $wilayah,
				'tanggal_daftar' => date("Y-m-d H:i:s")
			);
			
			$m_member->insert_member('tbl_member',$data_pelanggan_baru);
			$kode_pelanggan = "PLG00001";
			$data_session = array(
						'kode_pelanggan' => $kode_pelanggan,
						'nomor_telepon'  => $nomor_telepon,
						'status' =>'login_member'
					);

			$this->session->set_userdata($data_session);
			redirect(base_url('member'),'refresh');
		}

	}


}

/* End of file Frontpage.php */
/* Location: ./application/controllers/Frontpage.php */
