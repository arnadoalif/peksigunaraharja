<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loginmember extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		//Do your magic here
		$this->load->model('Order_model');
		$m_order = new Order_model();
		$m_order->delete_masa_tenggang();
	}

	public function index()
	{
		$this->load->view('system_login/login_member_view');
	}

	public function action_login() {
		$this->load->model('Login_model');
		$m_login = new Login_model();

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		

		if (!empty($username) && !empty($password)) {
			$where = array(
			'username' => $username,
			'password' => sha1($password)
			);
			$valid = $m_login->cek_login('tbl_member', $where)->num_rows();

			if ($valid > 0) {
				$data = $m_login->cek_login('tbl_member', $where)->row();
				$data_session = array(
						'kode_pelanggan' => $data->kode_pelanggan,
						'nomor_telepon'  => $data->nomor_telepon,
						'status' =>'login_member'
					);

				$this->session->set_userdata($data_session);
				redirect(base_url('member'),'refresh');
			} else {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Periksa kembali login.');
				redirect($this->agent->referrer());
			}

		} else {
			$this->session->set_flashdata('username', $username);
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Periksa kembali login.');
			redirect($this->agent->referrer());
		}
	}

	public function logout() {
		$this->session->unset_userdata(array('kode_pelanggan'=> '', 'nomor_telepon' => '', 'status' => ''));
		session_destroy();
		redirect(base_url());
	}

}

/* End of file Loginmember.php */
/* Location: ./application/controllers/Loginmember.php */