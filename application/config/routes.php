<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'page';
$route['home'] = 'page';

$route['local'] = 'page/tentang_kami_local';
$route['youtube'] = 'page/tentang_kami_youtube';

$route['2014/03/cara-pemasaran-telur-puyuh.html'] = 'seo';
$route['2016/10/dijual-bibit-burung-puyuh.html'] = 'seo';
$route['2016/08/bibit-puyuh-siap-kirim-ke.html'] = 'seo';
$route['2016/10/membuat-perkedel-ayam-telor-puyuh.html'] = 'seo';
$route['2017/06/jaminan-yang-kami-berikan-ke-setiap.html'] = 'seo';
$route['2016/12/manfaat-cahaya-untuk-unggas.html'] = 'seo';
$route['2017/04/pondasi-pembesaran-bibit-puyuh.html'] = 'seo';
$route['2016/10/dijual-bibit-burung-puyuh.html'] = 'seo';
$route['2016/08/bibit-puyuh-siap-kirim-ke.html'] = 'seo';
$route['2016/10/membuat-perkedel-ayam-telor-puyuh.html'] = 'seo';
$route['2017/06/jaminan-yang-kami-berikan-ke-setiap.html'] = 'seo';
$route['2017/04/pondasi-pembesaran-bibit-puyuh.html'] = 'seo';
$route['2016/04/jual-bibit-puyuh-dikalimantan.html'] = 'seo';

$route['google2853deb7ec073ff5.html'] = 'seo/web_master';

$route['brosur'] = 'page/browsur_peksi';
$route['artikel'] = 'page/artikel';
$route['artikel/(:any)']  = 'page/detail_artikel/$1';
$route['galeri'] = 'page/galeri';
$route['galeri/(:any)'] = 'page/detail_galeri/$1';
$route['kontak'] = 'page/kontak';
$route['tentang'] = 'page/tentang_kami';
$route['lowongan'] = 'page/lowongan_kerja';

$route['404_override'] = 'seo';
//$route['404_override'] = 'my404';
$route['superoot/access'] = 'administrator';
$route['translate_uri_dashes'] = FALSE;
