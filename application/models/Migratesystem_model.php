<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migratesystem_model extends CI_Model {

	public function view_total_harga_doq() {
		$tgl = date('d/m/Y');
		$sql = "
				declare @tgl1 varchar(10)	   declare @tgl2 varchar(10)
				select @tgl1='$tgl', @tgl2= convert(varchar(10), dateadd(day, 30, convert(date, @tgl1, 103)), 103)

				select  periode, substring(periode,3,2) minggu, sum(JumlahPlot) jumlah
				from LINK_POP.hatce18.dbo.hacplot1 
				where kode_zona>='900' and kode_zona<>'tot'  and jenis='1' and 
					   convert(date, tanggal, 103)>convert(date, @tgl1, 103) and convert(date, tanggal, 103)<=convert(date, @tgl2, 103)
				group by periode
				 ";
		return $this->db->query($sql);
	}

	public function view_total_penjualan() {
		$tgl = date('d/m/Y');
		$sql1 = " 
			declare @tgl1 varchar(10)	   declare @tgl2 varchar(10)
			select @tgl1='$tgl', @tgl2= convert(varchar(10), dateadd(day, -30, convert(date, @tgl1, 103)), 103)

			select  'Minggu ke- '+substring(periode,3,2) tanggal, periode, sum(jumkirim) kirim
			from LINK_POP.hatce18.dbo.HACDOQ2 a inner join LINK_POP.hatce18.dbo.refzona b on a.kode_zona=b.kode_zona
			where convert(date, tglDeliver, 103)>=convert(date, @tgl2, 103) and convert(date, tglDeliver, 103)<=convert(date, @tgl1, 103)
			group by periode
		 ";
		 return $this->db->query($sql1);
	}

	public function detail_total_kirim($periode) {
		$sql = "
			select  zona zona, periode, sum(jumkirim) kirim
			from LINK_POP.hatce18.dbo.HACDOQ2 a inner join LINK_POP.hatce17.dbo.refzona b on a.kode_zona=b.kode_zona
			where periode='$periode'
			group by a.kode_zona, zona, periode
		";
		return $this->db->query($sql);
	}

	public function detail_jumlah_doq_per_periode($periode) {
		$tgl = date('d/m/Y');
		$sql = "
			select 'Tanggal - '+Tanggal tgl, sum(JumlahPlot) total
			from LINK_POP.hatce18.dbo.hacplot1
			where kode_zona>='900' and kode_zona <>'tot' and jenis='1' and periode='$periode' and Tanggal > '$tgl'  group by tanggal
		";
		return $this->db->query($sql);
	}

	public function view_doq_data_order($periode) {
		$tanggal_sekarang = date('d/m/Y');
		$sql = "
		select periode, Tanggal tgl, sum(JumlahPlot) total
		from LINK_POP.hatce18.dbo.hacplot1
		where kode_zona>='900' and kode_zona <>'tot' and jenis='1' and periode='$periode' and Tanggal > '$tanggal_sekarang'  group by tanggal, periode
		";
		return $this->db->query($sql);
	}

	public function detail_doq_order_member($periode) {
		$tanggal_sekarang = date('d/m/Y');
		$sql = "  
		select periode, Tanggal tgl, sum(JumlahPlot) total
		from LINK_POP.hatce18.dbo.hacplot1
		where kode_zona>='900' and kode_zona <>'tot' and jenis='1' and periode='$periode' and Tanggal > '$tanggal_sekarang'  group by tanggal, periode
		";
		return $this->db->query($sql);
	}

	public function query_new_mas_rian() {
		$tgl_sekarang = date('d/m/Y');
		$sql = "

			select	 Rendering, backgroundColor, start, convert(varchar, title)+convert(varchar, '') title
		    from	 (
			   select	'background' rendering ,'#fe5959' backgroundColor, convert(date, convert(varchar, tgldeliver, 108), 103) start, sum(jumkirim) title
			   from LINK_POP.hatce18.dbo.hacdoq2 
			   where kode_prod='DOQ001' and 
					 convert(date, tgldeliver, 103) < convert(date, '$tgl_sekarang', 103) and
					 convert(date, tgldeliver, 103) >= dateadd(day, -60, convert(date, '$tgl_sekarang', 103))
			   group by tgldeliver
			   union all
			   select  'background' rendering, '#5959fe' backgroundColor, convert(date, convert(varchar, tanggal, 108), 103) start, sum(jumlahplot) title 
			   from LINK_POP.hatce18.dbo.hacplot1	
			   where	kode_zona>='900' and kode_zona <>'tot' and jenis='1' and jumlahplot<>'0' and 
					 convert(date, tanggal, 103) >= convert(date, '$tgl_sekarang', 103)
			   group by tanggal
		    ) a	
		 ";
		 return $this->db->query($sql);
	}

	public function query_new_mas_rian_tot_penjualan() {
		$tgl_sekarang = date('d/m/Y');
		$sql = "
			select	 Rendering, backgroundColor, start, convert(varchar, title)+convert(varchar, '') title
		    from	 (
			   select	'background' rendering ,'#fe5959' backgroundColor, convert(date, convert(varchar, tgldeliver, 108), 103) start, count(*) title
			   from LINK_POP.hatce18.dbo.hacdoq2 
			   where kode_prod='DOQ001' and 
					 convert(date, tgldeliver, 103) < convert(date, '$tgl_sekarang', 103) and
					 convert(date, tgldeliver, 103) >= dateadd(day, -60, convert(date, '$tgl_sekarang', 103))
			   group by tgldeliver
			   union all
			   select  'background' rendering, '#5959fe' backgroundColor, convert(date, convert(varchar, tanggal, 108), 103) start, count(*) title 
			   from LINK_POP.hatce18.dbo.hacplot1	
			   where	kode_zona>='900' and kode_zona <>'tot' and jenis='1' and jumlahplot<>'0' and 
					 convert(date, tanggal, 103) >= convert(date, '$tgl_sekarang', 103)
			   group by tanggal
		    ) a	
		 ";
		 return $this->db->query($sql);
	}


}

/* End of file Migratesystem_model.php */
/* Location: ./application/models/Migratesystem_model.php */