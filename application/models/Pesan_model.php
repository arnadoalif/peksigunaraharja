<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesan_model extends CI_Model {

	public function get_data_pesan() {
		$this->db->order_by('id_pesan', 'desc');
		$data = $this->db->get('tbl_pesan');
		return $data->result();
	}

	public function view_pesan() {
		return $this->db->get('tbl_pesan');
	}

	public function insert_pesan($table, $data) {
		$this->db->insert($table, $data);
	}

	public function delete_pesan($table, $id_data) {
		$this->db->where('id_pesan', $id_data);
		$this->db->delete($table);
	}

}

/* End of file Pesan_mode.php */
/* Location: ./application/models/Pesan_mode.php */