<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Totaldoq_model extends CI_Model {

	public function view_total_doq() {
		$sql = "
				declare @tgl1 varchar(10)   declare @tgl2 varchar(10)	 declare @tgl3 varchar(10)
							select @tgl1='01/08/2017', @tgl2='01/10/2017', @tgl3='04/09/2017'
							--@tgl1 filter tanggal batas awal pencarian
							--@tgl2 filter tanggal batas akhir pencarian
							--@tgl3 tanggal sort

							select * from (
							    select  periode, tanggal, JumlahPlot jumlah, 'prediksi penjualan' mark
							    from LINK_POP.hatce17.dbo.hacplot1 where Kode_zona='tot' and convert(date, Tanggal,103)>=convert(date, @tgl3,103) and convert(date, Tanggal,103)<=convert(date, @tgl2,103) 
							    union all
							    select  periode, tglrekap, sum(jumjual)*100/102 jumlah, 'realisasi penjualan' mark
							    from LINK_POP.hatce17.dbo.hacinterdiv a inner join LINK_POP.hatce17.dbo.accode b on a.kode_akun=b.Kode_akun 
							    where b.divisi='31' and kodebrg='DOQ001' and convert(date, tglrekap,103)>=convert(date, @tgl1,103) and convert(date, tglrekap,103)<convert(date, @tgl3,103)
							    group by periode, tglrekap
							) as a order by convert(date, tanggal,103)
				 ";
		return $sql;
	}	

}

/* End of file Totaldoq_model.php */
/* Location: ./application/models/Totaldoq_model.php */