<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery_model extends CI_Model {

	
	public function gallery_view_all() {
		$this->db->select('*');
		$this->db->order_by('kode_gallery', 'desc');
		return $this->db->get('tbl_gallery');
	}

	public function gallery_view_by_kata_kunci($table , $kata_kunci) {
		$this->db->where('kata_kunci', $kata_kunci);
		$this->db->select('*');
		$this->db->order_by('kode_gallery', 'desc');
		return $this->db->get($table, 1);
	}

	public function gallery_view_by_kode_gallery($table, $kode_gallery) {
		$this->db->where('kode_gallery', $kode_gallery);
		$this->db->select('*');
		$this->db->order_by('kode_gallery', 'asc');
		return $this->db->get($table);
	}

	public function gallery_insert_data($table, $data) {
		$this->db->insert($table, $data);
	}

	public function gallery_insert_data_album($table, $data) {
		$this->db->insert($table, $data);
	}

	public function gallery_update_data($table, $kode_gallery, $where) {
		$this->db->select('*');
		$this->db->where('kode_gallery', $kode_gallery);
		$this->db->update($table, $where);
	}

	public function gallery_delete_data($table, $kode_gallery) {
		$this->db->select('*');
		$this->db->where('kode_gallery', $kode_gallery);
		$this->db->delete($table);
	}

}

/* End of file Gallery_model.php */
/* Location: ./application/models/Gallery_model.php */