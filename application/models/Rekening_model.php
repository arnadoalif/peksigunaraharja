<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekening_model extends CI_Model {

	public function view_data_rekening() {
		return $this->db->get('tbl_rekening');
	}

	public function insert_rekening($table, $data) {
		$this->db->insert($table, $data);
	}

	public function update_rekening($table, $where, $kode_rekening) {
		$this->db->where('kode_rekening', $kode_rekening);
		$this->db->update($table, $where);
	}

	public function get_name_rekening($table, $jenis_rekening) {
		$this->db->where('jenis_rekening', $jenis_rekening);
		return $this->db->get($table);
	}

	public function buat_kode_rekening($nomor_terakhir, $kunci, $jumlah_karakter = 0)
	{
	    /* mencari nomor baru dengan memecah nomor terakhir dan menambahkan 1
	    string nomor baru dibawah ini harus dengan format XXX000000 
	    untuk penggunaan dalam format lain anda harus menyesuaikan sendiri */
	    $nomor_baru = intval(substr($nomor_terakhir, strlen($kunci))) + 1;
	//    menambahkan nol didepan nomor baru sesuai panjang jumlah karakter
	    $nomor_baru_plus_nol = str_pad($nomor_baru, $jumlah_karakter, "0", STR_PAD_LEFT);
	//    menyusun kunci dan nomor baru
	    $kode = $kunci . $nomor_baru_plus_nol;
	    return $kode;
	}

}

/* End of file Rekening_model.php */
/* Location: ./application/models/Rekening_model.php */