<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_model extends CI_Model {

	public function get_data_invoice($kode_order, $kode_pelanggan) {
		$sql = " 

			SELECT tbl_order.kode_order, tbl_order.kode_pelanggan, tbl_order.periode, tbl_order.jumlah_order, tbl_order.tanggal_order, tbl_order.tanggal_kirim, tbl_order.total_harga, tbl_order.status_order,
			tbl_member.nama_pelanggan, tbl_member.alamat, tbl_member.kota, tbl_member.nomor_telepon, tbl_member.email_address, tbl_harga_doq.harga_jual
			FROM tbl_order INNER JOIN
			tbl_member ON tbl_order.kode_pelanggan = tbl_member.kode_pelanggan CROSS JOIN
			tbl_harga_doq where kode_order = '$kode_order' and tbl_order.kode_pelanggan = '$kode_pelanggan';
		 ";

		 return $this->db->query($sql);
	}

}

/* End of file Invoice_model.php */
/* Location: ./application/models/Invoice_model.php */