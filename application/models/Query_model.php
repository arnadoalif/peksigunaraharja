<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Query_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function getAll() {
		// return $this->db->query("select * from tbl_pelanggan");
		return $this->db->get('tbl_pelanggan');
	}

}

/* End of file Query_model.php */
/* Location: ./application/models/Query_model.php */