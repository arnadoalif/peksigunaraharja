<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi_order_model extends CI_Model {

	public function view_transaksi_order() {
		return $this->db->get('tbl_transaksi_order');
	}

	public function insert_transaksi_order($table, $data) {
		$this->db->insert($table, $data);
	}

	public function view_transaksi_order_data_pelanggan_order($table, $kode_pelanggan, $kode_order) {
		$this->db->where('kode_order', $kode_order);
		$this->db->where('kode_pelanggan', $kode_pelanggan);
		return $this->db->get($table);
	}

	public function update_status_pembayaran($table, $kode_order,$kode_pelanggan, $kode_transaksi, $where) {
		$this->db->where('kode_order', $kode_order);
		$this->db->where('kode_pelanggan', $kode_pelanggan);
		$this->db->where('kode_transaksi', $kode_transaksi);
		$this->db->update($table, $where);
	}

	// public function update_status_pembayaran_sisa($table, $kode_pelanggan, $kode_order, $kode_transaksi, $where) {
	// 	$this->db->where('kode_pelanggan', $kode_pelanggan);
	// 	$this->db->where('kode_order', $kode_order);
	// 	$this->db->where('kode_transaksi', $kode_transaksi);
	// 	$this->db->update($table, $where);
	// }

	public function get_data_tranfer_upload_bukti($table, $kode_order, $kode_pelanggan) {
		$this->db->where('kode_order', $kode_order);
		$this->db->where('kode_pelanggan', $kode_pelanggan);
		return $this->db->get($table);
	}

	public function delete_transfer_order($table, $kode_order, $kode_pelanggan) {
		$this->db->where('kode_order', $kode_order);
		$this->db->where('kode_pelanggan', $kode_pelanggan);
		$this->db->delete($table);
	}

	public function valid_data_update_status($table, $kode_order, $kode_pelanggan, $kode_transaksi) {
		$this->db->where('kode_order', $kode_order);
		$this->db->where('kode_pelanggan', $kode_pelanggan);
		$this->db->where('kode_transaksi', $kode_transaksi);
		return $this->db->get($table);
	}

	public function buat_kode_transaksi($nomor_terakhir, $kunci, $jumlah_karakter = 0)
	{
	    /* mencari nomor baru dengan memecah nomor terakhir dan menambahkan 1
	    string nomor baru dibawah ini harus dengan format XXX000000 
	    untuk penggunaan dalam format lain anda harus menyesuaikan sendiri */
	    $nomor_baru = intval(substr($nomor_terakhir, strlen($kunci))) + 1;
	//    menambahkan nol didepan nomor baru sesuai panjang jumlah karakter
	    $nomor_baru_plus_nol = str_pad($nomor_baru, $jumlah_karakter, "0", STR_PAD_LEFT);
	//    menyusun kunci dan nomor baru
	    $kode = $kunci . $nomor_baru_plus_nol;
	    return $kode;
	}

	public function view_detail_pembayaran($kode_pelanggan) {
		$sql = "

			SELECT tbl_transaksi_order.kode_order AS kode_order, tbl_transaksi_order.jenis_rekening AS jenis_rekening, tbl_transaksi_order.nomor_rekening, tbl_order.presentase, tbl_order.total_harga AS total_harga, tbl_order.satuan_doq,
			tbl_transaksi_order.atas_nama, tbl_transaksi_order.uang_muka, tbl_transaksi_order.status_bayar_uang_muka, tbl_transaksi_order.sisa_uang, tbl_transaksi_order.status_bayar_uang_sisa,
			tbl_transaksi_order.total_harga, tbl_transaksi_order.status_total_harga
			FROM            tbl_transaksi_order INNER JOIN
			tbl_order ON tbl_transaksi_order.kode_order = tbl_order.kode_order
			WHERE tbl_order.kode_pelanggan = '$kode_pelanggan';
		 ";
		 return $this->db->query($sql);
	}

	
	

}

/* End of file Transaksi_order_model.php */
/* Location: ./application/models/Transaksi_order_model.php */