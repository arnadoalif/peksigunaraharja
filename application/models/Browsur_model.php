<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Browsur_model extends CI_Model {

	public function get_data_browsur() {
		$this->db->order_by('id_browsur', 'desc');
		$data = $this->db->get('tbl_dt_browsur');
		return $data->result();
	}

	public function insert_browsur($table, $data) {
		$this->db->insert($table, $data);
	}

	public function delete_browsur($table, $id_data) {
		$this->db->where('id_browsur', $id_data);
		$this->db->delete($table);
	}

}

/* End of file Browsur_model.php */
/* Location: ./application/models/Browsur_model.php */