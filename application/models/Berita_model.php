<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita_model extends CI_Model {

	function view_berita()
	{
		
		$this->db->order_by('id_berita', 'desc');
		return $this->db->get('tbl_data_berita');
	}

	function tambah_berita($data) 
	{
		$this->db->insert('tbl_data_berita', $data);
	}

	function data($number,$offset){
		return $query = $this->db->get('tbl_data_berita',$number,$offset)->result();		
	}

	function jumlah_data($table_name) {
		return $this->db->get($table_name)->num_rows();
	}

	function update_berita($apikey_berita, $data) {
		$this->db->where("apikey_berita", $apikey_berita);
		$this->db->update("tbl_data_berita", $data);
	}

	function view_limit_data($number) {
		$this->db->order_by('apikey_berita', 'RANDOM');
		return $this->db->get('tbl_data_berita', $number)->result();
	}

	function delete_berita($id_berita) 
	{
		$this->db->where("apikey_berita", $id_berita);
		$this->db->delete("tbl_data_berita");
	}

	function get_detail_data($apikey_berita) {
		$this->db->where('apikey_berita', $apikey_berita);
		return $this->db->get('tbl_data_berita');
	}

	function get_detail_data_kata_kunci($kata_kunci) {
		$this->db->where('kata_kunci', $kata_kunci);
		return $this->db->get('tbl_data_berita', 1);
	}


}

/* End of file Berita_model.php */
/* Location: ./application/models/Berita_model.php */