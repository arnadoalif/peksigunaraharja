<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_model extends CI_Model {

	public function get_data_member() {
		$this->db->order_by('kode_pelanggan', 'desc');
		$data = $this->db->get('tbl_member');
		return $data->result();
	}

	public function view_member() {
		return $this->db->get('tbl_member');
	}

	public function get_data_where_kode_member($table, $kode_pelanggan) {
		$this->db->where('kode_pelanggan', $kode_pelanggan);
		return $this->db->get($table);
	}

	public function insert_member($table, $data) {
		$this->db->insert($table, $data);
	}

	public function delete_member($table, $id_data) {
		$this->db->where('id_browsur', $id_data);
		$this->db->delete($table);
	}

	public function check_vallid_data($table ,$where) {
		return $this->db->get_where($table,$where);
	}

	public function update_member($table, $kode_pelanggan, $where) {
		$this->db->where('kode_pelanggan', $kode_pelanggan);
		return $this->db->update($table, $where);
	}

	public function buatkode($nomor_terakhir, $kunci, $jumlah_karakter = 0)
	{
	    /* mencari nomor baru dengan memecah nomor terakhir dan menambahkan 1
	    string nomor baru dibawah ini harus dengan format XXX000000 
	    untuk penggunaan dalam format lain anda harus menyesuaikan sendiri */
	    $nomor_baru = intval(substr($nomor_terakhir, strlen($kunci))) + 1;
	//    menambahkan nol didepan nomor baru sesuai panjang jumlah karakter
	    $nomor_baru_plus_nol = str_pad($nomor_baru, $jumlah_karakter, "0", STR_PAD_LEFT);
	//    menyusun kunci dan nomor baru
	    $kode = $kunci . $nomor_baru_plus_nol;
	    return $kode;
	}

}

/* End of file Member_model.php */
/* Location: ./application/models/Member_model.php */