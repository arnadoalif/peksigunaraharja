<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends CI_Model {

	public function view_histori_order($kode_member) {
		$this->db->where('kode_pelanggan', $kode_member);
		$this->db->where('status_order !=', 'menunggu');
		$this->db->where('status_order !=', 'diproses');
		$this->db->where('status_order !=', 'terkirim');
		$this->db->order_by('kode_order', 'desc');
		return $this->db->get('tbl_order');
	}

	public function list_order($kode_member) {
		$this->db->where('kode_pelanggan', $kode_member);
		$this->db->where('status_order !=', 'diterima');
		$this->db->where('status_order !=', 'gagal');
		$this->db->order_by('kode_order', 'desc');
		return $this->db->get('tbl_order');
	}

	public function insert_data_order($data) {
		$this->db->insert('tbl_order', $data);
	}

	public function view_order() {
		$this->db->where('status_order', 'menunggu');
		return $this->db->get('tbl_order');
	}

	public function check_data_order($table, $kode_order, $kode_pelanggan) {
		$this->db->where('kode_order', $kode_order);
		$this->db->where('kode_pelanggan', $kode_pelanggan);
		return $this->db->get($table);
	}

	public function update_data_order($table, $kode_order, $kode_pelanggan, $data) {
		$this->db->where('kode_order', $kode_order);
		$this->db->where('kode_pelanggan', $kode_pelanggan);
		$this->db->update($table, $data);
	}

	public function count_status_order($table, $kode_pelanggan, $status) {
		$this->db->where('kode_pelanggan', $kode_pelanggan);
		$this->db->where('status_order', $status);
		return $this->db->get($table);
	}

	public function count_status_order_all($table, $status) {
		$this->db->where('status_order', $status);
		return $this->db->get($table);
	}

	public function delete_order($table, $kode_order, $kode_pelanggan) {
		$this->db->where('kode_order', $kode_order);
		$this->db->where('kode_pelanggan', $kode_pelanggan);
		$this->db->delete($table);
	}

	public function update_status_checkout($table, $kode_order, $kode_pelanggan, $data) {
		$this->db->where('kode_pelanggan', $kode_pelanggan);
		$this->db->where('kode_order', $kode_order);
		$this->db->update($table, $data);
	}

	public function update_status_pembayaran_order($table, $kode_order, $kode_pelanggan, $data) {
		$this->db->where('kode_order', $kode_order);
		$this->db->where('kode_pelanggan', $kode_pelanggan);
		$this->db->update($table, $data);
	}

	public function buat_kode_order($nomor_terakhir, $kunci, $jumlah_karakter = 0)
	{
	    /* mencari nomor baru dengan memecah nomor terakhir dan menambahkan 1
	    string nomor baru dibawah ini harus dengan format XXX000000 
	    untuk penggunaan dalam format lain anda harus menyesuaikan sendiri */
	    $nomor_baru = intval(substr($nomor_terakhir, strlen($kunci))) + 1;
	//    menambahkan nol didepan nomor baru sesuai panjang jumlah karakter
	    $nomor_baru_plus_nol = str_pad($nomor_baru, $jumlah_karakter, "0", STR_PAD_LEFT);
	//    menyusun kunci dan nomor baru
	    $kode = $kunci . $nomor_baru_plus_nol;
	    return $kode;
	}

	public function delete_masa_tenggang() {
		$tanggal_sekarang = date('y-m-d');
		$this->db->where('tgl_masa_tenggang', $tanggal_sekarang);
		$this->db->where('status_checkout', "");
		$this->db->delete('tbl_order');
	}


}

/* End of file Order_model.php */
/* Location: ./application/models/Order_model.php */