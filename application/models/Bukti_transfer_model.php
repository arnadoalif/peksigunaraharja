<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bukti_transfer_model extends CI_Model {

	public function insert_data_bukti($data) {
		$this->db->insert('tbl_upload_bukti_transfer', $data);
	}

	public function view_data_bukti_transfer($table) {
		return $this->db->get($table);
	}

	public function view_data_bukti_transfer_by_member($table, $kode_order, $kode_pelanggan) {
		$this->db->where('kode_order', $kode_order);
		$this->db->where('kode_pelanggan', $kode_pelanggan);
		return $this->db->get($table);
	}

	public function update_status_upload_file($table, $kode_order, $kode_pelanggan, $kode_transaksi, $kode_bukti, $data) {
		$this->db->where('kode_order', $kode_order);
		$this->db->where('kode_bukti', $kode_bukti);
		$this->db->where('kode_transaksi', $kode_transaksi);
		$this->db->where('kode_pelanggan', $kode_pelanggan);
		$this->db->update($table, $data);
	}

	public function delete_bukti_upload($table, $kode_order, $kode_transaksi, $kode_pelanggan) {
		$this->db->where('kode_order', $kode_order);
		$this->db->where('kode_transaksi', $kode_transaksi);
		$this->db->where('kode_pelanggan', $kode_pelanggan);
		$this->db->delete($table);
	}

	public function view_data_bukti_transfer_status_menunggu() {
		$this->db->where('status_bukti', 'menunggu');
		return $this->db->get('tbl_upload_bukti_transfer');
	}

}

/* End of file Bukti_transfer_model.php */
/* Location: ./application/models/Bukti_transfer_model.php */