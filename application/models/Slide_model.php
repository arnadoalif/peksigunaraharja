<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slide_model extends CI_Model {

	public function insert_slide($table, $data) {
		$this->db->insert($table, $data);
	}

	public function view_slide($table) {
		$this->db->order_by('kode_slide', 'desc');
		return $this->db->get($table);
	}

	public function delete_slide($table, $kode_slide) {
		$this->db->where('kode_slide', $kode_slide);
		$this->db->delete($table);
	}

}

/* End of file Slide_model.php */
/* Location: ./application/models/Slide_model.php */