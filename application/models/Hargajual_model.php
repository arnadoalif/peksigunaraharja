<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hargajual_model extends CI_Model {

	public function view_harga_jual() {
		$this->db->order_by('kode_harga', 'desc');
		return $this->db->get('tbl_harga_doq', 1);
	}

	public function insert_harga_jual($table, $data) {
		$this->db->insert($table, $data);
	}

	public function update_harga_jual($table, $where, $kode_harga) {
		$this->db->where('kode_harga', $kode_harga);
		$this->db->update($table, $where);
	}

	public function delete_harga_jual($table, $kode_harga) {
		$this->db->where('kode_harga', $kode_harga);
		$this->db->delete($table);
	}

}

/* End of file Hargajual_model.php */
/* Location: ./application/models/Hargajual_model.php */